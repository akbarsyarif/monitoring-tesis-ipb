<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?= get_option('app_title') ?> | <?= get_option('app_description') ?></title>

	<!-- stylesheet -->

	<!-- Bootstrap core CSS -->
	<link href="<?= theme_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">

	<!-- font awesome -->
	<link href="<?= theme_url('assets/fonts/css/font-awesome.min.css') ?>" rel="stylesheet">
	
	<!-- jquery animate -->
	<link href="<?= theme_url('assets/css/animate.min.css') ?>" rel="stylesheet">

	<!-- datatables css -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>">

	<!-- select2 -->
	<link href="<?= theme_url('assets/css/select/select2.min.css') ?>" rel="stylesheet">

	<!-- nprogress -->
	<link href="<?= theme_url('assets/css/nprogress.css') ?>" rel="stylesheet">

	<!-- alertify -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bower_components/alertify.js/themes/alertify.core.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bower_components/alertify.js/themes/alertify.bootstrap.css') ?>">

	<!-- magnific popup -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bower_components/magnific-popup/dist/magnific-popup.css') ?>">

	<!-- datetime picker -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') ?>">

	<!-- Custom styling plus plugins -->
	<link href="<?= theme_url('assets/css/custom.css') ?>" rel="stylesheet">




	<!-- javascript -->

	<!-- jquery and bootstrap -->
	<script src="<?= base_url('assets/bower_components/jquery/dist/jquery.min.js') ?>"></script>
	<script src="<?= theme_url('assets/js/bootstrap.min.js') ?>"></script>

	<!-- jquery form -->
	<script src="<?= base_url('assets/js/jquery.form.min.js') ?>"></script>
	
	<!-- select2 -->
	<script src="<?= theme_url('assets/js/select/select2.full.js') ?>"></script>

	<!-- nprogress -->
	<script src="<?= theme_url('assets/js/nprogress.js') ?>"></script>

	<!-- datatables -->
	<script src="<?= base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
	<script src="<?= base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>

	<!-- form validation -->
	<script type="text/javascript" src="<?= base_url('assets/bower_components/parsleyjs/dist/parsley.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/bower_components/parsleyjs/src/i18n/id.js') ?>"></script>

	<!-- nicescroll -->
	<script src="<?= theme_url('assets/js/nicescroll/jquery.nicescroll.min.js') ?>"></script>

	<!-- inputmask -->
	<script type="text/javascript" src="<?= base_url('assets/bower_components/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') ?>"></script>

	<!-- alertify -->
	<script type="text/javascript" src="<?= base_url('assets/bower_components/alertify.js/lib/alertify.min.js') ?>"></script>

	<!-- jquery timeago -->
	<script src="<?= base_url('assets/js/jquery.timeago.js') ?>"></script>

	<!-- magnific popup -->
	<script type="text/javascript" src="<?= base_url('assets/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js') ?>"></script>

	<!-- tinymce -->
	<script type="text/javascript" src="<?= base_url('assets/js/tinymce/js/tinymce/tinymce.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/tinymce/js/tinymce/jquery.tinymce.min.js') ?>"></script>

	<!-- datepicker -->
	<script type="text/javascript" src="<?= base_url('assets/bower_components/moment/min/moment-with-locales.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') ?>"></script>

	<!-- chart.js -->
	<script type="text/javascript" src="<?= base_url('assets/bower_components/chart.js/dist/Chart.bundle.min.js') ?>"></script>

	<!-- custom -->
	<script src="<?= theme_url('assets/js/custom.js') ?>"></script>
	<script src="<?= base_url('assets/js/loading.js') ?>"></script>
	<script src="<?= base_url('assets/js/app.js') ?>"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			NProgress.inc();
		});
	</script>
	
	<!--[if lt IE 9]>
		<script src="../assets/js/ie8-responsive-file-warning.js"></script>
		<![endif]-->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body class="nav-md">

	<?= display_loading() ?>

	<div class="container body">

		<div class="main_container">

			<div class="col-md-3 left_col">

				<div class="left_col scroll-view">

					<div class="navbar nav_title" style="border: 0;">
						<a href="index.html" class="site_title"><i class="fa fa-bullhorn"></i> <span><?= get_option('app_title') ?></span></a>
					</div>
					<div class="clearfix"></div>

					<!-- menu prile quick info -->
					<div class="profile">
						<div class="profile_pic">
							<img src="<?= $this->sp_auth->userData()->profile_picture ?>" alt="..." class="img-circle profile_img current_user_avatar">
						</div>
						<div class="profile_info">
							<span>Welcome,</span>
							<h2><?= $this->sp_auth->userData()->full_name ?></h2>
						</div>
					</div>
					<!-- /menu prile quick info -->

					<p>&nbsp;</p>

					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

						<?php

							if ( $this->sp_auth->isInThisGroup('administrator') ) {

								require 'administrator_sidemenu.php';

							}

							if ( $this->sp_auth->isInThisGroup('operator_fakultas') ) {

								require 'operator_fakultas_sidemenu.php';

							}

						?>

					</div>
					<!-- /sidebar menu -->

					<!-- /menu footer buttons -->
					<div class="sidebar-footer hidden-small">
						<a data-toggle="tooltip" data-placement="top" title="Settings">
							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="FullScreen">
							<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Lock">
							<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Logout" href="<?= site_url('logout') ?>">
							<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
						</a>
					</div>
					<!-- /menu footer buttons -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">

				<div class="nav_menu">
					<nav class="" role="navigation">
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">
							<li class="">
								<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<img class="current_user_avatar" src="<?= $this->sp_auth->userData()->profile_picture ?>" alt=""> <?= $this->sp_auth->userData()->full_name ?>
									<span class=" fa fa-angle-down"></span>
								</a>
								<ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
									<li><a href="<?= site_url('authentication/profile') ?>">Profile</a></li>
									<li><a href="<?= site_url('logout') ?>"><i class="fa fa-sign-out pull-right"></i> Logout</a></li>
								</ul>
							</li>

							<li role="presentation" class="dropdown">
								<a href="javascript:;" class="navbar_count_unread_message dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
									<i class="fa fa-envelope-o"></i>
									<span class="badge bg-green">0</span>
								</a>
								<ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
									<li>
										<div class="text-center">
											<a>
												<strong><a href="<?= site_url('authentication/profile') ?>">Lihat Semua Pesan</strong>
												<i class="fa fa-angle-right"></i>
											</a>
										</div>
									</li>
								</ul>
							</li>

							<!-- <li role="presentation" class="dropdown">
								<a href="javascript:;" class="navbar_count_unread_notification dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
									<i class="fa fa-globe"></i>
									<span class="badge bg-green">0</span>
								</a>
								<ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
									<li>
										<div class="text-center">
											<a>
												<strong><a href="<?= site_url('administrator/notification') ?>">Lihat Semua</strong>
												<i class="fa fa-angle-right"></i>
											</a>
										</div>
									</li>
								</ul>
							</li> -->

							<!-- <li role="presentation" class="dropdown">
								<a href="javascript:;" class="navbar_count_unread_payment_confirmation dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
									<i class="fa fa-money"></i>
									<span class="badge bg-green">0</span>
								</a>
								<ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
									<li>
										<div class="text-center">
											<a>
												<strong><a href="<?= site_url('administrator/konfirmasi_pembayaran') ?>">Lihat Semua</strong>
												<i class="fa fa-angle-right"></i>
											</a>
										</div>
									</li>
								</ul>
							</li> -->

						</ul>
					</nav>
				</div>
			</div>
			<!-- /top navigation -->