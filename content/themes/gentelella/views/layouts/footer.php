</div>

	</div>

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			NProgress.done();

			// initialize timeago
			$("abbr.timeago").timeago();

			// magnific popup
			$('a.magnific-popup').magnificPopup({
				type: 'image'
			});

			// init tinymce
			initTinymce('textarea.richtext');

			$('.datepicker').datetimepicker({
				format: "YYYY-MM-DD"
			});

			// call get notification
			// getNotifications();

			// get notification every 5 minutes
			// setInterval(function() {
			// 	getNotifications();
			// }, 600000);
		});

		// show progress when ajax start
		$(document).ajaxStart(function() {
			NProgress.start();
			NProgress.set(0.3);
			NProgress.inc();
		});

		// stop progress when ajax stop
		$(document).ajaxStop(function() {
			NProgress.done();
		});

		// count unread message
		// show it in navbar
		function getUnreadMessage() {
			$.ajax({
				url : '<?= site_url("message/count_unread") ?>',
				dataType : 'json',
				success : function(response) {
					// set count message
					$('.navbar_count_unread_message .badge').html(response.count);

					var messages = response.data;
					var html = '';

					for ( i=0; i<=messages.length - 1; i++ ) {
						var message = messages[i];

						html += '<li>';
							html += '<a>';
								html += '<span class="image">';
									html += '<img src="'+ message.avatar +'" alt="Profile Image" />';
								html += '</span>';
								html += '<span>';
									html += '<span>'+ message.full_name +'</span>';
									html += '<span class="time"><abbr class="timeago" title="'+ message.timestamp +'">'+ message.month + ' ' + message.date +'</abbr></span>';
								html += '</span>';
								html += '<span class="message">';
									html += message.message_brief;
								html += '</span>';
							html += '</a>';
						html += '</li>';
					}

					$('#menu1').prepend(html);
				},
				error : function() {
					console.log('Tidak dapat menampilkan pesan yang belum dibaca.');
					alert('Error occured. Tidak dapat menghubungi server.')
				},
				complete : function() {
					$('#menu1').find("abbr.timeago").timeago();
				}
			})
		};

		// get notifications
		function getNotifications() {
			$.ajax({
				url : '<?= site_url("notification/count") ?>',
				dataType : 'json',
				success : function(response) {
					// set count message
					$('.navbar_count_unread_notification .badge').html(response.count_notification);
					$('.navbar_count_unread_payment_confirmation .badge').html(response.count_payment_confirmation);
				},
				error : function() {
					alert('Error occured. Tidak dapat menghubungi server.');
				}
			})
		};

		// get and set kab/kota when provinsi changed
		function getKabupaten(obj) {
			if (typeof obj.beforeSend != 'undefined') {
				obj.beforeSend();
			}

			// validate parameter
			// id provinsi
			if (obj.prov_id == '' || typeof obj.prov_id == 'undefined') {
				console.log('Error : ID Provinsi tidak ditemukan.');
			}
			else {
				$.ajax({
					url : obj.url,
					method : 'get',
					data : { prov_id : obj.prov_id },
					dataType : 'json',
					success : function(response) {
						if (response.result == false) {
							alert(response.message);
						}
						else {
							html = '<option value="">-- Pilih Kabupaten --</option>';
							$(obj.target).html(function() {
								for (i=0; i<=response.data.length - 1; i++) {
									var current_option = response.data[i];

									if (typeof obj.selected != 'undefined') {
										if (obj.selected == current_option.id_kab) {
											html += '<option selected value="'+current_option.id_kab+'">'+current_option.nama+'</option>';
										}
										else {
											html += '<option value="'+current_option.id_kab+'">'+current_option.nama+'</option>';
										}
									}
									else {
										html += '<option value="'+current_option.id_kab+'">'+current_option.nama+'</option>';
									}
								}

								return html;
							});
						}
					},
					error : function(error) {
						console.log(response);
					},
					complete : function() {
						if (typeof obj.complete != 'undefined') {
							obj.complete();
						}
					}
				})
			}
		}

		// get and set kecamatan when kabupaten changed
		function getKecamatan(obj) {
			if (typeof obj.beforeSend != 'undefined') {
				obj.beforeSend();
			}

			// validate parameter
			// id kabupaten
			if (obj.kab_id == '' || typeof obj.kab_id == 'undefined') {
				console.log('Error : ID Kabupaten tidak ditemukan.');
			}
			else {
				$.ajax({
					url : obj.url,
					method : 'get',
					data : { kab_id : obj.kab_id },
					dataType : 'json',
					success : function(response) {
						if (response.result == false) {
							alert(response.message);
						}
						else {
							html = '<option value="">-- Pilih Kecamatan --</option>';
							$(obj.target).html(function() {
								for (i=0; i<=response.data.length - 1; i++) {
									var current_option = response.data[i];

									if (typeof obj.selected != 'undefined') {
										if (obj.selected == current_option.id_kec) {
											html += '<option selected value="'+current_option.id_kec+'">'+current_option.nama+'</option>';
										}
										else {
											html += '<option value="'+current_option.id_kec+'">'+current_option.nama+'</option>';
										}
									}
									else {
										html += '<option value="'+current_option.id_kec+'">'+current_option.nama+'</option>';
									}
								}

								return html;
							});
						}
					},
					error : function(error) {
						console.log(response);
					},
					complete : function() {
						if (typeof obj.complete != 'undefined') {
							obj.complete();
						}
					}
				})
			}
		}

		// get and set kelurahan when kecamatan changed
		function getKelurahan(obj) {
			if (typeof obj.beforeSend != 'undefined') {
				obj.beforeSend();
			}

			// validate parameter
			// id kecamatan
			if (obj.kec_id == '' || typeof obj.kec_id == 'undefined') {
				console.log('Error : ID Kabupaten tidak ditemukan.');
			}
			else {
				$.ajax({
					url : obj.url,
					method : 'get',
					data : { kec_id : obj.kec_id },
					dataType : 'json',
					success : function(response) {
						if (response.result == false) {
							alert(response.message);
						}
						else {
							html = '<option value="">-- Pilih Kelurahan --</option>';
							$(obj.target).html(function() {
								for (i=0; i<=response.data.length - 1; i++) {
									var current_option = response.data[i];

									if (typeof obj.selected != 'undefined') {
										if (obj.selected == current_option.id_kel) {
											html += '<option selected value="'+current_option.id_kel+'">'+current_option.nama+'</option>';
										}
										else {
											html += '<option value="'+current_option.id_kel+'">'+current_option.nama+'</option>';
										}
									}
									else {
										html += '<option value="'+current_option.id_kel+'">'+current_option.nama+'</option>';
									}
								}

								return html;
							});
						}
					},
					error : function(error) {
						console.log(response);
					},
					complete : function() {
						if (typeof obj.complete != 'undefined') {
							obj.complete();
						}
					}
				})
			}
		}

		// get and set program studi when fakultas changed
		function getProgramStudi(obj) {
			if (typeof obj.beforeSend != 'undefined') {
				obj.beforeSend();
			}

			// validate parameter
			// fakultas id
			if (obj.fakultas_id == '' || typeof obj.fakultas_id == 'undefined') {
				console.log('Error : ID Fakultas tidak ditemukan.');
			}
			else {
				$.ajax({
					url : obj.url,
					method : 'get',
					data : { fakultas_id : obj.fakultas_id },
					dataType : 'json',
					success : function(response) {
						if (response.result == false) {
							alert(response.message);
						}
						else {
							html = '<option value="">-- Pilih Program Studi --</option>';
							$(obj.target).html(function() {
								for (i=0; i<=response.data.length - 1; i++) {
									var current_option = response.data[i];
									
									if (typeof obj.selected != 'undefined') {
										if (obj.selected == current_option.id) {
											html += '<option selected value="'+current_option.id+'">'+current_option.program_studi+'</option>';
										}
										else {
											html += '<option value="'+current_option.id+'">'+current_option.program_studi+'</option>';
										}
									}
									else {
										html += '<option value="'+current_option.id+'">'+current_option.program_studi+'</option>';
									}
								}

								return html;
							});
						}
					},
					error : function(error) {
						console.log(response);
					},
					complete : function() {
						if (typeof obj.complete != 'undefined') {
							obj.complete();
						}
					}
				})
			}
		}
	</script>
	<!-- /datepicker -->
	<!-- /footer content -->
</body>

</html>