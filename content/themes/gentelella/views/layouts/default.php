<?php require 'header.php' ?>

<!-- page content -->
<div class="right_col" role="main">

	<!-- main content -->
	<div class="row">
		<div class="col-md-12">
			<div class="main-content">
				<div class="page-title">
					<div class="title_left">
						<h3>
							<?php $icon = isset($page_fa_icon) ? '<i class="'.$page_fa_icon.' fa fa-fw"></i>' : '' ?>
							<?= $icon ?> <?= $page_title ?> <small>( <?= $page_sub_title ?> )</small>
						</h3>
					</div>
				</div>
				<div class="clearfix"></div>

				<?= $template['body'] ?>

				<noscript>JavaScript is off. Please enable to view full site.</noscript>
			</div>
		</div>
	</div>

	<!-- footer content -->
	<footer>
		<div class="pull-left">This page loaded in {elapsed_time} seconds</div>
		<div class="pull-right">Copyright &copy; 2015 <?= get_option('app_title') ?> | <?= get_option('app_description') ?><div>
	</footer>
	<!-- /footer content -->
</div>
<!-- /page content -->

<?php require 'footer.php' ?>