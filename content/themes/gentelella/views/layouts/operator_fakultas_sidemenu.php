<div class="menu_section">
	<h3>Operator Fakultas</h3>
	<ul class="nav side-menu">
		<li><a href="<?= site_url('operator_fakultas/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard </a></li>
		<li><a href="<?= site_url('operator_fakultas/progress') ?>"><i class="fa fa-line-chart"></i> Daftar Progress </a></li>
		<li><a><i class="fa fa-line-chart"></i> Perkembangan Tesis <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu" style="display: none">
				<li><a href="<?= site_url('laporan/perkembangan_tesis/semua_mahasiswa') ?>">Semua Mahasiswa</a></li>
				<li><a href="<?= site_url('laporan/perkembangan_tesis/per_angkatan') ?>">Per Angkatan</a></li>
				<li><a href="<?= site_url('laporan/perkembangan_tesis/per_pembimbing') ?>">Per Pembimbing</a></li>
			</ul>
		</li>
		<li><a><i class="fa fa-user"></i> Account <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu" style="display: none">
				<li><a href="<?= site_url('authentication/profile') ?>">Profile</a></li>
				<li><a href="<?= site_url('logout') ?>">Logout</a></li>
			</ul>
		</li>
	</ul>
</div>