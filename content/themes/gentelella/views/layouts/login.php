<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?= get_option('app_title') ?> | <?= get_option('app_description') ?></title>

	<!-- Bootstrap core CSS -->

	<link href="<?= theme_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">

	<link href="<?= theme_url('assets/fonts/css/font-awesome.min.css') ?>" rel="stylesheet">
	<link href="<?= theme_url('assets/css/animate.min.css') ?>" rel="stylesheet">

	<!-- Custom styling plus plugins -->
	<link href="<?= theme_url('assets/css/custom.css') ?>" rel="stylesheet">
	<link href="<?= theme_url('assets/css/icheck/flat/green.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets/css/loading.css') ?>" rel="stylesheet">

	<script src="<?= theme_url('assets/js/jquery.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/jquery.form.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/loading.js') ?>"></script>
	<script src="<?= theme_url('assets/js/nprogress.js') ?>"></script>

	<!-- PNotify -->
	<script type="text/javascript" src="<?= theme_url('assets/js/notify/pnotify.core.js') ?>"></script>
	<script type="text/javascript" src="<?= theme_url('assets/js/notify/pnotify.buttons.js') ?>"></script>
	<script type="text/javascript" src="<?= theme_url('assets/js/notify/pnotify.nonblock.js') ?>"></script>
	
	<script type="text/javascript">
		NProgress.start();
	</script>

	<!--[if lt IE 9]>
		<script src="../assets/js/ie8-responsive-file-warning.js"></script>
		<![endif]-->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body style="background:#F7F7F7;">

	<!-- display loading -->
	<?= display_loading() ?>
	
	<div class="">

		<div id="wrapper">

			<?= $template['body'] ?>
		
		</div>
	</div>

	<script type="text/javascript">
		NProgress.done();
	</script>

</body>

</html>