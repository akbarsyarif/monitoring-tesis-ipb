<div class="menu_section">
	<h3>Admdinistrator Menu</h3>
	<ul class="nav side-menu">
		<li><a href="<?= site_url('administrator/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard </a></li>
		<li><a><i class="fa fa-database"></i> Master <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu" style="display: none">
				<li><a href="<?= site_url('administrator/manage_fakultas') ?>">Fakultas</a></li>
				<li><a href="<?= site_url('administrator/manage_program_studi') ?>">Program Studi</a></li>
				<li><a href="<?= site_url('administrator/manage_dosen') ?>">Dosen</a></li>
				<li><a href="<?= site_url('administrator/manage_mahasiswa') ?>">Mahasiswa</a></li>
			</ul>
		</li>
		<li><a><i class="fa fa-line-chart"></i> Perkembangan Tesis <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu" style="display: none">
				<li><a href="<?= site_url('laporan/perkembangan_tesis/semua_mahasiswa') ?>">Semua Mahasiswa</a></li>
				<li><a href="<?= site_url('laporan/perkembangan_tesis/per_angkatan') ?>">Per Angkatan</a></li>
				<li><a href="<?= site_url('laporan/perkembangan_tesis/per_pembimbing') ?>">Per Pembimbing</a></li>
			</ul>
		</li>
		<li><a><i class="fa fa-cogs"></i> Settings<span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu" style="display: none">
				<li><a href="<?= site_url('administrator/setting_general') ?>">Umum</a></li>
			</ul>
		</li>
		<li><a href="<?= site_url('administrator/manage_user') ?>"><i class="fa fa-users"></i> Users</a></li>
		<li><a><i class="fa fa-dropbox"></i> Extras<span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu" style="display: none">
				<li><a href="<?= site_url('pengumuman/manage_pengumuman') ?>">Pengumuman</a></li>
			</ul>
		</li>
		<li><a><i class="fa fa-user"></i> Account<span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu" style="display: none">
				<li><a href="<?= site_url('authentication/profile') ?>">Profile</a></li>
				<li><a href="<?= site_url('logout') ?>">Logout</a></li>
			</ul>
		</li>
	</ul>
</div>