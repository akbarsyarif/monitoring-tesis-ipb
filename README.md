# Monitoring Tesis #

Aplikasi monitoring progress studi mahasiswa pascasarjana IPB

### Cara Install ###

* Masuk ke direktori system/application/config
* Copy file config-sample.php, lalu paste dengan nama config.php
* Edit file config.php dan sesuaikan dengan konfigurasi server anda
* Copy file database-sample.php, lalu paste dengan nama database.php
* Edit file database.php dan sesuaikan dengan konfigurasi server anda
* Buat database baru (sesuaikan dengan konfigurasi pada file database.php) lalu import file .sql monitoring_tesis.sql