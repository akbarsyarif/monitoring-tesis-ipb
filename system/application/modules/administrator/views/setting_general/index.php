<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_content">
				<?= form_open('administrator/setting_general/save_settings', 'class="form-horizontal" id="setting-form" data-parsley-validate="true"'); ?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<?= form_label('Nama Aplikasi', '', array( 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' )); ?>
								<div class="col-md-9 col-sm-9 col-xs-12">
									<?= form_input('app_title', get_option('app_title'), array( 'class' => 'form-control', 'required' => true )); ?>
								</div>
							</div>
							<div class="form-group">
								<?= form_label('Deskripsi Aplikasi', '', array( 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' )); ?>
								<div class="col-md-9 col-sm-9 col-xs-12">
									<?= form_input('app_description', get_option('app_description'), array( 'class' => 'form-control', 'required' => true )); ?>
								</div>
							</div>
							<div class="form-group">
								<?= form_label('Jumlah Pengumuman', '', array( 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' )); ?>
								<div class="col-md-9 col-sm-9 col-xs-12">
									<?= form_input('announcement_count', get_option('announcement_count'), array( 'class' => 'form-control', 'required' => true )); ?>
									<p class="help-block">Berapa banyak pengumuman yang ingin ditampilkan di dashboard.</p>
								</div>
							</div>

							<div class="ln_solid"></div>
							<div class="col-md-offset-3 col-md-9 col-sm-9 col-xs-12">
								<button class="btn btn-round btn-sm btn-dark" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Sedang memproses data..." type="submit"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
								<button class="btn btn-round btn-sm btn-default" type="reset"><i class="fa fa-fw fa-times-circle"></i> Batal</button>&nbsp;&nbsp;
							</div>
						</div>
					</div>
				<?= form_close(); ?>
			</div>
		</div>
	</div>
</div>

<!-- javascript -->
<script type="text/javascript">
	$(document).ready(function() {
		/**
		 * save settings
		 */
		$('#setting-form').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				if ( $form.parsley().isValid() ) {
					$('#setting-form [type="submit"]').button('loading');
					return true;
				}

				return false;
			},
			success: function(response) {
				if ( response.result == false ) {
					alertify.error(response.message);
				}
				else {
					alertify.success('Pengaturan anda telah disimpan.');
				}
			},
			error: function(error) {
				alertify.error('Error occured. Tidak dapat menghubungi server.');
			},
			complete: function() {
				$('#setting-form [type="submit"]').button('reset');
			}
		})
	});
</script>