<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="pull-left">
					<button class="btn btn-round btn-sm btn-dark" data-toggle="modal" href='#data-add-modal'><i class="fa fa-fw fa-plus-circle"></i> Tambah</button>
				</div>
				<div class="pull-right">
					<!-- put something here -->
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<!-- advance search -->

				<!-- datatable -->
				<table class="table" id="data-list" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th width="10"><?= form_checkbox('ids', ''); ?></th>
							<th width="10"></th>
							<th>Nama</th>
							<th>Username</th>
							<th>Grup</th>
							<th>Status</th>
							<th width="10"></th>
							<th width="80"></th>
							<th width="90"></th>
							<th width="80"></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<?php require 'data_add_modal.php' ?>
<?php require 'data_edit_modal.php' ?>
<?php require 'data_remove_modal.php' ?>

<!-- javascript -->
<script type="text/javascript">
	$(document).ready(function() {
		function format ( d ) {
			// `d` is the original data object for the row
			var html = '';

			html += '<table>';
				html += '<tr>';
					html += '<td width="140"><img src="'+d.profile_picture+'" class="img-circle profile_img" style="margin-top: 0" /></td>';				
					html += '<td>';
						html += '<table class="padding">';
							html += '<tr>';
								html += '<th>Nama Depan</th><td>:</td><td>'+ d.first_name +'</td>';
							html += '</tr>';
							html += '<tr>';
								html += '<th>Nama Belakang</th><td>:</td><td>'+ d.last_name +'</td>';
							html += '</tr>';
							html += '<tr>';
								html += '<th>Email</th><td>:</td><td>'+ d.email +'</td>';
							html += '</tr>';
							html += '<tr>';
								html += '<th>Username</th><td>:</td><td>'+ d.username +'</td>';
							html += '</tr>';
							html += '<tr>';
								html += '<th>Grup</th><td>:</td><td>'+ d.group_alias +'</td>';
							html += '</tr>';

							if ( d.group_id == "2" ) {
								html += '<tr>';
									html += '<th>Fakultas</th><td>:</td><td>'+ d.fakultas +'</td>';
								html += '</tr>';
							}

						html += '</table>';
					html += '</td>';
				html += '</tr>';
			html += '</table>';

			return html;
		};


		/**
		 * data list / datatable handler
		 */
		var table = $('#data-list').DataTable({
			ordering : true,
			order : [[ 2, "asc" ]],
			stateSave : true,
			processing : true,
			serverSide : true,
			ajax : '<?= site_url("administrator/manage_user/dataTable") ?>',
			columns : [
				{ 
					"orderable": false,
					"data": null,
					"defaultContent": "<input type='checkbox' name='id[]' value='' />"
				},
				{
					"className"      : 'details-control',
					"orderable"      : false,
					"data"           : null,
					"defaultContent" : '<i class="fa fa-fw fa-plus-circle"></i> <i class="fa fa-fw fa-minus-circle"></i>'
				},
				{ "data": "full_name" },
				{ "data": "username" },
				{ "data": "group_alias" },
				{
					"orderable": false,
					"data": "user_status",
					"mRender": function(data, type, full) {
						if ( data == 'active' ) {
							html = '<span class="label label-success">AKTIF</span>';
						}
						else if ( data == 'inactive' ) {
							html = '<span class="label label-warning">TIDAK AKTIF</span>';
						}
						else {
							html = '<span class="label label-danger">DIBLOKIR</span>';
						}

						return html;
					}
				},
				{
					"orderable": false,
					"data": "lock",
					"mRender": function(data, type, full) {
						if ( data == false ) {
							html = '<a href="javascript:void(0)" class="lock text-success"><i class="fa fa-fw fa-unlock"></i></a>';
						}
						else {
							html = '<a href="javascript:void(0)" class="lock text-danger"><i class="fa fa-fw fa-lock"></i></a>';
						}

						return html;
					}
				},
				{
					"orderable": false,
					"data": null,
					"defaultContent": "<a href='javascript:void(0)' class='login-as'>login sebagai</a>"
				},
				{
					"orderable": false,
					"data": null,
					"defaultContent": "<a href='javascript:void(0)' class='reset-password'>reset password</a>"
				},
				{
					"orderable": false,
					"data": null,
					"defaultContent": "<a href='javascript:void(0)' class='data-edit-modal'>edit</a> | <a href='javascript:void(0)' class='data-remove-modal alertify-confirm'>hapus</a>"
				},
			],
		});


		// Add event listener for opening and closing details
		$('#data-list tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format(row.data()) ).show();
				tr.addClass('shown');
			}
		});


		// lock/unlock user access
		$('#data-list tbody').on('click', '.lock', function() {
			$row = table.row( $(this).parents('tr') );
			var data = table.row( $(this).parents('tr') ).data();
			var uid = data.uid;

			$lock_toggle_btn = $(this);

			alertify.set({
				labels : {
					ok : '<i class="fa fa-fw fa-check-circle"></i> Ya',
					cancel : '<i class="fa fa-fw fa-times-circle"></i> Tidak'
				}
			});
			alertify.confirm('Apakah anda yakin akan mengunci akses?', function(e) {
				if (e) {
					$.ajax({
						url : '<?= site_url("administrator/manage_user/toggle_lock") ?>',
						dataType : 'json',
						data : { uid : uid },
						success : function(response) {
							if (response.result == false) {
								console.log(response);
								alertify.error(response.message);
							}
							else {
								if (response.lock == false) {
									$lock_toggle_btn.removeClass('text-danger');
									$lock_toggle_btn.addClass('text-success');
									$lock_toggle_btn.html('<i class="fa fa-fw fa-unlock"></i>');
								}
								else {
									$lock_toggle_btn.removeClass('text-success');
									$lock_toggle_btn.addClass('text-danger');
									$lock_toggle_btn.html('<i class="fa fa-fw fa-lock"></i>');
								}
							}
						},
						error : function(error) {
							console.log(error);
							alert('Error occured. Tidak dapat menghubungi server.')
						}
					})
				}
				else {
					return;
				}
			})
		})


		/**
		 * login as handler
		 */
		$('#data-list tbody').on('click', '.login-as', function() {
			$row = table.row( $(this).parents('tr') );
			var data = table.row( $(this).parents('tr') ).data();
			var uid = data.uid;

			// set alertify label
			alertify.set({
				labels : {
					ok : '<i class="fa fa-fw fa-check-circle"></i> Ya',
					cancel : '<i class="fa fa-fw fa-times-circle"></i> Tidak'
				}
			});
			alertify.confirm('Apakah anda yakin akan login sebagai user ini?', function(e) {
				if ( e ) {
					$.ajax({
						url : '<?= site_url("administrator/manage_user/login_as") ?>',
						dataType : 'json',
						data : { uid : uid },
						success : function(response) {
							if ( response.result == false ) {
								console.log(response);
								alertify.error(response.message);
							}
							else {
								window.location.assign(response.dst);
							}
						}
					});
				}
				else {
					return;
				}
			});
		});


		/**
		 * reset password handler
		 */
		$('#data-list tbody').on('click', '.reset-password', function() {
			$row = table.row( $(this).parents('tr') );
			var data = table.row( $(this).parents('tr') ).data();
			var uid = data.uid;

			// set alertify label
			alertify.set({
				labels : {
					ok : '<i class="fa fa-fw fa-check-circle"></i> Ya',
					cancel : '<i class="fa fa-fw fa-times-circle"></i> Tidak'
				}
			});
			alertify.confirm('Apakah anda yakin akan mereset password user ini?', function(e) {
				if ( e ) {
					$.ajax({
						url : '<?= site_url("administrator/manage_user/reset_password") ?>',
						dataType : 'json',
						data : { uid : uid },
						success : function(response) {
							if ( response.result == false ) {
								console.log(response);
								alertify.error(response.message);
							}
							else {
								// set alertify label
								alertify.set({ labels : { ok : 'OK' } });
								alertify.alert('Password baru anda adalah <b>'+response.new_password+'</b>');
							}
						}
					});
				}
				else {
					return;
				}
			});
		});

		
		/**
		 * data add form handler
		 */
		$('#data-add-form').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				if ( $form.parsley().isValid() ) {
					$('#data-add-form [type="submit"]').button('loading');

					return true;
				}

				return false;
			},
			success: function(response) {
				if ( response.result == false ) {
					alertify.error(response.message);
				} else {
					$('#data-add-form').resetForm();
					$('#data-add-modal').modal('hide');

					table.ajax.reload(null, false);
				}
			},
			error: function(error) {
				alertify.error('Error occured. Tidak dapat menghubungi server.');
			},
			complete: function() {
				$('#data-add-form [type="submit"]').button('reset');
			}
		});

		/**
		 * data edit modal handler
		 */
		$('#data-list tbody').on('click', '.data-edit-modal', function() {
			$row = table.row( $(this).parents('tr') );

			var data = table.row( $(this).parents('tr') ).data();

			$modal = $('#data-edit-modal');

			var uid = data.uid;

			// get data
			$.ajax({
				url : '<?= site_url("administrator/manage_user/get_by_id") ?>',
				dataType : 'json',
				data : { uid : uid },
				success : function(response) {
					if ( response.result == false ) {
						alertify.error(response.message);
					}
					else {
						var data = response.data;
						
						$modal.find('[name="uid"]').val(data.uid);
						$modal.find('[name="first_name"]').val(data.first_name);
						$modal.find('[name="last_name"]').val(data.last_name);
						$modal.find('[name="email"]').val(data.email);
						$modal.find('[name="username"]').val(data.username);
						$modal.find('[name="user_status"]').val(data.user_status);
						$modal.find('[name="group_id"]').val(data.group_id);
						$modal.find('[name="fakultas_id"]').val(data.fakultas_id);

						$modal.modal('show');
					}
				},
				error: function() {
					alertify.error('Error occured. Tidak dapat menghubungi server.');
				}
			});
		});

		
		/**
		 * data edit form handler
		 */
		$('#data-edit-form').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				if ( $form.parsley().isValid() ) {
					$('#data-edit-form [type="submit"]').button('loading');

					return true;
				}

				return false;
			},
			success: function(response) {
				if ( response.result == false ) {
					alertify.error(response.message);
				}
				else {
					$('#data-edit-form').resetForm();
					$('#data-edit-modal').modal('hide');

					table.ajax.reload(null,false);
				}
			},
			error: function(error) {
				alertify.error('Error occured. Tidak dapat menghubungi server.');
			},
			complete: function() {
				$('#data-edit-form [type="submit"]').button('reset');
			}
		});


		/**
		 * data remove handler
		 */
		$('#data-list tbody').on('click', '.data-remove-modal', function() {
			var data = table.row( $(this).parents('tr') ).data();
			var uid = data.uid;

			// set alertify label
			alertify.set({
				labels : {
					ok : '<i class="fa fa-fw fa-check-circle"></i> Ya',
					cancel : '<i class="fa fa-fw fa-times-circle"></i> Tidak'
				}
			});
			alertify.confirm('Apakah anda yakin akan menghapus data?', function(e) {
				if ( e ) {
					// remove data
					$.ajax({
						url : '<?= site_url("administrator/manage_user/remove") ?>',
						dataType : 'json',
						data : { uid : uid },
						success : function(r) {
							if ( r.metadata.code !== "200" ) {
								console.log(r);
								alertify.error(r.metadata.message);
							}
							else {
								alertify.success('Berhasil menghapus user');
								table.ajax.reload(null, false);
							}
						}
					});
				}
				else {
					return;
				}
			});
		});
	});
</script>