<div class="modal fade" id="data-remove-modal">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- set the hidden input to store data's id temporary -->
			<?= form_hidden('id', ''); ?>

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Menghapus Data</h4>
			</div>
			<div class="modal-body">
				Apakah anda yakin akan menghapus data?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-round btn-sm btn-default" data-dismiss="modal"><i class="fa fa-fw fa-times-circle"></i> Tidak</button>
				<button type="button" class="btn btn-round btn-sm btn-dark" id="confirm-remove-yes"><i class="fa fa-fw fa-check-circle"></i> Ya</button>
			</div>
		</div>
	</div>
</div>