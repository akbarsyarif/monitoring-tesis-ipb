<div class="modal fade" id="data-edit-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<?= form_open('administrator/manage_user/update', 'id="data-edit-form" data-parsley-validate=""'); ?>
				<?= form_hidden('uid', ''); ?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><?= $page_title ?> <small>( Edit )</small></h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<?= form_label('Nama Depan *', 'first_name'); ?>
								<?= form_input('first_name', '', 'class="form-control" id="first_name" required=""') ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<?= form_label('Nama Belakang', 'last_name'); ?>
								<?= form_input('last_name', '', 'class="form-control" id="last_name"') ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<?= form_label('Email *', 'email'); ?>
								<?= form_input('email', '', 'class="form-control" id="email" required="" type="email" data-parsley-type="email"') ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<?= form_label('Username *', 'username'); ?>
								<?= form_input('username', '', 'class="form-control" id="username" required="true"') ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<?= form_label('User Status *', 'user_status'); ?>
						<?= form_dropdown('user_status', array(
							'active' => 'Aktif',
							'inactive' => 'Tidak Aktif',
							'banned' => 'Blokir'
						), '', 'class="form-control" id="user_status" required="true"'); ?>
					</div>
					<div class="form-group">
						<?= form_label('Grup *', 'group_id'); ?>
						<?= dinamyc_dropdown(array(
							'name' => 'group_id',
							'table' => 'groups',
							'key' => 'id',
							'label' => 'group_alias',
							'default' => '',
							'empty_first' => true,
							'empty_first_label' => '[ Pilih Grup ]',
							'attr' => 'class="form-control" required="true" id="group_id"'
						)) ?>
					</div>
					<div class="group-detail operator-fakultas" style="display:none">
						<div class="form-group">
							<?= form_label('Fakultas', 'fakultas_id') ?>
							<?= dinamyc_dropdown(array(
								'name' => 'fakultas_id',
								'table' => 'fakultas',
								'key' => 'id',
								'label' => 'fakultas',
								'default' => '',
								'empty_first' => true,
								'empty_first_label' => '[ Pilih Fakultas ]',
								'attr' => 'class="form-control" id="fakultas_id"'
							)) ?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" data-loading-text="<i class='fa fa-fw fa-spin fa-circle-o-notch'></i> Sedang memproses..." class="btn btn-sm btn-round btn-dark"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
					<button type="button" class="btn btn-sm btn-round btn-default" data-dismiss="modal"><i class="fa fa-fw fa-times-circle"></i> Batal</button>
				</div>
			<?= form_close(); ?>
		</div>
	</div>
</div>

<!-- javascript -->
<script type="text/javascript">
	$(document).ready(function() {
		// -----------------------------------------------------------------------------

		$('#data-edit-modal').on('show.bs.modal', function() {
			showOperatorFakultasDetail();
		})

		// -----------------------------------------------------------------------------

		$('#data-edit-form [name="group_id"]').on('change', function() {
			showOperatorFakultasDetail();
		});

		// -----------------------------------------------------------------------------

		function showOperatorFakultasDetail()
		{
			var select = $('#data-edit-form [name="group_id"]');
			var group = select.val();
			var group_detail = $('.group-detail');
			var operator_fakultas = $('.operator-fakultas');

			group_detail.hide();

			unvalidateOperatorFakultasDetail();

			if ( group == "2" ) {
				operator_fakultas.show();

				validateOperatorFakultasDetail();
			}
		}

		// -----------------------------------------------------------------------------

		function validateOperatorFakultasDetail()
		{
			var operator_fakultas = $('.operator-fakultas');

			operator_fakultas.find('[name="fakultas_id"]').addClass('required');
		}

		// -----------------------------------------------------------------------------

		function unvalidateOperatorFakultasDetail()
		{
			var operator_fakultas = $('.operator-fakultas');

			operator_fakultas.find('[name="fakultas_id"]').removeClass('required');
		}

		// -----------------------------------------------------------------------------
	});
</script>