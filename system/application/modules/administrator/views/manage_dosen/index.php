<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="pull-left">
					<button class="btn btn-round btn-sm btn-dark" data-toggle="modal" href='#data-add-modal'><i class="fa fa-fw fa-plus-circle"></i> Tambah</button>
				</div>
				<div class="pull-right">
					<!-- put something here -->
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped" id="data-list" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th width="10"><?= form_checkbox('ids', ''); ?></th>
							<th>Nip</th>
							<th>Nama</th>
							<th>Kode</th>
							<th>Email</th>
							<th width="100"></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<?php require 'data_add_modal.php' ?>
<?php require 'data_edit_modal.php' ?>

<!-- javascript -->
<script type="text/javascript">
	$(document).ready(function() {
		/**
		 * data list / datatable handler
		 */
		var table = $('#data-list').DataTable({
			ordering : true,
			order : [[ 2, "asc" ]],
			stateSave : false,
			processing : true,
			serverSide : true,
			ajax : '<?= site_url("administrator/manage_dosen/get/datatable") ?>',
			columns : [
				{
					"orderable": false,
					"data": null,
					"defaultContent": "<input type='checkbox' name='id[]' value='' />"
				},
				{ "data": "nip" },
				{ "data": "nama" },
				{ "data": "kode" },
				{ "data": "email" },
				{
					"orderable": false,
					"data": null,
					"defaultContent": "<a href='javascript:void(0)' class='data-edit-modal'>edit</a> | <a href='javascript:void(0)' class='data-remove-modal'>hapus</a>"
				},
			],
		});


		/**
		 * data add modal handler
		 */
		$('#data-add-form').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				if ( $form.parsley().isValid() ) {
					return true;
				}

				return false;
			},
			success: function(response) {
				if ( response.result == false ) {
					console.log(response);
					alertify.error(response.message);
				} else {
					$('#data-add-form').resetForm();
					$('#data-add-modal').modal('hide');
					alertify.success('Berhasil menyimpan data');
					table.ajax.reload();
				}
			}
		});


		/**
		 * data edit modal handler
		 */
		$('#data-list tbody').on('click', '.data-edit-modal', function() {
			$row = table.row( $(this).parents('tr') );
			var data = table.row( $(this).parents('tr') ).data();
			$modal = $('#data-edit-modal');
			var id = data.id;

			// get data
			$.ajax({
				url : '<?= site_url("administrator/manage_dosen/get/json") ?>/'+id,
				dataType : 'json',
				success : function(response) {
					if ( response.result !== true ) {
						console.log(response);
						alertify.error(response.message);
					} else {
						// show modal
						$modal.modal('show');

						var data = response.data;
						
						$modal.find('[name="id"]').val(data.id);
						$modal.find('[name="nip"]').val(data.nip);
						$modal.find('[name="nama"]').val(data.nama);
						$modal.find('[name="kode"]').val(data.kode);
						$modal.find('[name="email"]').val(data.email);
					}
				}
			});
		});


		/**
		 * data edit form handler
		 */
		$('#data-edit-form').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				if ( $form.parsley().isValid() ) {
					return true;
				}

				return false;
			},
			success: function(response) {
				if ( response.result == false ) {
					console.log(response);
					alertify.error(response.message);
				} else {
					$('#data-edit-form').resetForm();
					$('#data-edit-modal').modal('hide');

					// $row.data(newData).draw(false);
					table.ajax.reload(null,false);
				}
			}
		});


		/**
		 * data remove handler
		 */
		$('#data-list tbody').on('click', '.data-remove-modal', function() {
			var data = table.row( $(this).parents('tr') ).data();
			var id = data.id;

			// set alertify label
			alertify.set({
				labels : {
					ok : '<i class="fa fa-fw fa-check-circle"></i> Ya',
					cancel : '<i class="fa fa-fw fa-times-circle"></i> Tidak'
				}
			});
			alertify.confirm('Apakah anda yakin akan menghapus data?', function(e) {
				if ( e ) {
					// remove data
					$.ajax({
						url : '<?= site_url("administrator/manage_dosen/remove") ?>',
						dataType : 'json',
						data : { id : id },
						success : function(response) {
							if ( response.result !== true ) {
								console.log(response);
								alertify.error(response.message);
							} else {
								// remova row from table
								table.row( $(this).parents('tr') ).remove().draw(false);
							}
						}
					});
				}
				else {
					return;
				}
			});
		});
	});
</script>