<div class="modal fade" id="data-add-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<?= form_open('administrator/manage_dosen/save', 'id="data-add-form" data-parsley-validate=""'); ?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><?= $page_title ?> <small>( Tambah )</small></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<?= form_label('NIP *'); ?>
						<?= form_input(array(
							'name' => 'nip',
							'value' => '',
							'class' => 'form-control'
						)) ?>
					</div>
					<div class="form-group">
						<?= form_label('Nama *'); ?>
						<?= form_input(array(
							'name' => 'nama',
							'value' => '',
							'class' => 'form-control required'
						)) ?>
					</div>
					<div class="form-group">
						<?= form_label('Kode'); ?>
						<?= form_input(array(
							'name' => 'kode',
							'value' => '',
							'class' => 'form-control'
						)) ?>
					</div>
					<div class="form-group">
						<?= form_label('Email'); ?>
						<?= form_input(array(
							'name' => 'email',
							'value' => '',
							'class' => 'form-control'
						)) ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-round btn-default" data-dismiss="modal"><i class="fa fa-fw fa-times-circle"></i> Batal</button>
					<button type="submit" class="btn btn-sm btn-round btn-dark"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				</div>
			<?= form_close(); ?>
		</div>
	</div>
</div>