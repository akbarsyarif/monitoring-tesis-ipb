<div class="modal fade" id="data-add-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<?= form_open('administrator/manage_program_studi/save', 'id="data-add-form" data-parsley-validate=""'); ?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Tambah Program Studi</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<?= form_label('Program Studi') ?>
						<?= form_input(array(
							'name' => 'program_studi',
							'value' => '',
							'class' => 'form-control required'
						)) ?>
					</div>
					<div class="form-group">
						<div class="form-group">
							<?= form_label('Fakultas', 'fakultas_id') ?>
							<?= dinamyc_dropdown(array(
								'name' => 'fakultas_id',
								'table' => 'fakultas',
								'key' => 'id',
								'label' => 'fakultas',
								'default' => '',
								'order_by' => 'fakultas asc',
								'empty_first' => TRUE,
								'empty_first_label' => '-- Pilih Fakultas --',
								'attr' => 'class="form-control required"'
							)) ?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-round btn-default" data-dismiss="modal"><i class="fa fa-fw fa-times-circle"></i> Batal</button>
					<button type="submit" class="btn btn-sm btn-round btn-dark"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				</div>
			<?= form_close(); ?>
		</div>
	</div>
</div>