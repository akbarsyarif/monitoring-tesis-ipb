<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="pull-left">
					<button class="btn btn-round btn-sm btn-dark" data-toggle="modal" href='#data-add-modal'><i class="fa fa-fw fa-plus-circle"></i> Tambah</button>
					<button class="btn btn-round btn-sm btn-info" id="advance-search-toggle-btn"><i class="fa fa-fw fa-search-plus"></i> Filter Data</button>
				</div>
				<div class="pull-right">
					<!-- put something here -->
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<!-- advance search -->
				<div class="advance-search-form well">
					<h4 class="title"><i class="fa fa-fw fa-search-plus"></i> Filter Data</h4>
					<?= form_open('', array(
						'class' => 'form-horizontal',
						'id' => 'filter_form',
						'onsubmit' => 'return false'
					)); ?>
						<div class="form-group">
							<?= form_label('Program Studi', '', array('class' => 'control-label col-md-2')) ?>
							<div class="col-md-10">
								<?= form_input(array(
									'name' => 'program_studi',
									'value' => '',
									'class' => 'form-control',
									'data-column' => "1"
								)) ?>
							</div>
						</div>
						<div class="form-group">
							<?= form_label('Fakultas', '', array('class' => 'control-label col-md-2')); ?>
							<div class="col-md-10">
								<?= dinamyc_dropdown(array(
									'name' => 'fakultas',
									'table' => 'fakultas',
									'key' => 'fakultas',
									'label' => 'fakultas',
									'default' => '',
									'order_by' => 'fakultas asc',
									'empty_first' => TRUE,
									'empty_first_label' => '-- Semua --',
									'attr' => 'class="form-control select2-single" data-column="2"'
								)) ?>
							</div>
						</div>
						<div class="col-sm-offset-2 col-sm-10 footer">
							<button type="button" class="btn btn-warning btn-sm btn-round" id="clear_filter_btn"><i class="fa fa-fw fa-times-circle"></i> Clear Filter</button>
							<button type="submit" class="btn btn-dark btn-sm btn-round" id="filter_btn"><i class="fa fa-fw fa-check-circle"></i> Filter</button>
						</div>
					<?= form_close(); ?>
				</div>

				<table class="table table-striped" id="data-list" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th width="10"><?= form_checkbox('ids', ''); ?></th>
							<th>Program Studi</th>
							<th>Fakultas</th>
							<th width="10"></th>
							<th width="10"></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<?php require 'data_add_modal.php' ?>
<?php require 'data_edit_modal.php' ?>

<!-- javascript -->
<script type="text/javascript">
	$(document).ready(function() {
		// ---------------------------------------------------------------------
		/**
		 * data list / datatable handler
		 */
		var table = $('#data-list').DataTable({
			ordering : true,
			order : [[1,'asc']],
			pageLength : 10,
			stateSave : false,
			processing : true,
			serverSide : true,
			ajax : '<?= site_url("administrator/manage_program_studi/get/datatable") ?>',
			columns : [
				{
					"orderable": false,
					"data": null,
					"defaultContent": "<input type='checkbox' name='id[]' value='' />"
				},
				{ "data": "program_studi" },
				{ "data": "fakultas" },
				{
					"data": null,
					"orderable": false,
					"mRender": function(data, type, row) {
						return '<a href="javascript:void(0)" class="data-edit-modal">edit</a>';
					}
				},
				{
					"data": null,
					"orderable": false,
					"mRender": function(data, type, row) {
						return '<a href="javascript:void(0)" class="data-remove-modal">hapus</a>';
					}
				},
			],
		});

		// ---------------------------------------------------------------------

		// filter usulan
		$('#filter_form').on('submit', function() {
			$(this).find('.form-control').each(function() {
				var i = $(this).attr('data-column');

				table.column(i).search($(this).val());
			});

			table.draw();
		});

		// ---------------------------------------------------------------------

		// clear filter
		$('#clear_filter_btn').on('click', function() {
			$('#filter_form').resetForm();

			$('#filter_form .form-control').each(function() {
				var i = $(this).attr('data-column');

				table.column(i).search($(this).val());
			});

			table.draw();

			$('#filter_form .select2-single').trigger('change');
		});

		// ---------------------------------------------------------------------

		/**
		 * data add modal handler
		 */
		$('#data-add-form').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				$('#data-add-form [type="submit"]').button('loading');

				if ( $form.parsley().isValid() ) {
					return true;
				}

				return false;
			},
			success: function(r) {
				if ( r.metadata.code != "200" ) {
					alertify.error(r.metadata.message);
					console.log(r);
				}
				else {
					alertify.success('Berhasil menyimapn data');
					$('#data-add-form').resetForm();
					$('#data-add-modal').modal('hide');
					table.ajax.reload(null, false);
				}
			},
			error: function(e) {
				alertify.error('Error 400 Bad Request');
				console.log(e);
			},
			complete: function() {
				$('#data-add-form [type="submit"]').button('reset');
			}
		});

		// ---------------------------------------------------------------------

		/**
		 * data edit modal handler
		 */
		$('#data-list tbody').on('click', '.data-edit-modal', function() {
			$row = table.row( $(this).parents('tr') );
			var data = table.row( $(this).parents('tr') ).data();
			$modal = $('#data-edit-modal');
			var id = data.id;

			// get data
			$.ajax({
				url : '<?= site_url("administrator/manage_program_studi/get/json") ?>/'+id,
				dataType : 'json',
				success : function(r) {
					if ( r.metadata.code != "200" ) {
						alertify.error(r.metadata.message);
						console.log(r);
					}
					else {
						var d = r.response;

						$('#data-edit-form').find('[name="id"]').val(d.id);
						$('#data-edit-form').find('[name="program_studi"]').val(d.program_studi);
						$('#data-edit-form').find('[name="fakultas_id"]').val(d.fakultas_id);

						$('#data-edit-modal').modal('show');
					}
				},
				error: function(e) {
					alertify.error('Error 400 Bad Request');
					console.log(e);
				},
				complete: function() {

				}
			});
		});

		/**
		 * data edit form handler
		 */
		$('#data-edit-form').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				$('#data-edit-form [type="submit"]').button('loading');

				if ( $form.parsley().isValid() ) {
					return true;
				}

				return false;
			},
			success: function(r) {
				if ( r.metadata.code != "200" ) {
					alertify.error(r.metadata.message);
					console.log(r);
				}
				else {
					$('#data-edit-form').resetForm();
					$('#data-edit-modal').modal('hide');
					table.ajax.reload(null, false);
				}
			},
			error: function(e) {
				alertify.error('Error 400 Bad Request');
				console.log(e);
			},
			complete: function() {
				$('#data-edit-form [type="submit"]').button('reset');
			}
		});

		// ---------------------------------------------------------------------

		/**
		 * data remove handler
		 */
		$('#data-list tbody').on('click', '.data-remove-modal', function() {
			var data = table.row( $(this).parents('tr') ).data();
			var id = data.id;

			// set alertify label
			alertify.set({
				labels : {
					ok : '<i class="fa fa-fw fa-check-circle"></i> Ya',
					cancel : '<i class="fa fa-fw fa-times-circle"></i> Tidak'
				}
			});
			alertify.confirm('Apakah anda yakin akan menghapus data?', function(e) {
				if ( e ) {
					// remove data
					$.ajax({
						url : '<?= site_url("administrator/manage_mahasiswa/remove") ?>',
						dataType : 'json',
						data : { id : id },
						success : function(response) {
							if ( response.result !== true ) {
								console.log(response);
								alertify.error(response.message);
							} else {
								// remova row from table
								table.row( $(this).parents('tr') ).remove().draw(false);
							}
						}
					});
				}
				else {
					return;
				}
			});
		});

		// ---------------------------------------------------------------------
	});
</script>