<div class="modal fade" id="data-add-modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<?= form_open('administrator/manage_mahasiswa/save', 'id="data-add-form" data-parsley-validate=""'); ?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><?= $page_title ?> <small>( Tambah )</small></h4>
				</div>
				<div class="modal-body">
					<fieldset>
						<legend>Data Mahasiswa</legend>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<?= form_label('NRP *'); ?>
									<?= form_input(array(
										'name' => 'nrp',
										'value' => '',
										'class' => 'form-control required'
									)) ?>
								</div>
								<div class="form-group">
									<?= form_label('Nama *'); ?>
									<?= form_input(array(
										'name' => 'nama',
										'value' => '',
										'class' => 'form-control required'
									)) ?>
								</div>
								<div class="form-group">
									<?= form_label('Angkatan'); ?>
									<?= form_input(array(
										'name' => 'angkatan',
										'value' => '',
										'class' => 'form-control'
									)) ?>
								</div>
								<div class="form-group">
									<?= form_label('Tgl.Masuk'); ?>
									<?= form_input(array(
										'name' => 'tanggal_masuk',
										'value' => date('Y-m-d'),
										'class' => 'form-control datepicker'
									)) ?>
								</div>
								<div class="form-group">
									<?= form_label('Fakultas *'); ?>
									<?= dinamyc_dropdown(array(
										'name' => 'fakultas_id',
										'table' => 'fakultas',
										'key' => 'id',
										'default' => '',
										'label' => 'fakultas',
										'empty_first' => true,
										'empty_first_label' => '- Pilih Fakultas -',
										'attr' => 'class="form-control required"'
									)) ?>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<?= form_label('Program Studi'); ?>
									<?= form_input(array(
										'name' => 'program_studi',
										'value' => '',
										'class' => 'form-control'
									)) ?>
								</div>
								<div class="form-group">
									<?= form_label('Kelas *'); ?>
									<?= form_dropdown(array(
										'name' => 'kelas',
										'options' => array(
											'reguler' => 'Reguler',
											'khusus' => 'Khusus',
										),
										'selected' => 'reguler',
										'class' => 'form-control required'
									)) ?>
								</div>
								<div class="form-group">
									<?= form_label('Semester *'); ?>
									<?= form_input(array(
										'name' => 'semester',
										'value' => '',
										'class' => 'form-control input-mask-integer required'
									)) ?>
								</div>
								<div class="form-group">
									<?= form_label('Perguruan Tinggi Asal'); ?>
									<?= form_input(array(
										'name' => 'pt_asal',
										'value' => '',
										'class' => 'form-control'
									)) ?>
								</div>
								<div class="form-group">
									<?= form_label('Nama Instansi'); ?>
									<?= form_input(array(
										'name' => 'instansi',
										'value' => '',
										'class' => 'form-control'
									)) ?>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<?= form_label('No. Telepon Instansi'); ?>
									<?= form_input(array(
										'name' => 'telp_instansi',
										'value' => '',
										'class' => 'form-control'
									)) ?>
								</div>
								<div class="form-group">
									<?= form_label('No. HP'); ?>
									<?= form_input(array(
										'name' => 'no_hp',
										'value' => '',
										'class' => 'form-control'
									)) ?>
								</div>
								<div class="form-group">
									<?= form_label('Email'); ?>
									<?= form_input(array(
										'name' => 'email',
										'value' => '',
										'class' => 'form-control'
									)) ?>
								</div>
								<div class="form-group">
									<?= form_label('Status *'); ?>
									<?= form_dropdown(array(
										'name' => 'status',
										'options' => array(
											'1' => 'Aktif (Belum Tesis)',
											'2' => 'Aktif (Sedang Tesis)',
											'3' => 'Lulus',
											'4' => 'DO',
											'5' => 'Mengundurkan Diri',
										),
										'selected' => '1',
										'class' => 'form-control required'
									)); ?>
								</div>
								<div class="form-group">
									<?= form_label('Tanggal Lulus'); ?>
									<?= form_input(array(
										'name' => 'tanggal_lulus',
										'value' => '',
										'class' => 'form-control datepicker',
										'placeholder' => 'Diisi bila status mahasiswa lulus'
									)) ?>
								</div>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Data Tesis</legend>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<?= form_label('Judul Tugas Akhir'); ?>
									<?= form_input(array(
										'name' => 'judul_ta',
										'value' => '',
										'class' => 'form-control required'
									)) ?>
								</div>
								<div class="form-group">
									<?= form_label('Dosen Penguji *'); ?>
									<?= dinamyc_dropdown(array(
										'name' => 'dosen_id_penguji',
										'table' => 'dosen',
										'key' => 'id',
										'label' => 'nama',
										'default' => '',
										'empty_first' => true,
										'empty_first_label' => '- Pilih Dosen -',
										'attr' => 'class="form-control select2-single required"'
									)) ?>
								</div>
								<div class="form-group">
									<?= form_label('Dosen Pembimbing 1 *'); ?>
									<?= dinamyc_dropdown(array(
										'name' => 'dosen_id_pembimbing_1',
										'table' => 'dosen',
										'key' => 'id',
										'label' => 'nama',
										'default' => '',
										'empty_first' => true,
										'empty_first_label' => '- Pilih Dosen -',
										'attr' => 'class="form-control select2-single required"'
									)) ?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<?= form_label('Dosen Pembimbing 2 *'); ?>
									<?= dinamyc_dropdown(array(
										'name' => 'dosen_id_pembimbing_2',
										'table' => 'dosen',
										'key' => 'id',
										'label' => 'nama',
										'default' => '',
										'empty_first' => true,
										'empty_first_label' => '- Pilih Dosen -',
										'attr' => 'class="form-control select2-single required"'
									)) ?>
								</div>
								<div class="form-group">
									<?= form_label('Dosen Pembimbing 3'); ?>
									<?= dinamyc_dropdown(array(
										'name' => 'dosen_id_pembimbing_3',
										'table' => 'dosen',
										'key' => 'id',
										'label' => 'nama',
										'default' => '',
										'empty_first' => true,
										'empty_first_label' => '- Pilih Dosen -',
										'attr' => 'class="form-control select2-single"'
									)) ?>
								</div>
							</div>
						</div>
					</fieldset>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-round btn-default" data-dismiss="modal"><i class="fa fa-fw fa-times-circle"></i> Batal</button>
					<button type="submit" class="btn btn-sm btn-round btn-dark"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				</div>
			<?= form_close(); ?>
		</div>
	</div>
</div>