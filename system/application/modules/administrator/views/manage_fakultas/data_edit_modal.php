<div class="modal fade" id="data-edit-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<?= form_open('administrator/manage_fakultas/update', 'id="data-edit-form" data-parsley-validate=""'); ?>
				<?= form_hidden('id', ''); ?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><?= $page_title ?> <small>( Edit )</small></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<?= form_label('Fakultas *'); ?>
						<?= form_input(array(
							'name' => 'fakultas',
							'value' => '',
							'class' => 'form-control required'
						)) ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-round btn-default" data-dismiss="modal"><i class="fa fa-fw fa-times-circle"></i> Batal</button>
					<button type="submit" class="btn btn-sm btn-round btn-dark"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				</div>
			<?= form_close(); ?>
		</div>
	</div>
</div>