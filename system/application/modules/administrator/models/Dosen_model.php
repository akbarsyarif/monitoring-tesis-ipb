<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dosen_model extends MY_Model {

	public $table = 'dosen';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
	}

	// get all with manual query
	public function get_all_manual($query=array())
	{
		if ( isset($query['where']) ) $this->db->where($query['where']);
		if ( isset($query['where_no_escape']) ) $this->db->where($query['where_no_escape'], null, false);
		if ( isset($query['or_where']) ) $this->db->or_where($query['or_where']);
		if ( isset($query['or_where_no_escape']) ) $this->db->or_where($query['or_where_no_escape'], null, false);
		if ( isset($query['limit']) ) $this->db->limit($query['limit']);
		if ( isset($query['limit_offset']) ) $this->db->limit($query['limit_offset'][0], $query['limit_offset'][1]);
		if ( isset($query['group_by']) ) $this->db->group_by($query['group_by']);
		
		if ( isset($query['join']) ) $this->db->join($query['join'][0], $query['join'][1]);
		if ( isset($query['left_join']) ) $this->db->join($query['left_join'][0], $query['left_join'][1], 'left');
		if ( isset($query['right_join']) ) $this->db->right($query['right_join'][0], $query['right_join'][1], 'right');

		if ( isset($query['order_by']) ) {

			if ( is_array($query['order_by']) ) {

				foreach ( $query['order_by'] as $column => $dir ) {

					$this->db->order_by($column, $dir);

				}

			} else {

				$this->db->order_by($query['order_by']);

			}

		}

		$this->db
			 ->select("{$this->table}.id, {$this->table}.nip, {$this->table}.nama, {$this->table}.kode, {$this->table}.email, {$this->table}.created_at, {$this->table}.updated_at, {$this->table}.deleted_at")
			 ->from($this->table);

		$query = $this->db->get();

		if ( $query->num_rows() > 0 ) {

			$data = $query->result();

			return $data;

		}

		return false;
	}

	public function get_manual($query=array())
	{
		$result = $this->get_all_manual($query);

		if ( $result != false ) {

			return $result[0];

		}

		return false;
	}

	/**
	 * validate nip
	 * cek apakah nip dosen sudah digunakan
	 * @param nip
	 * @param id (cari semua dosen kecuali dosen dengan id ini)
	 * @return boolean
	 */
	public function validate_nip($nip, $id=null)
	{
		$this->where('nip', $nip);

		if ( ! is_null($id) ) {

			$this->where('id !=', $id);

		}

		$result = $this->get();

		if ( $result == false ) {

			return true;

		}

		return false;
	}

}

/* End of file sudut_padang_model.php */
/* Location: ./application/models/sudut_padang_model.php */