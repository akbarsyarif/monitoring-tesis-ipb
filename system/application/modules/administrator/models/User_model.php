<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model {

	public $table = 'users';
	public $primary_key = 'id';

	public function __construct()
	{

		// relationships
		$this->has_many_pivot['groups'] = array(
			'foreign_model' => 'group_model',
			'pivot_table' => 'user_groups',
			'local_key' => 'id',
			'pivot_local_key' => 'user_id',
			'foreign_key' => 'id',
			'pivot_foreign_key' => 'group_id',
			'get_relate' => true
		);

		parent::__construct();
		
	}


	public function get($query=array())
	{

		if ( isset($query['where']) ) $this->db->where($query['where']);
		if ( isset($query['or_where']) ) $this->db->or_where($query['or_where']);
		if ( isset($query['limit']) ) $this->db->limit($query['limit']);
		if ( isset($query['limit_offset']) ) $this->db->limit($query['limit_offset'][0], $query['limit_offset'][1]);

		$this->db 
		     ->select('users.id as uid, users.first_name, users.last_name, users.username, users.email, users.profile_picture, users.fakultas_id, users.user_status, users.lock, users.created_at')
		     ->select('groups.group_name, groups.group_alias')
		     ->select('user_groups.user_id, user_groups.group_id')
		     ->select('fakultas.fakultas')
		     ->from('users')
		     ->join('user_groups', 'user_groups.user_id = users.id')
		     ->join('groups', 'groups.id = user_groups.group_id')
		     ->join('fakultas', 'fakultas.id = users.fakultas_id', 'left')
		     ->where('users.id !=', 1)
		     ->where('users.created_at !=', null)
		     ->order_by('groups.group_name', 'asc')
		     ->order_by('users.first_name', 'asc');
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ) {

			$result = $query->result();

			for ($i=0; $i < count($result); $i++) { 
				
				$result[$i]->full_name = $result[$i]->first_name . ' ' . $result[$i]->last_name;

			}

			return $result;

		}

		return FALSE;

	}

	public function row($query=array())
	{

		$result = $this->get($query);

		if ( $result !== FALSE ) {

			return $result[0];

		}

		return FALSE;

	}

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */