<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Option_model extends MY_Model {

	public $table = 'options';
	public $primary_key = 'option_name';

	public function __construct()
	{
		
		parent::__construct();
		
	}

}

/* End of file Option_model.php */
/* Location: ./application/models/Option_model.php */