<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mahasiswa_model extends MY_Model {

	public $table = 'mahasiswa';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
	}

	// get all with manual query
	public function get_all_manual($query=array())
	{
		if ( isset($query['where']) ) $this->db->where($query['where']);
		if ( isset($query['where_no_escape']) ) $this->db->where($query['where_no_escape'], NULL, FALSE);
		if ( isset($query['or_where']) ) $this->db->or_where($query['or_where']);
		if ( isset($query['or_where_no_escape']) ) $this->db->or_where($query['or_where_no_escape'], NULL, FALSE);
		if ( isset($query['limit']) ) $this->db->limit($query['limit']);
		if ( isset($query['limit_offset']) ) $this->db->limit($query['limit_offset'][0], $query['limit_offset'][1]);
		if ( isset($query['group_by']) ) $this->db->group_by($query['group_by']);
		
		if ( isset($query['join']) ) $this->db->join($query['join'][0], $query['join'][1]);
		if ( isset($query['left_join']) ) $this->db->join($query['left_join'][0], $query['left_join'][1], 'left');
		if ( isset($query['right_join']) ) $this->db->right($query['right_join'][0], $query['right_join'][1], 'right');

		if ( isset($query['order_by']) ) {

			if ( is_array($query['order_by']) ) {

				foreach ( $query['order_by'] as $column => $dir ) {

					$this->db->order_by($column, $dir);

				}

			} else {

				$this->db->order_by($query['order_by']);

			}

		}

		$this->db
			 ->select("{$this->table}.*")
			 ->select("CASE WHEN mahasiswa.status = 1 THEN 'Aktif Belum Tesis' WHEN mahasiswa.status = 2 THEN 'Aktif Sedang Tesis' WHEN mahasiswa.status = 3 THEN 'Lulus' WHEN mahasiswa.status = 4 THEN 'DO' ELSE 'Mengundurkan Diri' END AS status_label")
			 ->select("fakultas.fakultas")
			 ->select("dosen_pembimbing_1.nip AS nip_dosen_pembimbing_1, dosen_pembimbing_1.nama AS nama_dosen_pembimbing_1, dosen_pembimbing_1.kode AS kode_dosen_pembimbing_1, dosen_pembimbing_1.email AS email_dosen_pembimbing_1")
			 ->select("dosen_pembimbing_2.nip AS nip_dosen_pembimbing_2, dosen_pembimbing_2.nama AS nama_dosen_pembimbing_2, dosen_pembimbing_2.kode AS kode_dosen_pembimbing_2, dosen_pembimbing_2.email AS email_dosen_pembimbing_2")
			 ->select("dosen_pembimbing_3.nip AS nip_dosen_pembimbing_3, dosen_pembimbing_3.nama AS nama_dosen_pembimbing_3, dosen_pembimbing_3.kode AS kode_dosen_pembimbing_3, dosen_pembimbing_3.email AS email_dosen_pembimbing_3")
			 ->select("dosen_penguji.nip AS nip_dosen_penguji, dosen_penguji.nama AS nama_dosen_penguji, dosen_penguji.kode AS kode_dosen_penguji, dosen_penguji.email AS email_dosen_penguji")
			 ->from($this->table)
			 ->join("fakultas", "fakultas.id = {$this->table}.fakultas_id", "left")
			 ->join("dosen dosen_pembimbing_1", "dosen_pembimbing_1.id = {$this->table}.dosen_id_pembimbing_1", "left")
			 ->join("dosen dosen_pembimbing_2", "dosen_pembimbing_2.id = {$this->table}.dosen_id_pembimbing_2", "left")
			 ->join("dosen dosen_pembimbing_3", "dosen_pembimbing_3.id = {$this->table}.dosen_id_pembimbing_3", "left")
			 ->join("dosen dosen_penguji", "dosen_penguji.id = {$this->table}.dosen_id_penguji", "left");

		$query = $this->db->get();

		if ( $query->num_rows() > 0 ) {

			$data = $query->result();

			// for ($i=0; $i < count($data); $i++) {
			// 	// set value jika default value NULL atau kosong
			// 	foreach ($data[$i] as $key => $value) {
			// 		if ( (is_null($value)) OR (empty($value)) OR ($value == '0000-00-00') ) $data[$i]->{$key} = '<i>Belum diisi</i>';
			// 	}
			// }

			return $data;

		}

		return FALSE;
	}

	public function get_manual($query=array())
	{
		$result = $this->get_all_manual($query);

		if ( $result != FALSE ) {

			return $result[0];

		}

		return FALSE;
	}

	/**
	 * validate nrp
	 * cek apakah nrp sudah digunakan
	 * @param nrp
	 * @param id (cari semua mahasiswa kecuali mahasiswa dengan id ini)
	 * @return boolean
	 */
	public function validate_nrp($nrp, $id=NULL)
	{
		$this->where('nrp', $nrp);

		if ( ! is_null($id) ) {

			$this->where('id !=', $id);

		}

		$result = $this->get();

		if ( $result == FALSE ) {

			return TRUE;

		}

		return FALSE;
	}

	public function getAngkatan($query=array())
	{
		if ( isset($query['where']) ) $this->db->where($query['where']);
		if ( isset($query['where_no_escape']) ) $this->db->where($query['where_no_escape'], null, false);
		if ( isset($query['or_where']) ) $this->db->or_where($query['or_where']);
		if ( isset($query['or_where_no_escape']) ) $this->db->or_where($query['or_where_no_escape'], null, false);
		if ( isset($query['limit']) ) $this->db->limit($query['limit']);
		if ( isset($query['limit_offset']) ) $this->db->limit($query['limit_offset'][0], $query['limit_offset'][1]);
		if ( isset($query['group_by']) ) $this->db->group_by($query['group_by']);
		
		if ( isset($query['join']) ) $this->db->join($query['join'][0], $query['join'][1]);
		if ( isset($query['left_join']) ) $this->db->join($query['left_join'][0], $query['left_join'][1], 'left');
		if ( isset($query['right_join']) ) $this->db->right($query['right_join'][0], $query['right_join'][1], 'right');

		if ( isset($query['order_by']) ) {

			if ( is_array($query['order_by']) ) {

				foreach ( $query['order_by'] as $column => $dir ) {

					$this->db->order_by($column . ' ' . $dir, NULL, FALSE);

				}

			} else {

				$this->db->order_by($query['order_by'], NULL, FALSE);

			}

		}

		$this->db
		     ->select('angkatan')
		     ->from('mahasiswa')
		     ->group_by('angkatan')
		     ->order_by('angkatan desc');
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ) {

			$result = $query->result();

			foreach ( $result as $row ) {

				$data[$row->angkatan] = $row->angkatan;

			}

			return $data;

		}

		return FALSE;
	}

	public function isInThisFaculty($mahasiswa_id, $fakultas_id)
	{
		$this->db
		     ->select('id')
		     ->from('mahasiswa')
		     ->where('id', $mahasiswa_id)
		     ->where('fakultas_id', $fakultas_id);
		$query = $this->db->get();

		if ( $query->num_rows() == 1 ) {

			return TRUE;
		
		}

		return FALSE;
	}

}

/* End of file sudut_padang_model.php */
/* Location: ./application/models/sudut_padang_model.php */