<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Progress_mahasiswa_model extends MY_Model {

	public $table = 'progress_mahasiswa';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * membaca data progress
	 * @param  array  $query [description]
	 * @return [type]        [description]
	 */
	public function get($query=array())
	{
		if ( isset($query['where']) ) $this->db->where($query['where']);
		if ( isset($query['where_no_escape']) ) $this->db->where($query['where_no_escape'], null, false);
		if ( isset($query['or_where']) ) $this->db->or_where($query['or_where']);
		if ( isset($query['or_where_no_escape']) ) $this->db->or_where($query['or_where_no_escape'], null, false);
		if ( isset($query['limit']) ) $this->db->limit($query['limit']);
		if ( isset($query['limit_offset']) ) $this->db->limit($query['limit_offset'][0], $query['limit_offset'][1]);
		if ( isset($query['group_by']) ) $this->db->group_by($query['group_by']);
		
		if ( isset($query['join']) ) $this->db->join($query['join'][0], $query['join'][1]);
		if ( isset($query['left_join']) ) $this->db->join($query['left_join'][0], $query['left_join'][1], 'left');
		if ( isset($query['right_join']) ) $this->db->right($query['right_join'][0], $query['right_join'][1], 'right');

		if ( isset($query['order_by']) ) {

			if ( is_array($query['order_by']) ) {

				foreach ( $query['order_by'] as $column => $dir ) {

					$this->db->order_by($column . ' ' . $dir, NULL, FALSE);

				}

			} else {

				$this->db->order_by($query['order_by'], NULL, FALSE);

			}

		}

		$this->db
		     ->select('progress_mahasiswa.id, progress_mahasiswa.mahasiswa_id, progress_mahasiswa.progress_id, progress_mahasiswa.tanggal, progress_mahasiswa.semester, progress_mahasiswa.nilai')
		     ->select('mahasiswa.nrp, mahasiswa.nama as nama_mahasiswa')
		     ->select('progress.nama as nama_progress, progress.urutan')
		     ->from('progress_mahasiswa')
		     ->join('progress', 'progress.id = progress_mahasiswa.progress_id')
		     ->join('mahasiswa', 'mahasiswa.id = progress_mahasiswa.mahasiswa_id');
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ) {

			$result = $query->result();

			for ($i=0; $i < count($result); $i++) {
				
				$result[$i]->tanggal_format_indonesia = tgl_indonesia($result[$i]->tanggal);

			}

			return $result;

		}

		return FALSE;
	}

	/**
	 * membaca data progress
	 * return a single row
	 * @param  array  $query [description]
	 * @return [type]        [description]
	 */
	public function row($query=array())
	{
		$result = $this->get($query);

		if ( $result !== FALSE ) {

			return $result[0];

		}

		return FALSE;
	}

	/**
	 * mengecek apakah progress mahasiswa telah terdaftar
	 */
	public function isExist($mahasiswa_id, $progress_id)
	{
		$this->db
		     ->select('id')
		     ->from('progress_mahasiswa')
		     ->where('mahasiswa_id', $mahasiswa_id)
		     ->where('progress_id', $progress_id);
		$query = $this->db->get();

		if ( $query->num_rows() == 1 ) {

			$row = $query->row();

			return $row->id;

		}

		return FALSE;
	}

}

/* End of file Progress_model.php */
/* Location: ./application/models/Progress_model.php */