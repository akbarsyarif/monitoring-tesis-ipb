<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group_model extends MY_Model {

	public $table = 'groups';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file group_model.php */
/* Location: ./application/models/group_model.php */