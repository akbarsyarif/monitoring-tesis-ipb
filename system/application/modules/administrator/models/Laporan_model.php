<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getPerkembanganTesis($fakultas=NULL, $angkatan=NULL, $pembimbing=NULL)
	{

		$fakultas = (! empty($fakultas) OR $fakultas === FALSE) ? $fakultas = " AND fakultas_id = '{$fakultas}'" : "";
		$angkatan = (! empty($angkatan) OR $angkatan === FALSE) ? $angkatan = " AND angkatan = '{$angkatan}'" : "";
		$pembimbing = (! empty($pembimbing) OR $pembimbing === FALSE) ? $pembimbing = "  AND (dosen_id_pembimbing_1 = '{$pembimbing}' OR dosen_id_pembimbing_2 = '{$pembimbing}' OR dosen_id_pembimbing_3 = '{$pembimbing}')" : "";

		$query = "SELECT
					(
						SELECT COUNT(*)
						FROM mahasiswa
						WHERE progress_ta <= 3
						{$fakultas}
						{$angkatan}
						{$pembimbing}
					) AS belum_kolokium,
					(
						SELECT COUNT(*)
						FROM mahasiswa
						WHERE progress_ta >= 4 AND progress_ta <= 7
						{$fakultas}
						{$angkatan}
						{$pembimbing}
					) AS kolokium,
					(
						SELECT COUNT(*)
						FROM mahasiswa
						WHERE progress_ta >= 8 AND progress_ta <= 9
						{$fakultas}
						{$angkatan}
						{$pembimbing}
					) AS seminar,
					(
						SELECT COUNT(*)
						FROM mahasiswa
						WHERE progress_ta = 10
						{$fakultas}
						{$angkatan}
						{$pembimbing}
					) AS sidang,
					(
						SELECT COUNT(*)
						FROM mahasiswa
						WHERE progress_ta >= 11
						{$fakultas}
						{$angkatan}
						{$pembimbing}
					) AS lulus,
					(
						SELECT (belum_kolokium + kolokium + seminar + sidang + lulus)
					) AS total
		         ";

		$query = $this->db->query($query);

		$result = $query->row();

		return $result;

	}

}

/* End of file Laporan_model.php */
/* Location: ./application/models/Laporan_model.php */