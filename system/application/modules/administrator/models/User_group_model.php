<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_group_model extends MY_Model {

	public $table = 'user_groups';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		
	}

}

/* End of file User_group_model.php */
/* Location: ./application/models/User_group_model.php */