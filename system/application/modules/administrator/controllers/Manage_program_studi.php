<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_program_studi extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		// authorization
		if ( ! $this->sp_auth->isInThisGroup('administrator') ) {

			show_error('Access forbiden.');

		}

		// load dependencies
		$this->load->model('administrator/program_studi_model');
	}

	public function index()
	{
		$this->data['page_fa_icon'] = 'fa-files-o';
		$this->data['page_title'] = 'Daftar Program Studi';

		$this->template->build('manage_program_studi/index', $this->data);
	}

	/**
	 * ajax handler
	 * menampilkan data program studi
	 * @return void
	 */
	public function get($format='json', $id=null)
	{
		if ( $this->input->is_ajax_request() === false ) {
			show_404();
		}
		else {
			if ( $format == 'datatable' ) {
				$this->_datatable();
			}
			else {
				if ( ! is_null($id) ) {
					$data = $this->program_studi_model->row(array(
						'where' => array(
							'program_studi.id' => $id
							)
						));

					if ( $data === false ) {
						$response = (object) array (
							'metadata' => (object) array (
								'code' => "204",
								'message' => 'Program studi tidak ditemukan'
							),
							'response' => NULL
						);
					}
					else {
						$response = (object) array (
							'metadata' => (object) array (
								'code' => "200",
								'message' => 'OK'
							),
							'response' => $data
						);
					}
				}
				else {
					$data = $this->program_studi_model->get();

					if ( $data === false ) {
						$response = (object) array (
							'metadata' => (object) array (
								'code' => "204",
								'message' => 'Program studi tidak ditemukan'
							),
							'response' => NULL
						);
					}
					else {
						$response = (object) array (
							'metadata' => (object) array (
								'code' => "200",
								'message' => 'OK'
							),
							'response' => $data
						);
					}
				}

				echo json_encode($response);
			}
		}
	}

	/**
	 * ajax handler
	 * menampilkan data program studi dalam format datatables
	 * @return void
	 */
	public function _datatable()
	{
		$query = array();

		// columns index
		$column_index = array(null, 'program_studi.program_studi', 'fakultas.fakultas');

		// filter column handler / search
		$columns = $_GET['columns'];

		if ( ! empty($_GET['search']['value']) ) {

			$search_value = xss_clean($_GET['search']['value']);

			// global search
			$query['where'][$column_index[1].' like '] = "%$search_value%";

		}
		else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( ($column_key == 2) OR ($column_key == 3) ) {

						$query['where'][$column_index[$column_key]] = $column_val['search']['value'];

					} else {

						$query['where'][$column_index[$column_key].' like '] = '%'.$column_val['search']['value'].'%';

					}

				}

			}

		}

		$records_total = count($this->program_studi_model->get($query));
		$records_filtered = $records_total;

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $_GET['order'] as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order_by'][$column_index[$order_column]] = $order_dir;
			}
		}

		// limit
		$query['limit_offset'] = ($_GET['length'] > 0) ? array($_GET['length'], $_GET['start']) : NULL;

		$data = $this->program_studi_model->get($query);

		$response = (object) array (
			'draw' => $_GET['draw'],
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_filtered,
			'data' => $data
		);

		echo json_encode($response);
	}

	/**
	 * ajax handler
	 * menambahkan data program_studi
	 * @return void
	 */
	public function save()
	{
		$rules[] = array(
			'field' => 'program_studi',
			'label' => 'Program Studi',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'fakultas_id',
			'label' => 'Fakultas',
			'rules' => 'required|trim|xss_clean',
			);

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == false ) {

			$response = (object) array(
				'result' => false,
				'message' => validation_errors('<div class="error">', '</div>')
				);

		}
		else {

			unset($data);

			$data['program_studi'] = $this->input->post('program_studi', TRUE);
			$data['fakultas_id'] = $this->input->post('fakultas_id', TRUE);

			if ( $this->program_studi_model->insert($data) == false ) {

				$response = (object) array(
					'metadata' => (object) array(
						'code' => "500",
						'message' => "Internal server error"
					),
					'response' => NULL
				);

			}
			else {

				$response = (object) array(
					'metadata' => (object) array(
						'code' => "200",
						'message' => "OK"
					),
					'response' => NULL
				);

			}

		}

		echo json_encode($response);
	}

	public function update()
	{
		$rules[] = array(
			'field' => 'program_studi',
			'label' => 'Program Studi',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'fakultas_id',
			'label' => 'Fakultas',
			'rules' => 'required|trim|xss_clean',
			);

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == false ) {

			$response = (object) array(
				'result' => false,
				'message' => validation_errors('<div class="error">', '</div>')
				);

		}
		else {

			$id = $this->input->post('id', TRUE);

			// cek apakah program studi terdaftar
			$result = $this->program_studi_model->row( array (
				'where' => array (
					'program_studi.id' => $id
				)
			) );

			if ( $result === FALSE ) {

				$response = (object) array(
					'metadata' => (object) array(
						'code' => "204",
						'message' => 'Program studi tidak ditemukan'
					),
					'response' => NULL
				);

			}
			else {

				unset($data);

				$data['program_studi'] = $this->input->post('program_studi', TRUE);
				$data['fakultas_id'] = $this->input->post('fakultas_id', TRUE);

				if ( $this->program_studi_model->where_id($id)->update($data) == false ) {

					$response = (object) array(
						'metadata' => (object) array(
							'code' => "500",
							'message' => "Internal server error"
						),
						'response' => NULL
					);

				}
				else {

					$response = (object) array(
						'metadata' => (object) array(
							'code' => "200",
							'message' => "OK"
						),
						'response' => NULL
					);

				}

			}

		}

		echo json_encode($response);
	}

	/**
	 * ajax handler
	 * menghapus program studi
	 * @return void
	 */
	public function delete()
	{
		$id = $this->input->get('id', TRUE);

		// cek apakah id terdaftar
		$result = $this->program_studi_model->row( array (
			'where' => array (
				'program_studi.id' => $id
			)
		) );

		if ( $result === FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "201",
					'message' => "ID tidak ditemukan"
				),
				'response' => NULL
			);

		}
		else {

			$result = $this->program_studi_model->delete($id);

			if ( $result === FALSE ) {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "202",
						'message' => "Terjadi error saat mencoba menghapus data"
					),
					'response' => NULL
				);

			}
			else {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "200",
						'message' => "OK"
					),
					'response' => NULL
				);

			}

		}

		echo json_encode($response);
	}

}

/* End of file manage_program_studi.php */
/* Location: ./application/controllers/manage_program_studi.php */