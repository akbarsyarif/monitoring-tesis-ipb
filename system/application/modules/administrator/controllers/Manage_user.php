<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_user extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		// load dependencies
		$this->load->model('user_model');
		$this->load->model('group_model');
		$this->load->model('user_group_model');

		// authorization
		if ( ! $this->sp_auth->isInThisGroup('administrator') ) {

			show_error('Access forbiden.');

		}
	}


	/**
	 * display users
	 */
	public function index()
	{

		$this->data['page_title'] = 'Users';
		$this->data['page_sub_title'] = 'Manage Users';
		
		$this->template->build('manage_user/index', $this->data);

	}


	/**
	 * retrieve data as a datatable's format
	 */
	public function dataTable()
	{

		// variable that contain condition
		$query['where'] = null;

		$query['where']['users.id !='] = $this->sp_auth->userData()->user_id;

		// columns index
		$column_index = array(null, null, 'users.first_name', 'users.username', 'groups.group_alias', null);

		// filter column handler / search
		$columns = $_GET['columns'];

		if ( ! empty($_GET['search']['value']) ) {

			$search_value = xss_clean($_GET['search']['value']);

			// global search
			$query['where']['CONCAT(users.first_name, " ", users.last_name) like'] = "%$search_value%";

		} else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( $column_key == 3 ) {

						$query['where'][] = array($column_index[$column_key], $column_val['search']['value']);

					} else {

						$query['where'][] = array($column_index[$column_key].' like ', '%'.$column_val['search']['value'].'%');

					}

				}

			}

		}

		$records_total    = count($this->user_model->get($query));
		$records_filtered = $records_total;

		// orders
		$order_column = $_GET['order'][0]['column'];
		$order_dir    = $_GET['order'][0]['dir'];

		$this->db->limit($_GET['length'], $_GET['start']);
		$this->db->order_by($column_index[$order_column], $order_dir);

		$data = $this->user_model->get($query);

		$response = (object) array (
			'draw' => $_GET['draw'],
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_filtered,
			'data' => $data
		);

		echo json_encode($response);

	}


	/**
	 * get data by id
	 */
	public function get_by_id()
	{

		$uid = $this->input->get('uid', TRUE);

		// get user data
		$query['where'] = array( 'users.id' => $uid );
		$query['limit'] = 1;
		$user = $this->user_model->get($query);

		if ( (! $uid) OR (! $user) ) {

			$response = (object) array(
				'result' => false,
				'message' => 'UID not found.'
			);

		} else {

			$response = (object) array(
				'result' => true,
				'data' => $user[0]
			);

		}

		echo json_encode($response);

	}


	/**
	 * login as specific user
	 */
	public function login_as()
	{

		// get user id
		$user_id = $this->input->get('uid', TRUE);

		// get userdata
		$user = $this->sp_auth->userData($user_id);

		if ( (! $user_id) OR (! $user) ) {

			$response = (object) array(
				'result' => false,
				'message' => 'UID not found.'
			);

		} else {

			// bypass authentication
			// just create login session for this user
			$session['user_id']   = $user_id;
			$session['logged_in'] = true;
 
			if ( $this->sp_auth->isInThisGroup('administrator', $user_id) ) {

				$dst = site_url('administrator/dashboard');

			}
			else if ( $this->sp_auth->isInThisGroup('operator_fakultas', $user_id) ) {

				$session['fakultas_id'] = $user->fakultas_id;

				$dst = site_url('operator_fakultas/dashboard');

			}
			else {

				$dst = site_url('logout');

			}

			// create login session
			$this->session->set_userdata($session);

			$response = (object) array(
				'result' => true,
				'dst' => $dst
			);

		}

		echo json_encode($response);

	}


	/**
	 * reset password
	 */
	public function reset_password()
	{

		$uid = $this->input->get('uid', TRUE);

		// get userdata
		$user = $this->sp_auth->userData($uid);

		if ( (! $uid) OR (! $user) ) {

			$response = (object) array(
				'result' => false,
				'message' => 'UID not found.'
			);

		} else {

			// generate new password from random string
			$new_password = random_string('alnum', 8);

			// hash version
			$pass_hash = $this->sp_auth->passHash($new_password);

			// update password
			if ( ! $this->user_model->update(array( 'password' => $pass_hash ), $uid) ) {

				$response = (object) array(
					'result' => false,
					'message' => 'Error occured. Fail to reset password.'
				);

			} else {

				$response = (object) array(
					'result' => true,
					'new_password' => $new_password
				);

			}

		}

		echo json_encode($response);

	}


	/**
	 * add new user
	 */
	public function save()
	{

		// form validation
		$rules = array(
			array(
				'field' => 'first_name',
				'label' => 'Nama Depan',
				'rules' => 'required|trim|xss_clean'
			),
			array(
				'field' => 'last_name',
				'label' => 'Nama Belakang',
				'rules' => 'trim|xss_clean'
			),
			array(
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required|valid_email|trim|xss_clean'
			),
			array(
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|trim|is_unique[users.username]|xss_clean'
			),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|min_length[5]|matches[confirm_password]|trim|xss_clean'
			),
			array(
				'field' => 'confirm_password',
				'label' => 'Confirm Password',
				'rules' => 'required|min_length[5]|trim|xss_clean'
			),
			array(
				'field' => 'group_id',
				'label' => 'Grup',
				'rules' => 'required|integer|trim|xss_clean'
			),
		);

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == false ) {

			$response = (object) array(
				'result' => false,
				'message' => validation_errors()
			);

		} else {

			$this->db->trans_begin();

			// hashing password
			$pass_hash = $this->sp_auth->passHash($this->input->post('password', TRUE));

			$userdata['first_name']  = $this->input->post('first_name', TRUE);
			$userdata['last_name']   = $this->input->post('last_name', TRUE);
			$userdata['email']       = $this->input->post('email', TRUE);
			$userdata['username']    = $this->input->post('username', TRUE);
			$userdata['password']    = $pass_hash;
			$userdata['user_status'] = $this->input->post('user_status', TRUE);

			// default profile pictures
			$userdata['profile_picture'] = base_url('assets/img/default_profile_picture.gif');

			$group_id = $this->input->post('group_id', TRUE);

			if ( $group_id == "2" ) $userdata['fakultas_id'] = $this->input->post('fakultas_id', TRUE);

			// save userdata
			if ( ! $this->user_model->insert($userdata) ) {

				$response = (object) array(
					'result' => false,
					'message' => 'Error occured. Fail to save userdata.'
				);

			} else {

				$usergroupdata['user_id']  = $this->db->insert_id();
				$usergroupdata['group_id'] = $group_id;

				// save user group relationship
				if ( ! $this->user_group_model->insert($usergroupdata) ) {

					$response = (object) array(
						'result' => false,
						'message' => 'Error occured. Fail to save user group relationship.'
					);

				} else {

					$response = (object) array(
						'result' => true
					);

				}

			}

			// check transaction status
			if ( $this->db->trans_status() === FALSE ) {

				$this->db->trans_rollback();

			} else {

				$this->db->trans_commit();

			}

		}

		echo json_encode($response);

	}


	/**
	 * update data
	 */
	public function update()
	{

		// form validation
		$rules = array(
			array(
				'field' => 'first_name',
				'label' => 'Nama Depan',
				'rules' => 'required|trim|xss_clean'
			),
			array(
				'field' => 'last_name',
				'label' => 'Nama Belakang',
				'rules' => 'trim|xss_clean'
			),
			array(
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required|valid_email|trim|xss_clean'
			),
			array(
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|trim|xss_clean'
			),
			array(
				'field' => 'group_id',
				'label' => 'Grup',
				'rules' => 'required|integer|trim|xss_clean'
			),
		);

		$group_id = $this->input->post('group_id', TRUE);

		if ( $group_id == "2" ) {
			$rules[] = array(
				'field' => 'fakultas_id',
				'label' => 'Fakultas',
				'rules' => 'required|trim|integer'
			);
		}

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == false ) {

			$response = (object) array(
				'result' => false,
				'message' => validation_errors()
			);

		} else {

			$uid      = $this->input->post('uid', TRUE);
			$username = $this->input->post('username', TRUE);
			$email    = $this->input->post('email', TRUE);

			// check username username must contain unique value
			if ( ! $this->isUsernameUnique($username, $uid) ) {

				$response = (object) array(
					'result' => false,
					'message' => "Username $username sudah digunakan oleh user lain."
				);

			} else if ( ! $this->isEmailUnique($email, $uid) ) {

				$response = (object) array(
					'result' => false,
					'message' => "Email $email sudah digunakan oleh user lain."
				);

			} else {

				$this->db->trans_begin();

				$userdata['first_name']  = $this->input->post('first_name', TRUE);
				$userdata['last_name']   = $this->input->post('last_name', TRUE);
				$userdata['email']       = $this->input->post('email', TRUE);
				$userdata['username']    = $this->input->post('username', TRUE);
				$userdata['user_status'] = $this->input->post('user_status', TRUE);

				$group_id = $this->input->post('group_id', TRUE);

				if ( $group_id == "2" ) $userdata['fakultas_id'] = $this->input->post('fakultas_id', TRUE);

				// save userdata
				if ( ! $this->user_model->update($userdata, $uid) ) {

					$response = (object) array(
						'result' => false,
						'message' => 'Error occured. Fail to update userdata.'
					);

				} else {

					$usergroupdata['user_id']  = $uid;
					$usergroupdata['group_id'] = $group_id;

					// save user group relationship
					if ( ! $this->user_group_model->where('user_id', $uid)->update($usergroupdata) ) {

						$response = (object) array(
							'result' => false,
							'message' => 'Error occured. Fail to save user group relationship.'
						);

					} else {

						$response = (object) array(
							'result' => true
						);

					}

				}

				// check transaction status
				if ( $this->db->trans_status() === FALSE ) {

					$this->db->trans_rollback();

				} else {

					$this->db->trans_commit();

				}

			}

		}

		echo json_encode($response);

	}


	/**
	 * remove user by given id
	 */
	public function remove()
	{
		$id = $this->input->get('uid', TRUE);

		$result = $this->user_model->where_id($id)->get();

		if ( $result === FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "201",
					'message' => "User tidak ditemukan"
				),
				'response' => NULL
			);

		}
		else {

			$result = $this->user_model->where_id($id)->delete();

			if ( $result === FALSE ) {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "800",
						'message' => "Gagal menghapus user"
					),
					'response' => NULL
				);

			}
			else {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "200",
						'message' => "OK"
					),
					'response' => NULL
				);

			}

		}

		echo json_encode($response);
	}


	/**
	 * toggle lock access
	 */
	public function toggle_lock()
	{

		$uid = $this->input->get('uid', TRUE);

		$user = $this->user_model->where('id', $uid)->get();

		if ( (! $uid) OR (! $user) ) {

			$response = (object) array(
				'result' => false,
				'message' => 'Data usulan tidak ditemukan.'
			);

		} else {

			if ( $user->lock == true ) {

				$lock = false;

			} else {

				$lock = true;

			}

			$data['lock'] = $lock;

			if ( ! $this->user_model->update($data, $uid) ) {

				$response = (object) array(
					'result' => false,
					'message' => 'Error occured. Fail to update lock status.'
				);

			} else {

				$response = (object) array(
					'result' => true,
					'lock' => $lock
				);

			}

		}

		echo json_encode($response);

	}


	/**
	 * check if this "username" is used by another "user"
	 * @param string [new username]
	 * @param integer [uid, this is optional parameter to not
	 * included search result of user with this id ]
	 */
	private function isUsernameUnique($username, $uid=null)
	{

		$where = null;

		$where['username'] = $username;

		if ( ! is_null($uid) ) {

			$where['id !='] = $uid;

		}

		$exist = $this->user_model->where($where)->count();

		if ( ! $exist ) {

			return true;

		}

		return false;

	}


	/**
	 * check if this "email" is used by another "user"
	 * @param string [new email]
	 * @param integer [uid, this is optional parameter to not
	 * included search result of user with this id ]
	 */
	private function isEmailUnique($email, $uid=null)
	{

		$where = null;

		$where['email'] = $email;

		if ( ! is_null($uid) ) {

			$where['id !='] = $uid;

		}

		$exist = $this->user_model->where($where)->count();

		if ( ! $exist ) {

			return true;

		}

		return false;

	}

}

/* End of file Manage_user.php */
/* Location: ./application/controllers/Manage_user.php */