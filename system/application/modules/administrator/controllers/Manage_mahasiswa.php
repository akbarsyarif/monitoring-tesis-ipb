<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_mahasiswa extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		// authorization
		if ( ! $this->sp_auth->isInThisGroup('administrator') ) {

			show_error('Access forbiden.');

		}

		// load dependencies
		$this->load->model('administrator/mahasiswa_model');
	}

	public function index()
	{
		$this->data['page_fa_icon'] = 'fa-files-o';
		$this->data['page_title'] = 'Daftar Mahasiswa';

		// angkatan array
		$angkatan = $this->mahasiswa_model->getAngkatan();
		$angkatan = array('' => '-- Semua --') + $angkatan;
		$this->data['angkatan'] = $angkatan;

		$this->template->build('manage_mahasiswa/index', $this->data);
	}

	/**
	 * ajax handler
	 * menampilkan daftar dosen
	 * @return void
	 */
	public function get($format='json', $id=null)
	{
		if ( $this->input->is_ajax_request() === false ) {
			show_404();
		}
		else {
			if ( $format == 'datatable' ) {
				$this->_datatable();
			}
			else {
				if ( ! is_null($id) ) {
					$data = $this->mahasiswa_model->get_manual(array(
						'where' => array(
							'mahasiswa.id' => $id
							)
						));

					if ( $data === false ) {
						$response = array(
							'result' => false,
							'message' => 'Data tidak ditemukan'
							);
					}
					else {
						$response = array(
							'result' => true,
							'data' => $data
							);
					}
				}
				else {
					$data = $this->mahasiswa_model->get_all_manual();

					if ( $data === false ) {
						$response = array(
							'result' => false,
							'message' => 'Data tidak ditemukan'
							);
					}
					else {
						$response = array(
							'result' => true,
							'data' => $data
							);
					}
				}

				echo json_encode($response);
			}
		}
	}

	/**
	 * ajax handler
	 * menampilkan data dosen dalam format json dataTable
	 * @return void
	 */
	public function _datatable()
	{
		$query = array();

		// columns index
		$column_index = array(null, null, 'mahasiswa.nrp', 'mahasiswa.nama', 'mahasiswa.angkatan', 'fakultas.fakultas', null);

		// filter column handler / search
		$columns = $_GET['columns'];

		if ( ! empty($_GET['search']['value']) ) {

			$search_value = xss_clean($_GET['search']['value']);

			// global search
			$query['where'][$column_index[3].' like '] = "%$search_value%";

		}
		else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( $column_key == 5 ) {

						$query['where'][$column_index[$column_key]] = $column_val['search']['value'];

					}
					else {

						$query['where'][$column_index[$column_key].' like '] = '%'.$column_val['search']['value'].'%';
						
					}

				}

			}

		}

		$records_total = count($this->mahasiswa_model->get_all_manual($query));
		$records_filtered = $records_total;

		// orders
		$order_column = $_GET['order'][0]['column'];
		$order_dir = $_GET['order'][0]['dir'];

		$query['limit_offset'] = array($_GET['length'], $_GET['start']);
		$query['order_by'] = $column_index[$order_column].' '.$order_dir;

		$data = $this->mahasiswa_model->get_all_manual($query);

		$response = (object) array (
			'draw' => $_GET['draw'],
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_filtered,
			'data' => $data
		);

		echo json_encode($response);
	}

	/**
	 * ajax handler
	 * menambahkan data mahasiswa
	 * @return void
	 */
	public function save()
	{
		$rules[] = array(
			'field' => 'nrp',
			'label' => 'NRP',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'nama',
			'label' => 'Nama',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'angkatan',
			'label' => 'Angkatan',
			'rules' => 'trim|numeric|xss_clean',
			);
		$rules[] = array(
			'field' => 'tanggal_masuk',
			'label' => 'Email',
			'rules' => 'trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'fakultas_id',
			'label' => 'Fakultas',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'program_studi',
			'label' => 'Program Studi',
			'rules' => 'trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'kelas',
			'label' => 'Kelas',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'semester',
			'label' => 'Semester',
			'rules' => 'required|trim|numeric|xss_clean',
			);
		$rules[] = array(
			'field' => 'pt_asal',
			'label' => 'Perguruan Tinggi Asal',
			'rules' => 'trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'instansi',
			'label' => 'Nama Instansi',
			'rules' => 'trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'telp_instansi',
			'label' => 'No. Telepon Instansi',
			'rules' => 'trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'no_hp',
			'label' => 'No. HP',
			'rules' => 'trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'trim|valid_email|xss_clean',
			);
		$rules[] = array(
			'field' => 'status',
			'label' => 'Status',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'fakultas_id',
			'label' => 'Fakultas',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'judul_ta',
			'label' => 'Judul Tugas Akhir',
			'rules' => 'trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'dosen_id_penguji',
			'label' => 'Dosen Penguji',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'dosen_id_pembimbing_1',
			'label' => 'Dosen Pembimbing 1',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'dosen_id_pembimbing_2',
			'label' => 'Dosen Pembimbing 2',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'dosen_id_pembimbing_1',
			'label' => 'Dosen Pembimbing 3',
			'rules' => 'trim|xss_clean',
			);

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == false ) {

			$response = (object) array(
				'result' => false,
				'message' => validation_errors('<div class="error">', '</div>')
				);

		}
		else {

			unset($data);

			$data['nrp']   = $this->input->post('nrp', TRUE);
			$data['nama']  = $this->input->post('nama', TRUE);
			$data['angkatan']  = $this->input->post('angkatan', TRUE);
			$data['tanggal_masuk'] = $this->input->post('tanggal_masuk', TRUE);
			$data['fakultas_id'] = $this->input->post('fakultas_id', TRUE);
			$data['program_studi'] = $this->input->post('program_studi', TRUE);
			$data['kelas'] = $this->input->post('kelas', TRUE);
			$data['semester'] = $this->input->post('semester', TRUE);
			$data['pt_asal'] = $this->input->post('pt_asal', TRUE);
			$data['instansi'] = $this->input->post('instansi', TRUE);
			$data['telp_instansi'] = $this->input->post('telp_instansi', TRUE);
			$data['no_hp'] = $this->input->post('no_hp', TRUE);
			$data['email'] = $this->input->post('email', TRUE);
			$data['status'] = $this->input->post('status', TRUE);
			$data['tanggal_lulus'] = ($this->input->post('tanggal_lulus', TRUE)) ? $this->input->post('tanggal_lulus', TRUE) : NULL;
			$data['judul_ta'] = $this->input->post('judul_ta', TRUE);
			$data['dosen_id_penguji'] = $this->input->post('dosen_id_penguji', TRUE);
			$data['dosen_id_pembimbing_1'] = $this->input->post('dosen_id_pembimbing_1', TRUE);
			$data['dosen_id_pembimbing_2'] = $this->input->post('dosen_id_pembimbing_2', TRUE);
			$data['dosen_id_pembimbing_3'] = ($this->input->post('dosen_id_pembimbing_3', TRUE)) ? $this->input->post('dosen_id_pembimbing_3', TRUE) : NULL;

			if ( $this->mahasiswa_model->validate_nrp($data['nrp']) == false ) {
				
				$response = (object) array(
					'result' => false,
					'message' => 'NRP sudah digunakan'
					);

			}
			else {

				if ( $this->mahasiswa_model->insert($data) == false ) {

					$response = (object) array(
						'result' => false,
						'message' => 'Server error. Gagal menyimpan data.'
						);

				}
				else {

					$response = (object) array(
						'result' => true
						);

				}
				
			}

		}

		echo json_encode($response);
	}

	/**
	 * ajax handler
	 * mengupdate data dosen
	 * @return void
	 */
	public function update()
	{
		$rules[] = array(
			'field' => 'nrp',
			'label' => 'NRP',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'nama',
			'label' => 'Nama',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'angkatan',
			'label' => 'Angkatan',
			'rules' => 'trim|numeric|xss_clean',
			);
		$rules[] = array(
			'field' => 'tanggal_masuk',
			'label' => 'Email',
			'rules' => 'trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'fakultas_id',
			'label' => 'Fakultas',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'program_studi',
			'label' => 'Program Studi',
			'rules' => 'trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'kelas',
			'label' => 'Kelas',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'semester',
			'label' => 'Semester',
			'rules' => 'required|trim|numeric|xss_clean',
			);
		$rules[] = array(
			'field' => 'pt_asal',
			'label' => 'Perguruan Tinggi Asal',
			'rules' => 'trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'instansi',
			'label' => 'Nama Instansi',
			'rules' => 'trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'telp_instansi',
			'label' => 'No. Telepon Instansi',
			'rules' => 'trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'no_hp',
			'label' => 'No. HP',
			'rules' => 'trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'trim|valid_email|xss_clean',
			);
		$rules[] = array(
			'field' => 'status',
			'label' => 'Status',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'fakultas_id',
			'label' => 'Fakultas',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'judul_ta',
			'label' => 'Judul Tugas Akhir',
			'rules' => 'trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'dosen_id_penguji',
			'label' => 'Dosen Penguji',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'dosen_id_pembimbing_1',
			'label' => 'Dosen Pembimbing 1',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'dosen_id_pembimbing_2',
			'label' => 'Dosen Pembimbing 2',
			'rules' => 'required|trim|xss_clean',
			);
		$rules[] = array(
			'field' => 'dosen_id_pembimbing_1',
			'label' => 'Dosen Pembimbing 3',
			'rules' => 'trim|xss_clean',
			);

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == false ) {

			$response = (object) array(
				'result' => false,
				'message' => validation_errors('<div class="error">', '</div>')
				);

		}
		else {

			// check if data exist
			$id     = $this->input->post('id', TRUE);
			$result = $this->mahasiswa_model->where_id($id)->get();

			if ( $id == FALSE OR $result == FALSE ) {

				$response = (object) array(
					'result' => false,
					'message' => 'Data tidak ditemukan.'
					);

			}
			else {

				unset($data);

				$data['nrp']   = $this->input->post('nrp', TRUE);
				$data['nama']  = $this->input->post('nama', TRUE);
				$data['angkatan']  = $this->input->post('angkatan', TRUE);
				$data['tanggal_masuk'] = $this->input->post('tanggal_masuk', TRUE);
				$data['fakultas_id'] = $this->input->post('fakultas_id', TRUE);
				$data['program_studi'] = $this->input->post('program_studi', TRUE);
				$data['kelas'] = $this->input->post('kelas', TRUE);
				$data['semester'] = $this->input->post('semester', TRUE);
				$data['pt_asal'] = $this->input->post('pt_asal', TRUE);
				$data['instansi'] = $this->input->post('instansi', TRUE);
				$data['telp_instansi'] = $this->input->post('telp_instansi', TRUE);
				$data['no_hp'] = $this->input->post('no_hp', TRUE);
				$data['email'] = $this->input->post('email', TRUE);
				$data['status'] = $this->input->post('status', TRUE);
				$data['tanggal_lulus'] = ($this->input->post('tanggal_lulus', TRUE)) ? $this->input->post('tanggal_lulus', TRUE) : NULL;
				$data['judul_ta'] = $this->input->post('judul_ta', TRUE);
				$data['dosen_id_penguji'] = $this->input->post('dosen_id_penguji', TRUE);
				$data['dosen_id_pembimbing_1'] = $this->input->post('dosen_id_pembimbing_1', TRUE);
				$data['dosen_id_pembimbing_2'] = $this->input->post('dosen_id_pembimbing_2', TRUE);
				$data['dosen_id_pembimbing_3'] = ($this->input->post('dosen_id_pembimbing_3', TRUE)) ? $this->input->post('dosen_id_pembimbing_3', TRUE) : NULL;

				if ( $this->mahasiswa_model->validate_nrp($data['nrp'], $id) == false ) {

					$response = (object) array(
						'result' => false,
						'message' => 'NRP sudah digunakan'
						);

				}
				else {

					if ( $this->mahasiswa_model->where_id($id)->update($data) == false ) {

						$response = (object) array(
							'result' => false,
							'message' => 'Server error. Gagal menyimpan perubahan.'
							);

					}
					else {

						$response = (object) array(
							'result' => true
							);

					}

				}

			}

		}

		echo json_encode($response);
	}

	/**
	 * ajax handler
	 * menghapus data dosen
	 * @return void
	 */
	public function remove()
	{
		$id     = $this->input->get('id', TRUE);
		$result = $this->mahasiswa_model->where_id($id)->get();

		if ( $id == FALSE OR $result == FALSE ) {

			$response = (object) array(
				'result' => false,
				'message' => 'Data tidak ditemukan.'
				);

		}
		else {

			$result = $this->mahasiswa_model->where_id($id)->delete();

			if ( $result == false ) {

				$response = (object) array(
					'result' => false,
					'message' => 'Server error. Gagal menghapus data.'
					);

			}
			else {

				$response = (object) array(
					'result' => true
					);

			}

		}

		echo json_encode($response);
	}

}

/* End of file manage_mahasiswa.php */
/* Location: ./application/controllers/manage_mahasiswa.php */