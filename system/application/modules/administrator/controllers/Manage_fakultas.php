<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_fakultas extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		// authorization
		if ( ! $this->sp_auth->isInThisGroup('administrator') ) {

			show_error('Access forbiden.');

		}

		// load dependencies
		$this->load->model('administrator/fakultas_model');
	}

	public function index()
	{
		$this->data['page_fa_icon'] = 'fa-files-o';
		$this->data['page_title'] = 'Daftar Fakultas';

		$this->template->build('manage_fakultas/index', $this->data);
	}

	/**
	 * ajax handler
	 * menampilkan daftar fakultas
	 * @return void
	 */
	public function get($format='json', $id=null)
	{
		if ( $this->input->is_ajax_request() === false ) {
			show_404();
		}
		else {
		
			if ( $format == 'datatable' ) {
				$this->_datatable();
			}
			else {
				if ( ! is_null($id) ) {
					$data = $this->fakultas_model->get_manual(array(
						'where' => array(
							'id' => $id
							)
						));

					if ( $data === false ) {
						$response = array(
							'result' => false,
							'message' => 'Data tidak ditemukan'
							);
					}
					else {
						$response = array(
							'result' => true,
							'data' => $data
							);
					}
				}
				else {
					$data = $this->fakultas_model->get_all_manual();

					if ( $data === false ) {
						$response = array(
							'result' => false,
							'message' => 'Data tidak ditemukan'
							);
					}
					else {
						$response = array(
							'result' => true,
							'data' => $data
							);
					}
				}

				echo json_encode($response);
			}
		
		}
	}

	/**
	 * ajax handler
	 * menampilkan data fakultas dalam format json dataTable
	 * @return void
	 */
	public function _datatable()
	{
		$query = array();

		// columns index
		$column_index = array(null, 'fakultas', null);

		// filter column handler / search
		$columns = $_GET['columns'];

		if ( ! empty($_GET['search']['value']) ) {

			$search_value = xss_clean($_GET['search']['value']);

			// global search
			$query['where'][$column_index[1].' like '] = "%$search_value%";

		}
		else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					$query['where'][$column_index[$column_key].' like '] = '%'.$column_val['search']['value'].'%';

				}

			}

		}

		$records_total = count($this->fakultas_model->get_all_manual($query));
		$records_filtered = $records_total;

		// orders
		$order_column = $_GET['order'][0]['column'];
		$order_dir = $_GET['order'][0]['dir'];

		$query['limit_offset'] = array($_GET['length'], $_GET['start']);
		$query['order_by'] = $column_index[$order_column].' '.$order_dir;

		$data = $this->fakultas_model->get_all_manual($query);

		$response = (object) array (
			'draw' => $_GET['draw'],
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_filtered,
			'data' => $data
		);

		echo json_encode($response);
	}

	/**
	 * ajax handler
	 * menambahkan data fakultas
	 * @return void
	 */
	public function save()
	{
		$rules[] = array(
			'field' => 'fakultas',
			'label' => 'Fakultas',
			'rules' => 'required|trim|xss_clean',
			);

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == false ) {

			$response = (object) array(
				'result' => false,
				'message' => validation_errors('<div class="error">', '</div>')
				);

		}
		else {

			unset($data);

			$data['fakultas'] = $this->input->post('fakultas', TRUE);

			if ( $this->fakultas_model->insert($data) == false ) {

				$response = (object) array(
					'result' => false,
					'message' => 'Server error. Gagal menyimpan data.'
					);

			}
			else {

				$response = (object) array(
					'result' => true
					);

			}

		}

		echo json_encode($response);
	}

	/**
	 * ajax handler
	 * mengupdate data fakultas
	 * @return void
	 */
	public function update()
	{
		$rules[] = array(
			'field' => 'fakultas',
			'label' => 'Fakultas',
			'rules' => 'required|trim|xss_clean',
			);

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == false ) {

			$response = (object) array(
				'result' => false,
				'message' => validation_errors('<div class="error">', '</div>')
				);

		}
		else {

			// check if data exist
			$id     = $this->input->post('id', TRUE);
			$result = $this->fakultas_model->where_id($id)->get();

			if ( $id == FALSE OR $result == FALSE ) {

				$response = (object) array(
					'result' => false,
					'message' => 'Data tidak ditemukan.'
					);

			}
			else {

				unset($data);

				$data['fakultas']        = $this->input->post('fakultas', TRUE);

				if ( $this->fakultas_model->where_id($id)->update($data) == false ) {

					$response = (object) array(
						'result' => false,
						'message' => 'Server error. Gagal menyimpan perubahan.'
						);

				}
				else {

					$response = (object) array(
						'result' => true
						);

				}

			}

		}

		echo json_encode($response);
	}

	/**
	 * ajax handler
	 * menghapus data fakultas
	 * @return void
	 */
	public function remove()
	{
		$id     = $this->input->get('id', TRUE);
		$result = $this->fakultas_model->where_id($id)->get();

		if ( $id == FALSE OR $result == FALSE ) {

			$response = (object) array(
				'result' => false,
				'message' => 'Data tidak ditemukan.'
				);

		}
		else {

			$result = $this->fakultas_model->where_id($id)->delete();

			if ( $result == false ) {

				$response = (object) array(
					'result' => false,
					'message' => 'Server error. Gagal menghapus data.'
					);

			}
			else {

				$response = (object) array(
					'result' => true
					);

			}

		}

		echo json_encode($response);
	}

}

/* End of file manage_fakultas.php */
/* Location: ./application/controllers/manage_fakultas.php */