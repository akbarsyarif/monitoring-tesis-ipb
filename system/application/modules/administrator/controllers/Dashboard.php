<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		// check authorization
		if ( ! $this->sp_auth->isInThisGroup('administrator') ) {

			show_error('Access forbiden.');

		}

		// load dependencies
		$this->load->model('pengumuman/pengumuman_model');

	}

	public function index()
	{
		$this->data['page_title'] = 'Pengumuman';
		$query_pengumuman['limit'] = get_option('announcement_count');
		$this->data['pengumuman'] = $this->pengumuman_model->get_all($query_pengumuman);

		$this->template->build('dashboard/index', $this->data);	
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */