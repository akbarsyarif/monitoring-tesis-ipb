<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting_general extends SP_Admin_Controller {

	private $error = false;
	private $error_messages = array();

	public function __construct()
	{
		parent::__construct();

		// load dependencies
		$this->load->model('option_model');

		// authorization
		if ( ! $this->sp_auth->isInThisGroup('administrator') ) {

			show_error('Access forbiden.');

		}
	}

	public function index()
	{

		$this->data['page_title'] = 'Setting';
		$this->data['page_sub_title'] = 'Pengaturan Umum';

		$this->template->build('setting_general/index', $this->data);
		
	}


	/**
	 * save settings
	 */
	public function save_settings()
	{

		// form validation
		$rules = array(
			array(
				'field' => 'app_title',
				'label' => 'Nama Aplikasi',
				'rules' => 'required|trim|xss_clean'
			),
			array(
				'field' => 'app_description',
				'label' => 'Deskripsi Aplikasi',
				'rules' => 'required|trim|xss_clean'
			),
			array(
				'field' => 'announcement_count',
				'label' => 'Jumlah Pengumuman',
				'rules' => 'required|integer|trim|xss_clean'
			),
		);

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == false ) {

			$response = (object) array(
				'result' => false,
				'message' => validation_errors()
			);

		} else {

			$data['app_title']                   = $this->input->post('app_title', TRUE);
			$data['app_description']             = $this->input->post('app_description', TRUE);
			$data['announcement_count']          = $this->input->post('announcement_count', TRUE);

			$this->db->trans_begin();

			// save settings
			foreach ( $data as $option_name => $option_value ) {

				if ( ! $this->option_model->where_option_name($option_name)->update(array('option_value' => $option_value)) ) {

					$this->error = true;

					array_push($this->error_messages, 'Error occured. Fail to save '.$option_name.' setting.');

				}

			}

			if ( $this->db->trans_status() == false ) {

				$this->db->trans_rollback();

				$response = (object) array(
					'result' => false,
					'message' => $this->error_messages
				);

			}
			else {

				$this->db->trans_commit();

				$response = (object) array(
					'result' => true
				);

			}

		}

		echo json_encode($response);

	}

}

/* End of file Setting_general.php */
/* Location: ./application/controllers/Setting_general.php */