<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message_model extends MY_Model {

	public $table = 'messages';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
	}


	/**
	 * get all messages
	 * @return mixed [object/boolean]
	 */
	public function get_all_messages($query=array())
	{

		if ( isset($query['where']) ) $this->db->where($query['where']);
		if ( isset($query['where_no_escape']) ) $this->db->where($query['where_no_escape'], null, false);
		if ( isset($query['or_where']) ) $this->db->or_where($query['or_where']);
		if ( isset($query['or_where_no_escape']) ) $this->db->or_where($query['or_where_no_escape'], null, false);
		if ( isset($query['limit']) ) $this->db->limit($query['limit']);
		if ( isset($query['limit_offset']) ) $this->db->limit($query['limit_offset'][0], $query['limit_offset'][1]);
		if ( isset($query['group_by']) ) $this->db->group_by($query['group_by']);
		
		if ( isset($query['join']) ) $this->db->join($query['join'][0], $query['join'][1]);
		if ( isset($query['left_join']) ) $this->db->join($query['left_join'][0], $query['left_join'][1], 'left');
		if ( isset($query['right_join']) ) $this->db->right($query['right_join'][0], $query['right_join'][1], 'right');

		if ( isset($query['order_by']) ) {

			if ( is_array($query['order_by']) ) {

				foreach ( $query['order_by'] as $column => $dir ) {

					$this->db->order_by($column, $dir);

				}

			} else {

				$this->db->order_by($query['order_by']);

			}

		}

		$this->db 
		     ->select('messages.id, messages.from, messages.to, messages.message, messages.timestamp')
		     ->select('users.id AS user_id, users.profile_picture AS avatar, users.first_name, users.last_name, CONCAT(users.first_name, " ", users.last_name) AS full_name')
		     ->from('messages')
		     ->order_by('messages.timestamp DESC');

		$result = $this->db->get();

		if ( $result->num_rows() > 0 ) {

			foreach ( $result->result() as $row ) {

				$data[] = (object) array(
					'id' => $row->id,
					'message' => $row->message,
					'message_brief' => character_limiter($row->message, 70),
					'to' => $row->to,
					'from' => $row->from,
					'full_name' => $row->full_name,
					'avatar' => $row->avatar,
					'timestamp' => $row->timestamp,
					'd' => date_format(date_create($row->timestamp), 'd'),
					'm' => date_format(date_create($row->timestamp), 'm'),
					'M' => date_format(date_create($row->timestamp), 'M'),
					'Y' => date_format(date_create($row->timestamp), 'Y'),
					'time' => date_format(date_create($row->timestamp), 'h:i:s'),
				);

			}

			return $data;

		}

		return false;

	}

}

/* End of file message_model.php */
/* Location: ./application/models/message_model.php */