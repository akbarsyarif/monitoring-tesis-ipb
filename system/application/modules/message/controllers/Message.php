<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		// load dependencies
		$this->load->model('message_model');

		// authorization
		if ( ! $this->sp_auth->isLoggedIn() ) {

			show_error('Access forbidden.');

		}
	}


	/**
	 * get all inbox
	 * @return mixed [object/boolean]
	 */
	public function get_all_inbox()
	{

		$query = NULL;

		// user input
		$limit   = $this->input->get('limit', TRUE);
		$offset  = $this->input->get('offset', TRUE);
		$user_id = $this->sp_auth->userData()->user_id;

		// display only inbox from current user
		$query['where']['messages.to'] = $user_id;

		// join
		$query['left_join'] = array('users', 'users.id = messages.from');

		// set limit and offset
		$query['limit_offset'] = array($limit, $offset);

		// group by
		$query['group_by'] = 'messages.from';

		$data = $this->message_model->get_all_messages($query);

		$total_records = count($data);

		if ( $data == false ) {

			$response = (object) array(
				'result' => false,
				'total_records' => $total_records
			);

		}
		else {

			$response = (object) array(
				'result' => true,
				'total_records' => $total_records,
				'data' => $data
			);

		}

		echo json_encode($response);

	}


	/**
	 * get all sent items
	 * @return mixed [object/boolean]
	 */
	public function get_all_sent_items()
	{

		$query = NULL;

		// user input
		$limit   = $this->input->get('limit', TRUE);
		$offset  = $this->input->get('offset', TRUE);
		$user_id = $this->sp_auth->userData()->user_id;

		// display only sent items from current user
		$query['where']['messages.from'] = $user_id;

		// join
		$query['left_join'] = array('users', 'users.id = messages.to');

		// set limit and offset
		$query['limit_offset'] = array($limit, $offset);

		// group by
		$query['group_by'] = 'messages.to';

		$data = $this->message_model->get_all_messages($query);

		$total_records = count($data);

		if ( $data == false ) {

			$response = (object) array(
				'result' => false,
				'total_records' => $total_records,
				'message' => 'Percakapan tidak ditemukan.'
			);

		}
		else {

			$response = (object) array(
				'result' => true,
				'total_records' => $total_records,
				'data' => $data
			);

		}

		echo json_encode($response);

	}


	/**
	 * get full conversation
	 */
	public function get_conversation()
	{

		$query = NULL;
		
		// user input
		$limit   = $this->input->get('limit', TRUE);
		$offset  = $this->input->get('offset', TRUE);
		
		// current user id
		$user1_id = $this->sp_auth->userData()->user_id;
		
		// another user 
		$user2_id = $this->input->get('user2_id', TRUE);

		// display only inbox from current user
		$query['where_no_escape']["(messages.to"] = "$user1_id AND messages.from = $user2_id)";
		$query['or_where_no_escape']["(messages.from"] = "$user1_id AND messages.to = $user2_id)";

		// join
		$query['left_join'] = array('users', 'users.id = messages.from');

		// set limit and offset
		$query['limit_offset'] = array($limit, $offset);

		$data = $this->message_model->get_all_messages($query);

		$total_records = count($data);

		if ( $data == false ) {

			$response = (object) array(
				'result' => false,
				'total_records' => $total_records,
				'message' => 'Percakapan tidak ditemukan.'
			);

		}
		else {

			// change read status
			$this->change_read_status($user2_id, $user1_id);

			$response = (object) array(
				'result' => true,
				'total_records' => $total_records,
				'data' => $data
			);

		}

		echo json_encode($response);

	}


	/**
	 * send message
	 */
	public function send()
	{

		// form validation
		$rules = array(
			array(
				'field' => 'to',
				'label' => 'Tujuan',
				'rules' => 'required|trim|xss_clean'
			),
			array(
				'field' => 'message',
				'label' => 'Pesan',
				'rules' => 'required|trim|xss_clean'
			)
		);

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == false ) {

			$response = (object) array(
				'result' => false,
				'message' => validation_errors()
			);

		}
		else {

			$data['from']      = $this->sp_auth->userData()->user_id;
			$data['to']        = $this->input->post('to', TRUE);
			$data['message']   = nl2br($this->input->post('message', TRUE));
			$data['timestamp'] = date('Y-m-d h:i:s');

			if ( ! $this->message_model->insert($data) ) {

				$response = (object) array(
					'result' => false,
					'message' => 'Error occured. Fail to send message.'
				);

			}
			else {

				$response = (object) array(
					'result' => true
				);

			}

		}

		echo json_encode($response);

	}


	/**
	 * count unread message
	 */
	public function count_unread()
	{

		$user_id = $this->sp_auth->userData()->user_id;

		$total_records = $this->message_model->where(array('to' => $user_id, 'read' => false))->count();

		// display only inbox from current user
		$query['where']['messages.to'] = $user_id;

		// join
		$query['left_join'] = array('users', 'users.id = messages.from');

		// set limit and offset
		$query['limit_offset'] = array(10, 0);

		// group by
		$query['group_by'] = 'messages.from';

		$data = $this->message_model->get_all_messages($query);

		$response = (object) array(
			'count' => $total_records,
			'data' => $data
		);

		echo json_encode($response);

	}


	/**
	 * change read status
	 */
	private function change_read_status($from, $to)
	{

		$data['read'] = true;

		if ( $this->message_model->where(array('from' => $from, 'to' => $to))->update($data) ) {

			return true;

		}

		return false;

	}

}

/* End of file message.php */
/* Location: ./application/controllers/message.php */