<div class="x_panel">
	<div class="x_content">
		<ul class="announcements list-unstyled">
			<?php if ( ! $pengumuman ) : ?>
				<div class="alert alert-info">Tidak ada pengumuman untuk ditampilkan.</div>
			<?php else : ?>
				<?php $i=1; foreach ( $pengumuman as $row ) : ?>
					<?php 
					if ( $row->type == 'penting' ) $label_type = 'label-success';
					if ( $row->type == 'himbauan' ) $label_type = 'label-info';
					if ( $row->type == 'peringatan' ) $label_type = 'label-danger';
					?>
					<li>
						<div class="media">
							<a class="pull-left" href="#">
								<h1><strong>#<?= $i ?></strong></h1>
							</a>
							<div class="media-body">
								<span><strong><?= $row->tanggal_pengumuman_indonesia ?></strong></span> oleh <a><?= $row->full_name ?></a>
								<span class="pull-right label <?= $label_type ?>"><?= $row->type ?></span>
								<p><?= $row->pengumuman ?></p>
							</div>
						</div>
					</li>
				<?php $i++; endforeach; ?>
			<?php endif; ?>
		</ul>
	</div>
</div>