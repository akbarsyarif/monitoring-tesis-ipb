<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="pull-left">
					<button class="btn btn-round btn-sm btn-info" id="advance-search-toggle-btn"><i class="fa fa-fw fa-search-plus"></i> Filter Data</button>
				</div>
				<div class="pull-right">
					<!-- put something here -->
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<!-- advance search -->
				<div class="advance-search-form well">
					<h4 class="title"><i class="fa fa-fw fa-search-plus"></i> Filter Data</h4>
					<?= form_open('', array(
						'class' => 'form-horizontal',
						'id' => 'filter_form',
						'onsubmit' => 'return false'
					)); ?>
						<div class="form-group">
							<?= form_label('Nrp', '', array('class' => 'control-label col-md-2')) ?>
							<div class="col-md-10">
								<?= form_input(array(
									'name' => 'nrp',
									'value' => '',
									'class' => 'form-control',
									'data-column' => "2"
								)) ?>
							</div>
						</div>
						<div class="form-group">
							<?= form_label('Nama', '', array('class' => 'control-label col-md-2')) ?>
							<div class="col-md-10">
								<?= form_input(array(
									'name' => 'nama',
									'value' => '',
									'class' => 'form-control',
									'data-column' => "3"
								)) ?>
							</div>
						</div>
						<div class="form-group">
							<?= form_label('Angkatan', '', array('class' => 'control-label col-md-2')); ?>
							<div class="col-md-10">
								<?= form_dropdown(array(
									'name' => '',
									'options' => $angkatan,
									'selected' => '',
									'class' => 'form-control select2-single',
									'data-column' => "4"
								)) ?>
							</div>
						</div>
						<div class="col-sm-offset-2 col-sm-10 footer">
							<button type="button" class="btn btn-warning btn-sm btn-round" id="clear_filter_btn"><i class="fa fa-fw fa-times-circle"></i> Clear Filter</button>
							<button type="submit" class="btn btn-dark btn-sm btn-round" id="filter_btn"><i class="fa fa-fw fa-check-circle"></i> Filter</button>
						</div>
					<?= form_close(); ?>
				</div>

				<table class="table table-striped" id="data-list" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th width="10"><?= form_checkbox('ids', ''); ?></th>
							<th width="10"></th>
							<th>Nrp</th>
							<th>Nama</th>
							<th>Angkatan</th>
							<th>Fakultas</th>
							<th width="20"></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- javascript -->
<script type="text/javascript">
	function format ( d ) {
		// `d` is the original data object for the row
		var html = '';

		if ( d.nrp == '' || d.nrp == null ) d.nrp = '<i>Belum diisi</i>';
		if ( d.nama == '' || d.nama == null ) d.nama = '<i>Belum diisi</i>';
		if ( d.ipk == '' || d.ipk == null ) d.ipk = '<i>Belum diisi</i>';
		if ( d.fakultas == '' || d.fakultas == null ) d.fakultas = '<i>Belum diisi</i>';
		if ( d.program_studi == '' || d.program_studi == null ) d.program_studi = '<i>Belum diisi</i>';
		if ( d.kelas == '' || d.kelas == null ) d.kelas = '<i>Belum diisi</i>';
		if ( d.angkatan == '' || d.angkatan == null ) d.angkatan = '<i>Belum diisi</i>';
		if ( d.tanggal_masuk == '' || d.tanggal_masuk == null || d.tanggal_lulus == '0000-00-00' ) d.tanggal_masuk = '<i>Belum diisi</i>';
		if ( d.semester == '' || d.semester == null ) d.semester = '<i>Belum diisi</i>';
		if ( d.pt_asal == '' || d.pt_asal == null ) d.pt_asal = '<i>Belum diisi</i>';
		if ( d.instansi == '' || d.instansi == null ) d.instansi = '<i>Belum diisi</i>';
		if ( d.no_hp == '' || d.no_hp == null ) d.no_hp = '<i>Belum diisi</i>';
		if ( d.telp_instansi == '' || d.telp_instansi == null ) d.telp_instansi = '<i>Belum diisi</i>';
		if ( d.email == '' || d.email == null ) d.email = '<i>Belum diisi</i>';
		if ( d.judul_ta == '' || d.judul_ta == null ) d.judul_ta = '<i>Belum diisi</i>';
		if ( d.lab == '' || d.lab == null ) d.lab = '<i>Belum diisi</i>';
		if ( d.nama_dosen_pembimbing_1 == '' || d.nama_dosen_pembimbing_1 == null ) d.nama_dosen_pembimbing_1 = '<i>Belum diisi</i>';
		if ( d.nama_dosen_pembimbing_2 == '' || d.nama_dosen_pembimbing_2 == null ) d.nama_dosen_pembimbing_2 = '<i>Belum diisi</i>';
		if ( d.nama_dosen_pembimbing_3 == '' || d.nama_dosen_pembimbing_3 == null ) d.nama_dosen_pembimbing_3 = '<i>Belum diisi</i>';
		if ( d.nama_dosen_penguji == '' || d.nama_dosen_penguji == null ) d.nama_dosen_penguji = '<i>Belum diisi</i>';
		if ( d.tanggal_lulus == '' || d.tanggal_lulus == null || d.tanggal_lulus == '0000-00-00' ) d.tanggal_lulus = '<i>Belum diisi</i>';
		if ( d.lama_masa_studi == '' || d.lama_masa_studi == null || d.lama_masa_studi == '0' ) d.lama_masa_studi = '<i>Belum diisi</i>';

		html += '<div class="row detail-row">';
			html += '<div class="col-md-4">';
				html += '<table width="100%" class="padding">';
					html += '<tr><td width=130>NRP</td><td width=10>:</td><td>'+d.nrp+'</td></tr>';
					html += '<tr><td>Nama</td><td>:</td><td>'+d.nama+'</td></tr>';
					html += '<tr><td>IPK</td><td>:</td><td>'+d.ipk+'</td></tr>';
					html += '<tr><td>Fakultas</td><td>:</td><td>'+d.fakultas+'</td></tr>';
					html += '<tr><td>Program Studi</td><td>:</td><td>'+d.program_studi+'</td></tr>';
					html += '<tr><td>Kelas</td><td>:</td><td>'+d.kelas+'</td></tr>';
					html += '<tr><td>Angkatan</td><td>:</td><td>'+d.angkatan+'</td></tr>';
					html += '<tr><td>Tgl.Masuk</td><td>:</td><td>'+d.tanggal_masuk+'</td></tr>';
					html += '<tr><td>Semester</td><td>:</td><td>'+d.semester+'</td></tr>';
					html += '<tr><td>PT. Asal</td><td>:</td><td>'+d.pt_asal+'</td></tr>';
					html += '<tr><td>Instansi</td><td>:</td><td>'+d.instansi+'</td></tr>';
					html += '<tr><td>No.Handphone</td><td>:</td><td>'+d.no_hp+'</td></tr>';
					html += '<tr><td>Telp.Instansi</td><td>:</td><td>'+d.telp_instansi+'</td></tr>';
					html += '<tr><td>Email</td><td>:</td><td>'+d.email+'</td></tr>';
				html += '</table>';
			html += '</div>';
			html += '<div class="col-md-4">';
				html += '<table width="100%" class="padding">';
					html += '<tr><td width=130>Judul TA</td><td width=10>:</td><td>'+d.judul_ta+'</td></tr>';
					html += '<tr><td>Lab</td><td>:</td><td>'+d.lab+'</td></tr>';
					html += '<tr><td>Dosen Pembimbing 1</td><td>:</td><td>'+d.nama_dosen_pembimbing_1+'</td></tr>';
					html += '<tr><td>Dosen Pembimbing 2</td><td>:</td><td>'+d.nama_dosen_pembimbing_2+'</td></tr>';
					html += '<tr><td>Dosen Pembimbing 3</td><td>:</td><td>'+d.nama_dosen_pembimbing_3+'</td></tr>';
					html += '<tr><td>Dosen Penguji</td><td>:</td><td>'+d.nama_dosen_penguji+'</td></tr>';
					html += '<tr><td>Tgl.Lulus</td><td>:</td><td>'+d.tanggal_lulus+'</td></tr>';
					html += '<tr><td>Lama Masa Studi</td><td>:</td><td>'+d.lama_masa_studi+'</td></tr>';
				html += '</table>';
			html += '</div>';
		html += '</div>';

		return html;
	}

	$(document).ready(function() {
		// ---------------------------------------------------------------------

		/**
		 * data list / datatable handler
		 */
		var table = $('#data-list').DataTable({
			ordering : true,
			order : [[ 4, "asc" ], [ 2, "asc" ]],
			pageLength : 10,
			processing : true,
			serverSide : true,
			ajax : '<?= site_url("operator_fakultas/progress/get_mahasiswa/datatable") ?>',
			columns : [
				{
					"orderable": false,
					"data": null,
					"defaultContent": "<input type='checkbox' name='id[]' value='' />"
				},
				{
					"className"      : 'details-control',
					"orderable"      : false,
					"data"           : null,
					"defaultContent" : '<i class="fa fa-fw fa-plus-circle"></i> <i class="fa fa-fw fa-minus-circle"></i>'
				},
				{ "data": "nrp" },
				{ "data": "nama" },
				{ "data": "angkatan" },
				{ "data": "fakultas" },
				{
					"orderable": false,
					"data": null,
					mRender: function(data, type, row) {
						return "<a href='<?= site_url('operator_fakultas/progress/detail/"+ row.id +"') ?>'>Detail</a>";
					}
				},
			],
		});

		// ---------------------------------------------------------------------

		// Add event listener for opening and closing details
		$('#data-list tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format(row.data()) ).show();
				tr.addClass('shown');

				// initialize magnific popup
				tr.next().find('a.magnific-popup').magnificPopup({
					type: 'image'
				});
			}
		});

		// ---------------------------------------------------------------------

		// filter usulan
		$('#filter_form').on('submit', function() {
			$(this).find('.form-control').each(function() {
				var i = $(this).attr('data-column');

				table.column(i).search($(this).val());
			});

			table.draw();
		});

		// ---------------------------------------------------------------------

		// clear filter
		$('#clear_filter_btn').on('click', function() {
			$('#filter_form').resetForm();

			$('#filter_form .form-control').each(function() {
				var i = $(this).attr('data-column');

				table.column(i).search($(this).val());
			});

			table.draw();

			$('#filter_form .select2-single').trigger('change');
		});

		// ---------------------------------------------------------------------
	});
</script>