<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<?= form_hidden('default_mahasiswa_id', $mahasiswa_id) ?>
		<div class="x_panel">
			<div class="x_title">
				<div class="pull-left">
					<button class="btn btn-round btn-sm btn-dark" data-toggle="modal" href='#modal-add-progress'><i class="fa fa-fw fa-plus-circle"></i> Tambah</button>
				</div>
				<div class="pull-right">
					<!-- put something here -->
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_section">
				<table class="table table-horizontal" style="margin-bottom: 0">
					<tr>
						<th width="200">NRP</th><td width="10">:</td><td colspan="4"><?= $mahasiswa->nrp ?></td>
					</tr>
					<tr>
						<th>Nama</th><td>:</td><td colspan="4"><?= $mahasiswa->nama ?></td>
					</tr>
					<tr>
						<th>Fakultas</th><td>:</td><td width="300"><?= $mahasiswa->fakultas ?></td>
						<th width="200">Program Studi</th><td width="10">:</td><td><?= (!empty($mahasiswa->program_studi)) ? $mahasiswa->program_studi : '<i class="text-light">Belum Dipilih</i>' ?></td>
					</tr>
					<tr>
						<th>Angkatan</th><td>:</td><td><?= (!empty($mahasiswa->angkatan)) ? $mahasiswa->angkatan : '<i class="text-light">Belum Dipilih</i>' ?></td>
						<th>Tgl. Masuk</th><td>:</td><td><?= ($mahasiswa->tanggal_masuk !== '0000-00-00') ? $mahasiswa->tanggal_masuk : '<i class="text-light">Belum Dipilih</i>' ?></td>
					</tr>
					<tr>
						<th>Semester</th><td>:</td><td><?= ($mahasiswa->semester !== 0) ? $mahasiswa->semester : '<i class="text-light">Belum Dipilih</i>' ?></td>
						<th>Kelas</th><td>:</td><td><?= (!empty($mahasiswa->kelas)) ? $mahasiswa->kelas : '<i class="text-light">Belum Dipilih</i>' ?></td>
					</tr>
					<tr>
						<th>Status</th><td>:</td><td colspan="4"><?= $mahasiswa->status_label ?></td>
					</tr>
					<tr>
						<th>PT. Asal</th><td>:</td><td colspan="4"><?= (!empty($mahasiswa->pt_asal)) ? $mahasiswa->pt_asal : '<i class="text-light">Belum Dipilih</i>' ?></td>
					</tr>
					<tr>
						<th>No. Handphone</th><td>:</td><td colspan="4"><?= (!empty($mahasiswa->no_hp)) ? $mahasiswa->no_hp : '<i class="text-light">Belum Dipilih</i>' ?></td>
					</tr>
					<tr>
						<th>Email</th><td>:</td><td colspan="4"><?= (!empty($mahasiswa->email)) ? $mahasiswa->email : '<i class="text-light">Belum Dipilih</i>' ?></td>
					</tr>
					<tr>
						<th>Instansi</th><td>:</td><td><?= (!empty($mahasiswa->instansi)) ? $mahasiswa->instansi : '<i class="text-light">Belum Dipilih</i>' ?></td>
						<th>Telp. Instansi</th><td>:</td><td><?= (!empty($mahasiswa->telp_instansi)) ? $mahasiswa->telp_instansi : '<i class="text-light">Belum Dipilih</i>' ?></td>
					</tr>
					<tr>
						<th>Lab</th><td>:</td><td colspan="4"><?= (!empty($mahasiswa->lab)) ? $mahasiswa->lab : '<i class="text-light">Belum Dipilih</i>' ?></td>
					</tr>
					<tr>
						<th>Judul Tugas Akhir</th><td>:</td><td colspan="4"><?= (!empty($mahasiswa->judul_ta)) ? $mahasiswa->judul_ta : '<i class="text-light">Belum Dipilih</i>' ?></td>
					</tr>
					<tr>
						<th>Dosen Pembimbing 1</th><td>:</td><td colspan="4"><?= (!empty($mahasiswa->nama_dosen_pembimbing_1)) ? $mahasiswa->nama_dosen_pembimbing_1 : '<i class="text-light">Belum Dipilih</i>' ?></td>
					</tr>
					<tr>
						<th>Dosen Pembimbing 2</th><td>:</td><td colspan="4"><?= (!empty($mahasiswa->nama_dosen_pembimbing_2)) ? $mahasiswa->nama_dosen_pembimbing_2 : '<i class="text-light">Belum Dipilih</i>' ?></td>
					</tr>
					<tr>
						<th>Dosen Pembimbing 3</th><td>:</td><td colspan="4"><?= (!empty($mahasiswa->nama_dosen_pembimbing_3)) ? $mahasiswa->nama_dosen_pembimbing_3 : '<i class="text-light">Belum Dipilih</i>' ?></td>
					</tr>
					<tr>
						<th>Dosen Penguji</th><td>:</td><td colspan="4"><?= (!empty($mahasiswa->nama_dosen_penguji)) ? $mahasiswa->nama_dosen_penguji : '<i class="text-light">Belum Dipilih</i>' ?></td>
					</tr>
					<tr>
						<th>Tgl. Lulus</th><td>:</td><td><?= ($mahasiswa->tanggal_lulus !== '0000-00-00') ? $mahasiswa->tanggal_lulus : '<i class="text-light">Belum Dipilih</i>' ?></td>
						<th>Lama Masa Studi</th><td>:</td><td><?= (!empty($mahasiswa->lama_masa_studi)) ? $mahasiswa->lama_masa_studi : '<i class="text-light">Belum Dipilih</i>' ?></td>
					</tr>
					<th>IPK</th><td>:</td><td colspan="4"><?= $mahasiswa->ipk ?></td>
				</table>
			</div>
			<div class="x_content">
				<table class="table table-striped" id="data-list" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Tanggal</th>
							<th>Progress</th>
							<th>Semester</th>
							<th>Nilai</th>
							<th width="20"></th>
							<th width="20"></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<?php require 'modal_add_progress.php' ?>
<?php require 'modal_edit_progress.php' ?>

<!-- javascript -->
<script type="text/javascript">
	$(document).ready(function() {
		//-----------------------------------------------------------

		var table = $('#data-list').DataTable({
			ordering : true,
			order : [[0, 'asc']],
			pageLength : 10,
			processing : true,
			serverSide : true,
			ajax : {
				url: '<?= site_url("operator_fakultas/progress/get_progress/datatable") ?>',
				data: function(d) {
					d.mahasiswa_id = $('[name="default_mahasiswa_id"]').val();
				}
			},
			columns : [
				{
					data: 'tanggal',
					mRender: function(data, type, row) {
						return row.tanggal_format_indonesia;
					}
				},
				{ data: 'nama_progress' },
				{ data: 'semester' },
				{ data: 'nilai' },
				{
					data: null,
					orderable: false,
					mRender: function(data, type, row) {
						return '<a href="javascript:void(0)" class="edit">Edit</a>';
					}
				},
				{
					data: null,
					orderable: false,
					mRender: function(data, type, row) {
						return '<a href="javascript:void(0)" class="delete">Delete</a>';
					}
				}
			],
		});

		//-----------------------------------------------------------

		$('#form-add-progress').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				$('#form-add-progress').find('[type="submit"]').button('loading');

				if ( $form.parsley().isValid() ) {
					return true;
				}

				return false;
			},
			success: function(r) {
				if ( r.metadata.code != "200" ) {
					alertify.error(r.metadata.message);
					console.log(r);
				}
				else {
					$('#modal-add-progress').modal('hide');
					$('#form-add-progress').resetForm();
					reinitializeSelect2Single('#form-add-progress .select2-single');
					alertify.success('Berhasil menyimpan data');
					table.ajax.reload(null, false);
				}
			},
			error: function(e) {
				alertify.error('Tidak dapat menghubungi server');
				console.log(e);
			},
			complete: function() {
				$('#form-add-progress').find('[type="submit"]').button('reset');
			}
		});

		//-----------------------------------------------------------

		$('#data-list tbody').on('click', '.edit', function() {
			$row = table.row( $(this).parents('tr') );
			var data = table.row( $(this).parents('tr') ).data();
			var id = data.id;

			$.ajax({
				url : '<?= site_url("operator_fakultas/progress/get_progress/json") ?>/'+id,
				dataType : 'json',
				success: function(r) {
					if ( r.metadata.code != "200" ) {
						alertify.error(r.metadata.message);
						console.log(r);
					}
					else {
						var data = r.response;

						$('#form-edit-progress [name="id"]').val(data.id);
						$('#form-edit-progress [name="tanggal"]').val(data.tanggal);
						$('#form-edit-progress [name="semester"]').val(data.semester);
						$('#form-edit-progress [name="nilai"]').val(data.nilai);

						$('#modal-edit-progress').modal('show');
					}
				},
				error: function(e) {
					alertify.error('Tidak dapat menghubungi server');
					console.log(e);
				}
			});
		});

		//-----------------------------------------------------------

		$('#form-edit-progress').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				$('#form-edit-progress').find('[type="submit"]').button('loading');

				if ( $form.parsley().isValid() ) {
					return true;
				}

				return false;
			},
			success: function(r) {
				if ( r.metadata.code != "200" ) {
					alertify.error(r.metadata.message);
					console.log(r);
				}
				else {
					$('#modal-edit-progress').modal('hide');
					$('#form-edit-progress').resetForm();
					alertify.success('Berhasil menyimpan perubahan');
					table.ajax.reload(null, false);
				}
			},
			error: function(e) {
				alertify.error('Tidak dapat menghubungi server');
				console.log(e);
			},
			complete: function() {
				$('#form-edit-progress').find('[type="submit"]').button('reset');
			}
		});

		//-----------------------------------------------------------

		$('#data-list tbody').on('click', '.delete', function() {
			var data = table.row( $(this).parents('tr') ).data();
			var id = data.id;

			// set alertify label
			alertify.set({
				labels : {
					ok : '<i class="fa fa-fw fa-check-circle"></i> Ya',
					cancel : '<i class="fa fa-fw fa-times-circle"></i> Tidak'
				}
			});
			alertify.confirm('Apakah anda yakin akan menghapus data?', function(e) {
				if ( e ) {
					// remove data
					$.ajax({
						url : '<?= site_url("operator_fakultas/progress/delete") ?>',
						dataType : 'json',
						data : { id : id },
						success : function(r) {
							if ( r.metadata.code != "200" ) {
								alertify.error(r.metadata.message);
								console.log(r);
							}
							else {
								alertify.success('Berhasil menghapus data');
								table.ajax.reload(null, false);
							}
						},
						error: function(e) {
							alertify.error('Tidak dapat menghubungi server');
							console.log(e);
						}
					});
				}
				else {
					return;
				}
			});
		});

		//-----------------------------------------------------------
	});
</script>