<div class="modal fade" id="modal-add-progress">
	<div class="modal-dialog">
		<div class="modal-content">
			<?= form_open('operator_fakultas/progress/save', 'id="form-add-progress" data-parsley-validate=""') ?>
				<?= form_hidden('mahasiswa_id', $mahasiswa_id) ?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Input Progress</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<?= form_label('Tanggal') ?>
						<?= form_input(array(
							'name' => 'tanggal',
							'value' => '',
							'class' => 'form-control required datepicker'
						)) ?>
					</div>
					<div class="form-group">
						<?= form_label('Progress', 'progress_id') ?>
						<?= dinamyc_dropdown(array(
							'name' => 'progress_id',
							'table' => 'progress',
							'key' => 'id',
							'label' => 'nama',
							'default' => '',
							'order_by' => 'nama asc',
							'empty_first' => TRUE,
							'empty_first_label' => '-- Pilih Progress --',
							'attr' => 'class="form-control required select2-single"'
						)) ?>
					</div>
					<div class="form-group">
						<?= form_label('Semester') ?>
						<?= form_input(array(
							'name' => 'semester',
							'value' => '',
							'class' => 'form-control required input-mask-numeric'
						)) ?>
					</div>
					<div class="form-group">
						<?= form_label('Nilai') ?>
						<?= form_input(array(
							'name' => 'nilai',
							'value' => '',
							'class' => 'form-control'
						)) ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm btn-round" data-dismiss="modal"><i class="fa fa-fw fa-times-circle"></i> Batal</button>
					<button type="submit" class="btn btn-dark btn-sm btn-round" data-loading-text="<i class='fa fa-fw fa-circle-o-notch'></i> Tunggu..."><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				</div>
			<?= form_close() ?>
		</div>
	</div>
</div>