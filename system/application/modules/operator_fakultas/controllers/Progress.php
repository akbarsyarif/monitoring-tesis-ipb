<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Progress extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		// load dependencies
		$this->load->model('administrator/mahasiswa_model');
		$this->load->model('administrator/progress_mahasiswa_model');
	}

	public function index()
	{
		$this->data['page_title'] = 'Progress Mahasiswa';

		$userData = $this->sp_auth->userData();

		// angkatan array
		$angkatan = $this->mahasiswa_model->getAngkatan(array(
			'where' => array(
				'fakultas_id' => $userData->fakultas_id
			)
		));
		$angkatan = array('' => '-- Semua --') + $angkatan;
		$this->data['angkatan'] = $angkatan;

		$this->template->build('progress/index', $this->data);
	}

	/**
	 * ajax handler
	 * menampilkan daftar progress mahasiswa
	 * @return void
	 */
	public function get_mahasiswa($format='json', $id=null)
	{
		if ( $this->input->is_ajax_request() === false ) {
			show_404();
		}
		else {
			if ( $format == 'datatable' ) {
				$this->_datatable_mahasiswa();
			}
			else {
				if ( ! is_null($id) ) {
					$data = $this->mahasiswa_model->get_manual(array(
						'where' => array(
							'mahasiswa.id' => $id
							)
						));

					if ( $data === false ) {
						$response = array(
							'result' => false,
							'message' => 'Data tidak ditemukan'
							);
					}
					else {
						$response = array(
							'result' => true,
							'data' => $data
							);
					}
				}
				else {
					$data = $this->mahasiswa_model->get_all_manual();

					if ( $data === false ) {
						$response = array(
							'result' => false,
							'message' => 'Data tidak ditemukan'
							);
					}
					else {
						$response = array(
							'result' => true,
							'data' => $data
							);
					}
				}

				echo json_encode($response);
			}
		}
	}

	/**
	 * ajax handler
	 * menampilkan data progress mahasiswa dalam format json dataTable
	 * @return void
	 */
	public function _datatable_mahasiswa()
	{
		$query = array();

		// columns index
		$column_index = array(null, null, 'mahasiswa.nrp', 'mahasiswa.nama', 'mahasiswa.angkatan', 'fakultas.fakultas', null);

		$fakultas_id = $this->sp_auth->userData()->fakultas_id;

		$query['where']['mahasiswa.fakultas_id'] = $fakultas_id;

		// filter column handler / search
		$columns = $_GET['columns'];

		if ( ! empty($_GET['search']['value']) ) {

			$search_value = xss_clean($_GET['search']['value']);

			// global search
			$query['where'][$column_index[3].' like '] = "%$search_value%";

		}
		else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					$query['where'][$column_index[$column_key].' like '] = '%'.$column_val['search']['value'].'%';

				}

			}

		}

		$records_total = count($this->mahasiswa_model->get_all_manual($query));
		$records_filtered = $records_total;

		// orders
		$order_column = $_GET['order'][0]['column'];
		$order_dir = $_GET['order'][0]['dir'];

		$query['limit_offset'] = array($_GET['length'], $_GET['start']);
		$query['order_by'] = $column_index[$order_column].' '.$order_dir;

		$data = $this->mahasiswa_model->get_all_manual($query);

		$response = (object) array (
			'draw' => $_GET['draw'],
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_filtered,
			'data' => $data
		);

		echo json_encode($response);
	}

	/**
	 * menampilkan detail progress mahasiswa
	 * @param  [int] $id [id mahasiswa]
	 * @return void
	 */
	public function detail($id)
	{
		$this->data['page_title'] = 'Detail Progress';
		$this->data['mahasiswa_id'] = $id;

		// data mahasiswa
		$this->data['mahasiswa'] = $this->mahasiswa_model->get_manual(array(
			'where' => array(
				'mahasiswa.id' => $id
			)
		));

		$this->template->build('progress/detail', $this->data);
	}

	/**
	 * menampilkan progress mahasiswa
	 * @param  string $format [description]
	 * @param  [type] $id     [description]
	 * @return [type]         [description]
	 */
	public function get_progress($format='json', $id=null)
	{
		if ( $this->input->is_ajax_request() === false ) {
			show_404();
		}
		else {
			if ( $format == 'datatable' ) {
				$this->_datatable_progress();
			}
			else {
				if ( ! is_null($id) ) {
					$data = $this->progress_mahasiswa_model->row(array(
						'where' => array(
							'progress_mahasiswa.id' => $id
							)
						));

					if ( $data === false ) {
						$response = (object) array (
							'metadata' => (object) array (
								'code' => "201",
								'message' => 'Data progress tidak ditemukan'
							),
							'response' => NULL
						);
					}
					else {
						$response = (object) array (
							'metadata' => (object) array (
								'code' => "200",
								'message' => 'OK'
							),
							'response' => $data
						);
					}
				}
				else {
					$data = $this->progress_mahasiswa_model->get();

					if ( $data === false ) {
						$response = (object) array (
							'metadata' => (object) array (
								'code' => "201",
								'message' => 'Data progress tidak ditemukan'
							),
							'response' => NULL
						);
					}
					else {
						$response = (object) array (
							'metadata' => (object) array (
								'code' => "200",
								'message' => 'OK'
							),
							'response' => $data
						);
					}
				}

				echo json_encode($response);
			}
		}
	}

	/**
	 * ajax handler
	 * menampilkan data progress dalam format datatable
	 * @return void
	 */
	public function _datatable_progress()
	{
		$query = array();

		// columns index
		$column_index = array('progress_mahasiswa.tanggal', 'progress.nama', 'progress_mahasiswa.semester', 'progress_mahasiswa.nilai', null, null);

		// where confition default
		$query['where']['mahasiswa.id'] = $this->input->get('mahasiswa_id', TRUE);

		// filter column handler / search
		$columns = $_GET['columns'];

		if ( ! empty($_GET['search']['value']) ) {

			$search_value = xss_clean($_GET['search']['value']);

			// global search
			$query['where'][$column_index[1].' like '] = "%$search_value%";

		}
		else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( ($column_key == 2) OR ($column_key == 3) ) {

						$query['where'][$column_index[$column_key]] = $column_val['search']['value'];

					} else {

						$query['where'][$column_index[$column_key].' like '] = '%'.$column_val['search']['value'].'%';

					}

				}

			}

		}

		$records_total = count($this->progress_mahasiswa_model->get($query));
		$records_filtered = $records_total;

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $_GET['order'] as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order_by'][$column_index[$order_column]] = $order_dir;
			}
		}

		// default orders
		$query['order_by']['mahasiswa.id'] = 'asc';

		// limit
		$query['limit_offset'] = ($_GET['length'] > 0) ? array($_GET['length'], $_GET['start']) : NULL;

		$data = $this->progress_mahasiswa_model->get($query);

		$response = (object) array (
			'draw' => $_GET['draw'],
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_filtered,
			'data' => $data
		);

		echo json_encode($response);
	}

	/**
	 * ajax handler
	 * insert progress
	 * @return [type] [description]
	 */
	public function save()
	{
		$mahasiswa_id = $this->input->post('mahasiswa_id', TRUE);
		$progress_id = $this->input->post('progress_id', TRUE);
		$userData = $this->sp_auth->userData();

		// cek apakah mahasiswa terdaftar di fakultas ini
		$result = $this->mahasiswa_model->isInThisFaculty($mahasiswa_id, $userData->fakultas_id);

		if ( $result === FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "201",
					'message' => "Mahasiswa tidak terdaftar di fakultas ini"
				),
				'response' => NULL
			);

		}
		else {
			// jika progress telah terdaftar, maka update progress

			// cek apakah progress sudah terdaftar
			$result = $this->progress_mahasiswa_model->isExist($mahasiswa_id, $progress_id);

			if ( $result === FALSE ) {

				$data = array (
					'mahasiswa_id' => $this->input->post('mahasiswa_id', TRUE),
					'progress_id' => $this->input->post('progress_id', TRUE),
					'tanggal' => $this->input->post('tanggal', TRUE),
					'semester' => $this->input->post('semester', TRUE),
					'nilai' => $this->input->post('nilai', TRUE),
				);

				$result = $this->progress_mahasiswa_model->insert($data);

				if ( $result === FALSE ) {

					$response = (object) array (
						'metadata' => (object) array (
							'code' => "201",
							'message' => "Terjadi error saat mencoba menyimpan data"
						),
						'response' => NULL
					);

				}
				else {

					$response = (object) array (
						'metadata' => (object) array (
							'code' => "200",
							'message' => "OK"
						),
						'response' => NULL
					);

				}

			}
			else {

				$id = $result;

				$data = array (
					'tanggal' => $this->input->post('tanggal', TRUE),
					'semester' => $this->input->post('semester', TRUE),
					'nilai' => $this->input->post('nilai', TRUE),
				);

				$result = $this->progress_mahasiswa_model->update($data, $id);

				if ( $result === FALSE ) {

					$response = (object) array (
						'metadata' => (object) array (
							'code' => "201",
							'message' => "Terjadi error saat mencoba mengupdate data"
						),
						'response' => NULL
					);

				}
				else {

					$response = (object) array (
						'metadata' => (object) array (
							'code' => "200",
							'message' => "OK"
						),
						'response' => NULL
					);

				}

			}

		}

		echo json_encode($response);
	}

	/**
	 * ajax handler
	 * update progress mahasiswa
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function update()
	{
		$id = $this->input->post('id', TRUE);
		$userData = $this->sp_auth->userData();

		// cek apakah progress terdaftar
		$result_progress = $this->progress_mahasiswa_model->row(array(
			'where' => array(
				'progress_mahasiswa.id' => $id
			)
		));

		if ( $result_progress === FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "201",
					'message' => "Data progress tidak ditemukan"
				),
				'response' => NULL
			);

		}
		else {

			// cek apakah mahasiswa terdaftar di fakultas ini
			$result = $this->mahasiswa_model->isInThisFaculty($result_progress->mahasiswa_id, $userData->fakultas_id);

			if ( $result === FALSE ) {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "201",
						'message' => "Mahasiswa tidak terdaftar di fakultas ini"
					),
					'response' => NULL
				);

			}
			else {

				$data = array (
					'tanggal' => $this->input->post('tanggal', TRUE),
					'semester' => $this->input->post('semester', TRUE),
					'nilai' => $this->input->post('nilai', TRUE),
				);

				$result = $this->progress_mahasiswa_model->update($data, $id);

				if ( $result === FALSE ) {

					$response = (object) array (
						'metadata' => (object) array (
							'code' => "201",
							'message' => "Terjadi error saat mencoba mengupdate data"
						),
						'response' => NULL
					);

				}
				else {

					$response = (object) array (
						'metadata' => (object) array (
							'code' => "200",
							'message' => "OK"
						),
						'response' => NULL
					);

				}

			}

		}

		echo json_encode($response);
	}

	public function delete()
	{
		$id = $this->input->get('id', TRUE);

		// cek apakah id terdaftar
		$result = $this->progress_mahasiswa_model->row( array (
			'where' => array (
				'progress_mahasiswa.id' => $id
			)
		) );

		if ( $result === FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "201",
					'message' => "ID tidak ditemukan"
				),
				'response' => NULL
			);

		}
		else {

			$result = $this->progress_mahasiswa_model->delete($id);

			if ( $result === FALSE ) {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "202",
						'message' => "Terjadi error saat mencoba menghapus data"
					),
					'response' => NULL
				);

			}
			else {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "200",
						'message' => "OK"
					),
					'response' => NULL
				);

			}

		}

		echo json_encode($response);
	}

}

/* End of file Progress.php */
/* Location: ./application/controllers/Progress.php */