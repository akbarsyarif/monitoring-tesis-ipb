<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perkembangan_tesis extends SP_Admin_Controller {

	public $userData;
	
	public function __construct()
	{
		parent::__construct();

		// load dependencies
		$this->load->model('administrator/mahasiswa_model');
		$this->load->model('administrator/progress_mahasiswa_model');
		$this->load->model('administrator/laporan_model');

		$this->userData = $this->sp_auth->userData();

		// cek apakah user yang sedang login bukan operator fakultas
		// jika bukan maka set fakultas_id menjadi NULL
		if ( $this->sp_auth->isInThisGroup('operator_fakultas') == FALSE ) {

			$this->userData->fakultas_id = NULL;

		}
	}

	public function semua_mahasiswa()
	{
		$this->data['page_title'] = 'Perkembangan Tesis Mahasiswa';
		$this->data['page_sub_title'] = 'Semua Mahasiswa';

		// data perkembangan tesis
		$this->data['data'] = $this->laporan_model->getPerkembanganTesis($this->userData->fakultas_id);

		$this->template->build('perkembangan_tesis/semua_mahasiswa/index', $this->data);
	}

	public function per_angkatan()
	{
		$this->data['page_title'] = 'Perkembangan Tesis Mahasiswa';
		$this->data['page_sub_title'] = 'Per Angkatan';

		$query = NULL;

		if ( $this->sp_auth->isInThisGroup('operator_fakultas') ) {
			$query['where'] = array('fakultas_id' => $this->userData->fakultas_id);
		}

		// angkatan array
		$angkatan_arr = $this->mahasiswa_model->getAngkatan($query);
		$angkatan_dropdown = array('' => '-- Semua --') + $angkatan_arr;
		$this->data['angkatan_dropdown'] = $angkatan_dropdown;

		// data perkembangan tesis
		$angkatan = $this->input->post('angkatan');
		$this->data['angkatan'] = $angkatan;
		$this->data['data'] = $this->laporan_model->getPerkembanganTesis($this->userData->fakultas_id, $angkatan);

		$this->template->build('perkembangan_tesis/per_angkatan/index', $this->data);
	}

	public function per_pembimbing()
	{
		$this->data['page_title'] = 'Perkembangan Tesis Mahasiswa';
		$this->data['page_sub_title'] = 'Per Pembimbing';

		// data perkembangan tesis
		$pembimbing = $this->input->post('pembimbing');
		$this->data['pembimbing'] = $pembimbing;
		$this->data['data'] = $this->laporan_model->getPerkembanganTesis($this->userData->fakultas_id, NULL, $pembimbing);

		$this->template->build('perkembangan_tesis/per_pembimbing/index', $this->data);
	}

}

/* End of file Perkembangan_tesis.php */
/* Location: ./application/controllers/Perkembangan_tesis.php */