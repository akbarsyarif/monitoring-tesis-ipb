<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_content">
				<div class="row">
					<div class="col-md-5">
						<canvas id="report-chart"></canvas>
					</div>
					<div class="col-md-7">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th width="10">No.</th>
									<th>Tahapan</th>
									<th>Jumlah</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>Belum Kolokium</td>
									<td><?= $data->belum_kolokium ?></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Kolokium</td>
									<td><?= $data->kolokium ?></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Seminar</td>
									<td><?= $data->seminar ?></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Sidang</td>
									<td><?= $data->sidang ?></td>
								</tr>
								<tr>
									<td>5</td>
									<td>Lulus</td>
									<td><?= $data->lulus ?></td>
								</tr>
							</tbody>
							<thead>
								<tr>
									<th colspan="2">Total</th>
									<th><?= $data->total ?></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var data = {
		labels: [
			"Belum Kolokium",
			"Kolokium",
			"Seminar",
			"Sidang",
			"Lulus",
		],
		datasets: [
			{
				data: [
					<?= number_format(($data->belum_kolokium * 100) / $data->total, 0) ?>,
					<?= number_format(($data->kolokium * 100) / $data->total, 0) ?>,
					<?= number_format(($data->seminar * 100) / $data->total, 0) ?>,
					<?= number_format(($data->sidang * 100) / $data->total, 0) ?>,
					<?= number_format(($data->lulus * 100) / $data->total, 0) ?>,
				],
				backgroundColor: [
					"#FF6384",
					"#36A2EB",
					"#FFCE56",
					"#D62020",
					"#1AB01A",
				],
				hoverBackgroundColor: [
					"#999999",
					"#999999",
					"#999999",
					"#999999",
					"#999999",
				]
			}]
	};

	var ctx = $('#report-chart');
	var myPieChart = new Chart(ctx,{
		type: 'pie',
		data: data
	});
</script>