<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		// load dependencies
		$this->load->model('user_model');
		$this->load->model('group_model');
	}

	
	/**
	 * edit user profile
	 */
	public function index()
	{

		$this->data['page_title'] = 'Profile';
		$this->data['page_sub_title'] = 'Profil Pengguna';

		$this->template->build('profile', $this->data);

	}


	/**
	 * change avatar
	 */
	public function change_avatar()
	{

		// user id
		$uid = $this->sp_auth->userData()->user_id;

		$config['upload_path'] = './content/uploads/avatar/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']  = '500';
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('avatar') ) {

			$response = (object) array(
				'result' => false,
				'message' => $this->upload->display_errors()
			);
		
		} else {

			$upload_data = $this->upload->data();

			$profile_picture = base_url('content/uploads/avatar/'.$upload_data['file_name']);

			$data['profile_picture'] = $profile_picture;
	
			if ( ! $this->user_model->where('id', $uid)->update($data) ) {

				$response = (object) array(
					'result' => false,
					'message' => 'Error occured. Fail to update profile picture.'
				);

			} else {

				$response = (object) array(
					'result' => true,
					'new_profile_picture' => $profile_picture
				);

			}
		
		}

		echo json_encode($response);

	}


	/**
	 * update profile
	 */
	public function update_profile()
	{

		$uid = $this->sp_auth->userData()->user_id;

		// form validation
		$rules[] = array(
			'field' => 'first_name',
			'label' => 'Nama Depan',
			'rules' => 'required|trim|xss_clean'
		);
		$rules[] = array(
			'field' => 'last_name',
			'label' => 'Nama Belakang',
			'rules' => 'trim|xss_clean'
		);
		$rules[] = array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'required|valid_email|trim|xss_clean'
		);

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == false ) {

			$response = (object) array(
				'result' => false,
				'message' => validation_errors()
			);

		} else {

			$data['first_name'] = $this->input->post('first_name', TRUE);
			$data['last_name']  = $this->input->post('last_name', TRUE);
			$data['email']      = $this->input->post('email', TRUE);

			if ( ! $this->user_model->where('id', $uid)->update($data) ) {

				$response = (object) array(
					'result' => false,
					'message' => 'Error occured. Fail to update profile.'
				);

			} else {

				$response = (object) array(
					'result' => true
				);

			}

		}

		echo json_encode($response);

	}


	/**
	 * change/update password
	 */
	public function update_password()
	{

		$uid = $this->sp_auth->userData()->user_id;

		// form validation
		$rules = array(
			array(
				'field' => 'old_password',
				'label' => 'Kata Sandi Lama',
				'rules' => 'required|trim|xss_clean'
			),
			array(
				'field' => 'new_password',
				'label' => 'Kata Sandi Baru',
				'rules' => 'required|trim|xss_clean'
			),
			array(
				'field' => 'confirm_password',
				'label' => 'Ulangi Kata Sandi',
				'rules' => 'required|trim|xss_clean'
			),
		);

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == false ) {

			$response = (object) array(
				'result' => false,
				'message' => validation_errors()
			);

		} else {

			$old_password = $this->sp_auth->passHash($this->input->post('old_password', TRUE));

			// check old password
			$result = $this->user_model->where(array('id' => $uid, 'password' => $old_password))->count();

			if ( ! $result ) {

				$response = (object) array(
					'result' => false,
					'message' => 'Kata sandi yang anda masukkan salah.'
				);

			} else {

				$data['password'] = $this->sp_auth->passHash($this->input->post('new_password', TRUE));

				if ( ! $this->user_model->where('id', $uid)->update($data) ) {

					$response = (object) array(
						'result' => false,
						'message' => 'Error occured. Fail to update profile.'
					);

				} else {

					$response = (object) array(
						'result' => true
					);

				}

			}

		}

		echo json_encode($response);

	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */