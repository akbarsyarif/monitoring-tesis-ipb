<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authentication extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		// load dependencies
		$this->load->model('user_model');

		// set layout
		$this->template->set_layout('login');
	}


	/**
	 * display login page
	 */
	public function login()
	{

		// prevent logged in user to access login page
		// redirect to dashboard
		if ( $this->sp_auth->isLoggedIn() ) {

			$uid = $this->sp_auth->userData()->user_id;

			if ( $this->sp_auth->isInThisGroup('administrator', $uid) ) {

				$dst = 'administrator/dashboard';
				
			} else if ( $this->sp_auth->isInThisGroup('operator_fakultas', $uid) ) {

				$dst = 'operator_fakultas/dashboard';

			} else {

				show_404();

			}

			redirect($dst);

		}

		if ( $this->input->post() ) {

			$this->_auth();

		}

		$this->template->build('login');

	}


	/**
	 * user authentication by username and password
	 * if username and password match, then create 
	 * login session for this user
	 * @access private
	 */
	private function _auth()
	{

		$username = $this->input->post('username', TRUE);
		$password = $this->input->post('password', TRUE);

		// hash password
		$pass_hash = $this->sp_auth->passHash($password);

		if ( $this->user_model->auth($username, $pass_hash) ) {

			// get user data
			$user = $this->user_model->where_username($username)->with_groups()->get();

			// create login session
			$session['user_id'] = $user->id;
			$session['logged_in'] = true;

			// default redirect destination after login
			$dst = 'logout';

			if ( $this->sp_auth->isInThisGroup('administrator', $user->id) ) {

				$dst = 'administrator/dashboard';
				
			}

			if ( $this->sp_auth->isInThisGroup('operator_fakultas', $user->id) ) {

				$session['fakultas_id'] = $user->fakultas_id;

				$dst = 'operator_fakultas/dashboard';

			}

			// create login session
			$this->session->set_userdata($session);

			redirect($dst);

		} else {

			foreach ( $this->user_model->error_message as $error ) {
				$error_messages .= $error;
			}

			set_flash_alert($error_messages, 'danger');

			redirect('login');

		}

	}


	/**
	 * Logout
	 * destroy current session
	 */
	public function logout()
	{

		$this->sp_auth->logout();

		redirect('login','refresh');

	}

}

/* End of file Authentication.php */
/* Location: ./application/controllers/Authentication.php */