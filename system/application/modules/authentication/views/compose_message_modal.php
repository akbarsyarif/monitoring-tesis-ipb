<div class="modal fade" id="compose_message_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<?= form_open('message/send', array('class' => 'compose_message_form', 'data-parsley-validate' => true)); ?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><i class="fa fa-fw fa-pencil"></i> Tulis Pesan</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<?= form_label('Tujuan'); ?>
						<?= dinamyc_dropdown(array(
							'name' => 'to',
							'table' => 'users',
							'key' => 'id',
							'label' => array('first_name', 'last_name'),
							'separator' => ' ',
							'default' => '',
							'empty_first' => true,
							'empty_first_label' => '[ Masukkan Tujuan ]',
							'attr' => 'class="form-control select2-single" required=""'
						)) ?>
					</div>
					<div class="form-group">
						<?= form_label('Pesan'); ?>
						<?= form_textarea(array(
							'name' => 'message',
							'value' => '',
							'rows' => 10,
							'class' => 'form-control',
							'required' => true
						)) ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-round btn-default" data-dismiss="modal"><i class="fa fa-fw fa-times-circle"></i> Batal</button>
					<button type="submit" class="btn btn-sm btn-round btn-dark"><i class="fa fa-fw fa-check-circle"></i> Kirim</button>
				</div>
			<?= form_close(); ?>
		</div>
	</div>
</div>