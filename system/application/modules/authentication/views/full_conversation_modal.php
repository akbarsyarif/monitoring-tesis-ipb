<div class="modal fade" id="full_conversation_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<?= form_open('message/send', array('class' => 'full_conversation_form', 'data-parsley-validate' => true)); ?>
				<?= form_hidden('to', '') ?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><i class="fa fa-fw fa-bullhorn"></i> Percakapan</h4>
				</div>
				<div class="modal-body">
					<ul class="messages" style="overflow: auto; height: 500px"></ul>
					<div class="form-group">
						<?= form_textarea(array(
							'name' => 'message',
							'value' => '',
							'placeholder' => 'Tulis pesan balasan...',
							'rows' => 5,
							'class' => 'form-control',
							'required' => true
						)); ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-round btn-default" data-dismiss="modal"><i class="fa fa-fw fa-times-circle"></i> Batal</button>
					<button type="submit" class="btn btn-sm btn-round btn-dark"><i class="fa fa-fw fa-check-circle"></i> Kirim</button>
				</div>
			<?= form_close(); ?>
		</div>
	</div>
</div>