<div class="modal fade" id="change_password_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<?= form_open('authentication/profile/update_password', 'data-parsley-validate=""') ?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Ganti Kata Sandi</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<?= form_label('Kata Sandi Lama *') ?>
						<?= form_password('old_password', '', 'class="form-control" required=""') ?>
					</div>
					<div class="form-group">
						<?= form_label('Kata Sandi Baru *'); ?>
						<?= form_password('new_password', '', 'class="form-control" id="password" required="true" minlength="5"	data-parsley-minlength="5"') ?>
					</div>
					<div class="form-group">
						<?= form_label('Ulangi Kata Sandi *'); ?>
						<?= form_password('confirm_password', '', 'class="form-control" id="confirm_password" required="true" data-parsley-equalto="#password" minlength="5" data-parsley-minlength="5"') ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-round btn-default" data-dismiss="modal"><i class="fa fa-fw fa-times-circle"></i> Batal</button>
					<button type="submit" class="btn btn-sm btn-round btn-dark"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				</div>
			<?= form_close() ?>
		</div>
	</div>
</div>