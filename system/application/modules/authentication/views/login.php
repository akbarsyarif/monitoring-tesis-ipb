<div id="login" class="animate form">
	<section class="login_content">
		<?= form_open('', 'id="login-form"'); ?>
			<h1>Login Form</h1>
			
			<div>
				<?= form_input('username', '', 'class="form-control" placeholder="Username"'); ?>
			</div>
			<div>
				<?= form_password('password', '', 'class="form-control" placeholder="Password"') ?>
			</div>

			<?= get_flash_alert() ?>

			<div>
				<button class="btn btn-default submit" type="submit">Log in</button>
				<a class="reset_pass" href="#">Lost your password?</a>
			</div>
			<div class="clearfix"></div>
			<div class="separator">
				<div>
					<p>©2015 <?= get_option('app_title') ?> All Rights Reserved.</p>
				</div>
			</div>
		<?= form_close() ?>
	</section>
</div>