<div class="x_panel">
	<div class="x_content">

		<div class="col-md-3 col-sm-3 col-xs-12 profile_left">

			<div class="profile_img">

				<!-- Current avatar -->
				<div class="avatar-view" title="Avatar">
					<a href="#change_avatar_modal" data-toggle="modal"><img src="<?= $this->sp_auth->userData()->profile_picture ?>" alt="Avatar" class="current_user_avatar"></a>
				</div>

			</div>
			<h3><?= $this->sp_auth->userData()->full_name ?></h3>

			<ul class="list-unstyled user_data">
				<li>
					<i class="fa fa-briefcase user-profile-icon"></i> <?= $this->sp_auth->userData()->group_alias ?>
				</li>

				<li class="m-top-xs">
					<i class="fa fa-envelope-o"></i>
					<a href="#"><?= $this->sp_auth->userData()->email ?></a>
				</li>
			</ul>

			<a class="btn btn-sm btn-round btn-success" href="#edit_profile_modal" data-toggle="modal"><i class="fa fa-edit m-right-xs"></i> Edit Profile</a>
			<a class="btn btn-sm btn-round btn-default" href="#change_password_modal" data-toggle="modal"><i class="fa fa-key m-right-xs"></i> Ganti Kata Sandi</a>
			<br />

		</div>
		<div class="col-md-9 col-sm-9 col-xs-12">
			<div class="" role="tabpanel" data-example-id="togglable-tabs">
				<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
					<li role="presentation" class="active"><a href="#tab_messages" id="message-tab" role="tab" data-toggle="tab" aria-expanded="true">Pesan Masuk</a></li>
					<li role="presentation"><a href="#tab_sent" id="sent-tab" role="tab" data-toggle="tab">Pesan Terkirim</a></li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<p>
						<a href="#compose_message_modal" data-toggle="modal" class="btn btn-sm btn-info btn-round"><i class="fa fa-fw fa-pencil"></i> Tulis Pesan</a>
					</p>
					<div role="tabpanel" class="tab-pane fade active in" id="tab_messages" aria-labelledby="tab_messages">
						<!-- start recent activity -->
						<ul class="messages" id="inbox_list"></ul>
						<!-- end recent activity -->
					</div>
					<div role="tabpanel" class="tab-pane fade in" id="tab_sent" aria-labelledby="tab_messages">
						<!-- sent items -->
						<ul class="messages" id="sent_list"></ul>
						<!-- end sent items -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require 'edit_profile_modal.php' ?>
<?php require 'change_avatar_modal.php' ?>
<?php require 'change_password_modal.php' ?>
<?php require 'compose_message_modal.php' ?>
<?php require 'full_conversation_modal.php' ?>

<!-- javascript -->
<script type="text/javascript">
	$change_avatar_modal = $('#change_avatar_modal');
	$edit_profile_modal = $('#edit_profile_modal');
	$change_password_modal = $('#change_password_modal');
	$compose_message_modal = $('#compose_message_modal');
	$full_conversation_modal = $('#full_conversation_modal');

	$(document).ready(function() {
		// change avatar form submit
		$change_avatar_modal.find('form').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				if ( $form.parsley().isValid() ) {
					return true;
				}

				return false;
			},
			success: function(response) {
				if ( response.result == false ) {
					console.log(response);
					alertify.error(response.message);
				} else {
					var new_profile_picture = response.new_profile_picture;
					$change_avatar_modal.modal('hide');
					$('.current_user_avatar').attr('src', new_profile_picture);
					alertify.success('Pengaturan berhasil disimpan');
				}
			}
		})

		// edit profile form submit
		$edit_profile_modal.find('form').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				if ( $form.parsley().isValid() ) {
					return true;
				}

				return false;
			},
			success: function(response) {
				if ( response.result == false ) {
					console.log(response);
					alertify.error(response.message);
				} else {
					$edit_profile_modal.modal('hide');
					alertify.success('Pengaturan berhasil disimpan');
				}
			},
			error : function(error) {
				console.log(error);
			}
		})

		// change password form submit
		$change_password_modal.find('form').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				if ( $form.parsley().isValid() ) {
					return true;
				}

				return false;
			},
			success: function(response) {
				if ( response.result == false ) {
					console.log(response);
					alertify.error(response.message);
				} else {
					$change_password_modal.modal('hide');
					alertify.success('Pengaturan berhasil disimpan');
				}
			},
			error : function(error) {
				console.log(error);
			}
		})

		// ******************************* messages *************************************** //

		// show all inbox
		$.ajax({
			url : '<?= site_url("message/get_all_inbox") ?>',
			dataType : 'json',
			data : { limit : 20, offset : 0 },
			success : function(response) {
				if ( response.result == false ) {
					console.log(response);
				}
				else {
					var messages = response.data;
					var html = '';

					for ( i=0; i<=messages.length - 1; i++ ) {
						var message = messages[i];

						html += '<li>';
							html += '<img src="'+ message.avatar +'" class="current_user_avatar avatar" alt="Avatar">';
							html += '<div class="message_date">';
								html += '<h3 class="date text-info">'+ message.d +'</h3>';
								html += '<p class="month">'+ message.M +'</p>';
							html += '</div>';
							html += '<div class="message_wrapper">';
								html += '<h4 class="heading">'+ message.full_name +'</h4>';
								html += '<blockquote class="message">'+ message.message +'</blockquote>';
								html += '<br />';
								html += '<p class="url">';
									html += '<span class="fs1 text-info" aria-hidden="true"><i class="fa fa-fw fa-clock-o"></i> '+ message.time +'</span>';
									html += '<span><a href="javascript:void(0)" data-to="'+ message.from +'" class="reply btn btn-xs btn-default btn-round"><i class="fa fa-fw fa-reply"></i> Quick Reply</a></span>';
									html += '<span><a href="javascript:void(0)" data-from="'+ message.from +'" class="full_conversation btn btn-xs btn-default btn-round"><i class="fa fa-fw fa-eye"></i> See Full Conversation</a></span>';
								html += '</p>';
							html += '</div>';
						html += '</li>';
					}

					$('#inbox_list').html(html);
				}
			},
			error : function(error) {
				alertify.error('Error occured. Tidak dapat menghubungi server.');
			},
			complete : function() {

			}
		})

		// show all sent items
		$.ajax({
			url : '<?= site_url("message/get_all_sent_items") ?>',
			dataType : 'json',
			data : { limit : 20, offset : 0 },
			success : function(response) {
				if ( response == false ) {
					console.log(response);
					alertify.log(response.message);
				}
				else {
					var messages = response.data;
					for ( i=0; i<=messages.length - 1; i++ ) {
						var message = messages[i];
						var html = '';

						for ( i=0; i<=messages.length - 1; i++ ) {
							var message = messages[i];

							html += '<li>';
								html += '<img src="'+ message.avatar +'" class="current_user_avatar avatar" alt="Avatar">';
								html += '<div class="message_date">';
									html += '<h3 class="date text-info">'+ message.d +'</h3>';
									html += '<p class="month">'+ message.M +'</p>';
								html += '</div>';
								html += '<div class="message_wrapper">';
									html += '<h4 class="heading">'+ message.full_name +'</h4>';
									html += '<blockquote class="message">'+ message.message +'</blockquote>';
									html += '<br />';
									html += '<p class="url">';
										html += '<span class="fs1 text-info" aria-hidden="true"><i class="fa fa-fw fa-clock-o"></i> '+ message.time +'</span>';
										html += '<span><a href="javascript:void(0)" data-from="'+ message.to +'" class="full_conversation btn btn-xs btn-default btn-round"><i class="fa fa-fw fa-eye"></i> See Full Conversation</a></span>';
									html += '</p>';
								html += '</div>';
							html += '</li>';
						}

						$('#sent_list').html(html);
					}
				}
			},
			error : function(error) {
				alertify.error('Error occured. Tidak dapat menghubungi server.')
			},
			complete : function() {

			}
		})

		// compose message submit
		$compose_message_modal.find('.compose_message_form').ajaxForm({
			dataType: 'json',
			beforeSubmit : function(arr, $form, options) {
				show_loading();

				if ( $form.parsley().isValid() ) {
					return true;
				}

				return false;
			},
			success : function(response) {
				if ( response.result == false ) {
					console.log(response);
					alertify.error(response.message);
				}
				else {
					$compose_message_modal.modal('hide');
					alertify.success('Pesan anda telah dikirim.', '', 10000);
				}
			},
			error : function(error) {
				alertify.error('Error occured. Tidak dapat menghubungi server.');
			},
			complete : function() {
				hide_loading();
			}
		})

		// reply message
		$('#inbox_list').on('click', '.reply', function(e) {
			$form = $compose_message_modal.find('.compose_message_form');

			var to = $(this).data('to');

			$compose_message_modal.find('[name="to"]').val(to);

			reinitializeSelect2Single($form.find('.select2-single'));

			$compose_message_modal.modal('show');
		})

		// when compose message modal hide
		$compose_message_modal.on('hide.bs.modal', function() {
			resetForm($compose_message_modal.find('.compose_message_form'));
		})

		// full conversation modal
		$('#inbox_list, #sent_list').on('click', '.full_conversation', function(e) {
			var user2_id = $(this).data('from');
			$full_conversation_modal.find('.full_conversation_form [name="to"]').val(user2_id);

			reloadFullConversation();
		})

		// when full conversation modal hide
		$full_conversation_modal.on('hide.bs.modal', function() {
			$(this).find('.full_conversation_form [name="message"]').clearFields();
		})

		// full conversation form submit
		$full_conversation_modal.find('.full_conversation_form').ajaxForm({
			dataType : 'json',
			beforeSubmit : function(arr, $form, options) {
				show_loading();

				if ( $form.parsley().isValid() ) {
					return true;
				}

				return false;
			},
			success : function(response) {
				if ( response.result == false ) {
					console.log(response);
					alertify.error(response.message);
				}
				else {
					$full_conversation_modal.find('.full_conversation_form [name="message"]').clearFields();
					reloadFullConversation();
				}
			},
			error : function() {
				alertify.error('Error occured. Tidak dapat menghubungi server.');
			},
			complete : function() {
				hide_loading();
			}
		})
	})

	// reset form
	function resetForm($form) {
		$form.resetForm();

		reinitializeSelect2Single($form.find('.select2-single'));
	}

	// reload full conversation
	function reloadFullConversation() {
		var user2_id = $full_conversation_modal.find('.full_conversation_form [name="to"]').val();

		$.ajax({
			url : '<?= site_url("message/get_conversation") ?>',
			dataType : 'json',
			data : { limit : 100, offset : 0, user2_id : user2_id },
			success : function(response) {
				if ( response.result == false ) {
					console.log(response);
					alertify.log(response.message);
				}
				else {
					var messages = response.data;
					var html = '';

					for ( i=0; i<=messages.length - 1; i++ ) {
						var message = messages[i];

						html += '<li>';
							html += '<img src="'+ message.avatar +'" class="current_user_avatar avatar" alt="Avatar">';
							html += '<div class="message_date">';
								html += '<h3 class="date text-info">'+ message.d +'</h3>';
								html += '<p class="month">'+ message.M +'</p>';
							html += '</div>';
							html += '<div class="message_wrapper">';
								html += '<h4 class="heading">'+ message.full_name +'</h4>';
								html += '<blockquote class="message">'+ message.message +'</blockquote>';
								html += '<br />';
								html += '<p class="url">';
									html += '<span class="fs1 text-info" aria-hidden="true"><i class="fa fa-fw fa-clock-o"></i> '+ message.time +'</span>';
								html += '</p>';
							html += '</div>';
						html += '</li>';
					}

					$full_conversation_modal.find('.messages').html(html);

					$full_conversation_modal.modal('show');
				}
			},
			error : function() {
				alertify.error('Error occured. Tidak dapat menghubungi server.');
			},
			complete : function() {

			}
		})
	}

</script>