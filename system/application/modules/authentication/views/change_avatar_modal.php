<div class="modal fade" id="change_avatar_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<?= form_open('authentication/profile/change_avatar', 'data-parsley-validate=""') ?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Ganti Foto</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<?= form_upload('avatar', '', 'required=""'); ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-round btn-default" data-dismiss="modal"><i class="fa fa-fw fa-times-circle"></i> Batal</button>
					<button type="submit" class="btn btn-sm btn-round btn-dark"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				</div>
			<?= form_close() ?>
		</div>
	</div>
</div>