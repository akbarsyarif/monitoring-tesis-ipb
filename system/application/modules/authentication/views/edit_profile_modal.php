<div class="modal fade" id="edit_profile_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<?= form_open('authentication/profile/update_profile', 'data-parsley-validate=""') ?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Profile</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<?= form_label('Nama Depan *') ?>
						<?= form_input('first_name', $this->sp_auth->userData()->first_name, 'class="form-control" required=""') ?>
					</div>
					<div class="form-group">
						<?= form_label('Nama Belakang') ?>
						<?= form_input('last_name', $this->sp_auth->userData()->last_name, 'class="form-control"') ?>
					</div>
					<div class="form-group">
						<?= form_label('Email *') ?>
						<?= form_input('email', $this->sp_auth->userData()->email, 'class="form-control" required=""') ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-round btn-default" data-dismiss="modal"><i class="fa fa-fw fa-times-circle"></i> Batal</button>
					<button type="submit" class="btn btn-sm btn-round btn-dark"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				</div>
			<?= form_close() ?>
		</div>
	</div>
</div>