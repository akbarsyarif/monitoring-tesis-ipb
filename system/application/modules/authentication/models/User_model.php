<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model {

	public $table       = 'users';
	public $primary_key = 'id';

	public $error_message = array();

	public function __construct()
	{

		$this->has_many_pivot['groups'] = array(
			'foreign_model' => 'group_model',
			'pivot_table' => 'user_groups',
			'local_key' => 'id',
			'pivot_local_key' => 'user_id',
			'foreign_key' => 'id',
			'pivot_foreign_key' => 'group_id',
			'get_relate' => true
		);

		parent::__construct();
	}


	/**
	 * authentication
	 * @param string [username]
	 * @param password [password hash result]
	 * @return boolean
	 */
	public function auth($username, $password)
	{

		$this->db
		     ->select('users.id as user_id, users.first_name, users.last_name, users.profile_picture, users.user_status')
		     ->select('groups.id as group_id, groups.group_name')
		     ->where('users.username', $username)
		     ->where('users.password', $password)
		     ->join('user_groups', 'user_groups.user_id = users.id')
		     ->join('groups', 'groups.id = user_groups.group_id');

		$result = $this->db->get('users');

		if ( $result->num_rows() > 0 ) {

			$data = $result->row();

			// check user status
			switch ( $data->user_status ) {

				case 'active':
					
					return true;

					break;

				case 'inactive':

					array_push($this->error_message, 'Akun anda belum aktif.');

					break;
				
				case 'banned':
					
					array_push($this->error_message, 'Akun anda telah diblokir.');

					break;

			}

		} else {

			array_push($this->error_message, 'Invalid username or password.');

		}

		return false;

	}

}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */