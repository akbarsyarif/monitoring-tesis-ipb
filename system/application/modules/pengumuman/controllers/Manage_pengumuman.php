<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_pengumuman extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		// load dependencies
		$this->load->model('pengumuman_model');
		$this->load->model('administrator/user_model');
	}

	public function index()
	{

		$this->data['page_title'] = 'Pengumuman';
		
		$this->template->build('manage_pengumuman/index', $this->data);

		// authorization
		if ( ! $this->sp_auth->isInThisGroup('administrator') ) {

			show_error('Access forbiden.');

		}

	}


	/**
	 * retrieve data as a datatable's format
	 */
	public function dataTable()
	{

		// variable that contain condition
		$query = null;

		// columns index
		$column_index = array(null, 'pengumuman.created_at', 'pengumuman.pengumuman', 'first_name', null);

		// filter column handler / search
		$columns = $_REQUEST['columns'];

		if ( ! empty($_REQUEST['search']['value']) ) {

			$search_value = xss_clean($_REQUEST['search']['value']);

			// global search
			$query['where']['pengumuman like'] = "%$search_value%";

		} else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					$query['where'][$column_index[$column_key].' like'] = '%'.$column_val['search']['value'].'%';

				}

			}

		}

		$records_total    = count($this->pengumuman_model->get_all($query));
		$records_filtered = $records_total;

		// orders
		foreach ( $_REQUEST['order'] as $order ) {

			$order_column = $order['column'];
			$order_dir    = $order['dir'];

			$query['order_by'][$column_index[$order_column]] = $order_dir;

		}

		// limit
		$query['limit_offset'] = array($_REQUEST['length'], $_REQUEST['start']);

		$data = $this->pengumuman_model->get_all($query);

		$response = (object) array (
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_filtered,
			'data' => $data
		);

		echo json_encode($response);

	}


	/**
	 * get data by id
	 */
	public function get_by_id()
	{

		$id = $this->input->get('id', TRUE);

		$query['limit'] = 1;
		$query['where']['pengumuman.id'] = $id;

		$data = $this->pengumuman_model->get_all($query);

		if ( ! $data ) {

			$response = array(
				'result' => false,
				'message' => 'Data not found.'
			);

		} else {

			$response = array(
				'result' => true,
				'data' => $data[0]
			);

		}

		echo json_encode( (object) $response );

	}


	/**
	 * save data
	 */
	public function save()
	{

		$rules = array(
			array(
				'field' => 'type',
				'label' => 'Status',
				'rules' => 'required|trim|xss_clean'
			),
			array(
				'field' => 'pengumuman',
				'label' => 'Pengumuman',
				'rules' => 'required|trim|xss_clean'
			),
		);

		$this->form_validation->set_rules($rules);

		if ( ! $this->form_validation->run() ) {

			// validation fail
			$response = array(
				'result' => false,
				'message' => validation_errors()
			);

		} else {

			$data = array(
				'type' => $this->input->post('type', TRUE),
				'pengumuman' => nl2br($this->input->post('pengumuman', TRUE)),
				'user_id' => $this->session->userdata('user_id'),
			);

			// insert data
			if ( $this->pengumuman_model->insert($data) ) {

				$response = array(
					'result' => true
				);

			} else {

				$response = array(
					'result' => false,
					'message' => 'Error occured. Fail to save data.'
				);

			}

		}

		echo json_encode( (object) $response );

	}


	/**
	 * update data
	 * via ajax request
	 */
	public function update()
	{

		$rules = array(
			array(
				'field' => 'type',
				'label' => 'Status',
				'rules' => 'required|trim|xss_clean'
			),
			array(
				'field' => 'pengumuman',
				'label' => 'Pengumuman',
				'rules' => 'required|trim|xss_clean'
			),
		);

		$this->form_validation->set_rules($rules);

		if ( ! $this->form_validation->run() ) {

			// validation fail
			$response = array(
				'result' => false,
				'message' => validation_errors()
			);

		} else {

			$id = $this->input->post('id', TRUE);

			$data = array(
				'type' => $this->input->post('type', TRUE),
				'pengumuman' => nl2br($this->input->post('pengumuman', TRUE)),
			);

			// insert data
			if ( $this->pengumuman_model->update($data, $id) ) {

				$response = array(
					'result' => true
				);

			} else {

				$response = array(
					'result' => false,
					'message' => 'Something wrong. Fail to update data.'
				);

			}

		}

		echo json_encode( (object) $response );

	}


	/**
	 * remove data by id
	 */
	public function remove()
	{

		$id = $this->input->get('id', TRUE);

		if ( ! $this->pengumuman_model->delete($id) ) {

			$response = array(
				'result' => false,
				'message' => 'Something wrong. Fail to remove data.'
			);

		} else {

			$response = array(
				'result' => true
			);

		}

		echo json_encode( (object) $response );

	}

}

/* End of file manage_pengumuman.php */
/* Location: ./application/controllers/manage_pengumuman.php */