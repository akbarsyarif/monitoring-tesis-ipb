<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="pull-left">
					<button class="btn btn-round btn-sm btn-dark" data-toggle="modal" href='#data-add-modal'><i class="fa fa-fw fa-plus-circle"></i> Tambah</button>
				</div>
				<div class="pull-right">
					<!-- put something here -->
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-hover" id="data-list" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th width="10"><?= form_checkbox('ids', ''); ?></th>
							<th>Tanggal</th>
							<th>Pengumuman</th>
							<th>User</th>
							<th width="100"></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<?php require 'data_add_modal.php' ?>
<?php require 'data_edit_modal.php' ?>

<!-- javascript -->
<script type="text/javascript">
	$(document).ready(function() {
		/**
		 * data list / datatable handler
		 */
		var table = $('#data-list').DataTable({
			ordering : true,
			order : [[ 1, "desc" ]],
			stateSave : true,
			processing : true,
			serverSide : true,
			ajax : '<?= site_url("pengumuman/manage_pengumuman/dataTable") ?>',
			columns : [
				{
					"orderable": false,
					"data": null,
					"defaultContent": "<input type='checkbox' name='id[]' value='' />"
				},
				{ "data": "tanggal_pengumuman_indonesia" },
				{ "data": "pengumuman_excerpt" },
				{ "data": "full_name" },
				{
					"orderable": false,
					"data": null,
					"defaultContent": "<a href='javascript:void(0)' class='data-edit-modal'>edit</a> | <a href='javascript:void(0)' class='data-remove-modal'>hapus</a>"
				},
			],
		});


		/**
		 * data add modal handler
		 */
		$('#data-add-form').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				if ( $form.parsley().isValid() ) {
					return true;
				}

				return false;
			},
			success: function(response) {
				if ( response.result == false ) {
					console.log(response);
					alert(response.message);
				} else {
					$('#data-add-form').resetForm();
					$('#data-add-modal').modal('hide');

					table.ajax.reload();
				}
			}
		});


		/**
		 * data edit modal handler
		 */
		$('#data-list tbody').on('click', '.data-edit-modal', function() {
			$row = table.row( $(this).parents('tr') );
			var data = table.row( $(this).parents('tr') ).data();

			$modal = $('#data-edit-modal');

			var id = data.id;

			// get data
			$.ajax({
				url : '<?= site_url("pengumuman/manage_pengumuman/get_by_id") ?>',
				dataType : 'json',
				data : { id : id },
				success : function(response) {
					if ( response.result !== true ) {
						console.log(response);
						alert(response.message);
					} else {
						$form = $modal.find('form');

						// show modal
						$modal.modal('show');

						var data = response.data;
						
						$modal.find('[name="id"]').val(data.id);
						$modal.find('[name="type"]').val(data.type);
						$modal.find('[name="pengumuman"]').val(data.pengumuman);

						reinitializeSelect2Single($form.find('.select2-single'));
					}
				}
			});
		});


		/**
		 * data edit form handler
		 */
		$('#data-edit-form').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				if ( $form.parsley().isValid() ) {
					return true;
				}

				return false;
			},
			success: function(response) {
				if ( response.result == false ) {
					console.log(response);
					alert(response.message);
				} else {
					$('#data-edit-form').resetForm();
					$('#data-edit-modal').modal('hide');

					// $row.data(newData).draw(false);
					table.ajax.reload(null,false);
				}
			}
		});


		/**
		 * data remove handler
		 */
		$('#data-list tbody').on('click', '.data-remove-modal', function() {
			var data = table.row( $(this).parents('tr') ).data();
			var id = data.id;

			// set alertify label
			alertify.set({
				labels : {
					ok : '<i class="fa fa-fw fa-check-circle"></i> Ya',
					cancel : '<i class="fa fa-fw fa-times-circle"></i> Tidak'
				}
			});
			alertify.confirm('Apakah anda yakin akan menghapus data?', function(e) {
				if ( e ) {
					// remove data
					$.ajax({
						url : '<?= site_url("pengumuman/manage_pengumuman/remove") ?>',
						dataType : 'json',
						data : { id : id },
						success : function(response) {
							if ( response.result !== true ) {
								console.log(response);
								alert(response.message);
							} else {
								table.row( $(this).parents('tr') ).remove().draw(false);
							}
						}
					});
				}
				else {
					return;
				}
			});
		});
	});
</script>