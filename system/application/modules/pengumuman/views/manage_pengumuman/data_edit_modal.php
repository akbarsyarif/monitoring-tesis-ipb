<div class="modal fade" id="data-edit-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<?= form_open('pengumuman/manage_pengumuman/update', 'id="data-edit-form" data-parsley-validate=""'); ?>
				<?= form_hidden('id', ''); ?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><?= $page_title ?> <small>( Edit )</small></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<?= form_label('Status *'); ?>
						<?= form_dropdown('type', array(
							'penting' => 'Penting',
							'himbauan' => 'Himbauan',
							'peringatan' => 'Peringatan',
						), '', 'class="form-fropdown select2-single" required=""'); ?>
					</div>
					<div class="form-group">
						<?= form_label('Pengumuman *'); ?>
						<?= form_textarea(array(
							'name' => 'pengumuman',
							'value' => '',
							'class' => 'form-control',
							'rows' => 10,
							'required' => true
						)); ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-round btn-default" data-dismiss="modal"><i class="fa fa-fw fa-times-circle"></i> Batal</button>
					<button type="submit" class="btn btn-sm btn-round btn-dark"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				</div>
			<?= form_close(); ?>
		</div>
	</div>
</div>