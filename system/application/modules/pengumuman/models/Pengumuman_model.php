<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengumuman_model extends MY_Model {

	public $table = 'pengumuman';
	public $primary_key = 'id';

	public function __construct()
	{
		$this->has_one['users'] = array(
			'foreign_model' => 'user_model',
			'foreign_table' => 'users',
			'foreign_key' => 'id',
			'local_key' => 'user_id'
		);

		parent::__construct();
	}


	public function get_all($query=array())
	{

		if ( isset($query['where']) ) $this->db->where($query['where']);
		if ( isset($query['limit']) ) $this->db->limit($query['limit']);
		if ( isset($query['limit_offset']) ) $this->db->limit($query['limit_offset'][0], $query['limit_offset'][1]);

		if ( isset($query['order_by']) ) {

			if ( is_array($query['order_by']) ) {

				foreach ( $query['order_by'] as $column => $dir ) {

					$this->db->order_by($column, $dir);

				}

			} else {

				$this->db->order_by($query['order_by']);

			}

		}

		$this->db 
		     ->select('pengumuman.id, pengumuman.pengumuman, pengumuman.type, pengumuman.user_id, pengumuman.created_at as tanggal_pengumuman')
		     ->select('users.first_name, users.last_name, CONCAT(users.first_name, " ", users.last_name) AS full_name, users.email')
		     ->from('pengumuman')
		     ->join('users', 'users.id = pengumuman.user_id', 'left')
		     ->order_by('pengumuman.created_at desc');

		$result = $this->db->get();

		if ( $result->num_rows() > 0 ) {

			foreach ( $result->result() as $pengumuman ) {

				$pengumuman_excerpt = strip_tags($pengumuman->pengumuman);
				$pengumuman_excerpt = word_limiter($pengumuman_excerpt, 10);

				$data[] = (object) array(
					'id' => $pengumuman->id,
					'tanggal_pengumuman' => $pengumuman->tanggal_pengumuman,
					'tanggal_pengumuman_indonesia' => tgl_indonesia($pengumuman->tanggal_pengumuman),
					'type' => $pengumuman->type,
					'user_id' => $pengumuman->user_id,
					'pengumuman' => $pengumuman->pengumuman,
					'pengumuman_excerpt' => $pengumuman_excerpt,
					'first_name' => $pengumuman->first_name,
					'last_name' => $pengumuman->last_name,
					'full_name' => $pengumuman->full_name,
					'email' => $pengumuman->email
				);

			}

			return $data;

		}

		return false;

	}

}

/* End of file pengumuman_model.php */
/* Location: ./application/models/pengumuman_model.php */