<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * check if there's alert flashdata set
 * if there is then show alert
 * @return string/null
 */
if ( ! function_exists('get_flash_alert') ) {

	function get_flash_alert() {

		$ci =& get_instance();

		$alert = $ci->session->flashdata('flash_alert');

		if ( $alert !== false ) {

			return $alert;

		}

		return null;

	}

}


/**
 * set flash alert
 * @param string [alert message]
 * @param string [
 *   optional parameter for alert type. default value is 'default for alert-default'
 *   'success for alert-success',
 *   'info for alert-info',
 *   'warning for alert-warning',
 *   'danger for alert-danger'
 * ]
 * @return void
 */
if ( ! function_exists('set_flash_alert') ) {

	function set_flash_alert($message=null, $type='default') {

		$ci =& get_instance();

		if ( (is_null($message)) OR (empty($message)) ) {

			show_error('Message can not be empty.');

		}

		$html = '';

		switch ($type) {
			case 'success':
				$type = 'alert-success';
				break;

			case 'info':
				$type = 'alert-info';
				break;

			case 'warning':
				$type = 'alert-warning';
				break;

			case 'danger':
				$type = 'alert-danger';
				break;
			
			default:
				$type = 'alert-default';
				break;
		}

		$html .= '<div class="alert '.$type.'">';
		$html .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
		$html .= $message;
		$html .= '</div>';

		// set flashdata session
		$ci->session->set_flashdata('flash_alert', $html);

	}

}