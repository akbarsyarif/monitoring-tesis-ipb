<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Display loading screen DOM
 * @param string [id]
 * @return void
 */
if ( ! function_exists('display_loading') ) {

	function display_loading($id='') {

		$html  = '<div class="sp-loading" id="'.$id.'">';
			$html .= '<div><i class="fa fa-cog fa-spin"></i> Loading...</div>';
		$html  .= '</div>';

		echo $html;

	}
	
}