<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * get app options
 * @param string [option_name]
 * @return mix [return option_value if option exists and 
 * will return default message if option doesn't exists]
 */
if ( ! function_exists('get_option') ) {

	function get_option($option_name=null) {

		$CI =& get_instance();

		$default = 'Option name not found.';

		if ( ! is_null($option_name) ) {

			$CI->db->where('option_name', $option_name);

			$result = $CI->db->get('options');

			if ( count($result) > 0 ) {

				$option = $result->row();

				return $option->option_value;

			}

		}

		return $default;

	}

}


/**
 * get theme path url
 * @return string
 */
if ( ! function_exists('theme_url') ) {

	function theme_url($string='') {

		$CI =& get_instance();

		$CI->load->config('template');

		$theme_dirs = $CI->config->item('theme_locations');
		$theme_name = $CI->config->item('theme');

		foreach ( $theme_dirs as $theme_dir ) {

			$theme_path = $theme_dir.$theme_name;

			// check theme directory
			// if theme exists
			if ( is_dir($theme_path) ) {

				if ( ! empty($string) ) {

					$theme_path = $theme_path.'/'.$string;

				}

				return base_url($theme_path);

			}

		}

		show_error('Theme not found.');

	}

}


/**
 * get blog theme path url
 * @return string
 */
if ( ! function_exists('blog_theme_url') ) {

	function blog_theme_url($string='') {

		$CI =& get_instance();

		$CI->load->config('template');

		$theme_dirs = $CI->config->item('theme_locations');
		$theme_name = $CI->config->item('blog_theme');

		foreach ( $theme_dirs as $theme_dir ) {

			$theme_path = $theme_dir.$theme_name;

			// check theme directory
			// if theme exists
			if ( is_dir($theme_path) ) {

				if ( ! empty($string) ) {

					$theme_path = $theme_path.'/'.$string;

				}

				return base_url($theme_path);

			}

		}

		show_error('Theme not found.');

	}

}