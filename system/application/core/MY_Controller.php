<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

	protected $data = array();

	public function __construct()
	{

		parent::__construct();

	}

}


/**
 * core controllers for content management
 */
class SP_Admin_Controller extends MX_Controller {

	protected $data = array();

	public function __construct()
	{

		parent::__construct();

		// authorization
		// check access
		if ( ($this->uri->segment(1) !== 'login') AND ($this->uri->segment(2) !== 'login') ) {

			if ( ! $this->sp_auth->isLoggedIn() ) {

				redirect('login', 'refresh');

			}

		}

		// load dependencies
		$this->load->helper(array('form'));
		$this->load->library(array('form_validation'));

		// set default layout
		$this->template->set_layout('default');

		// default page title
		$this->data['page_title'] = get_option('app_title');
		$this->data['page_sub_title'] = get_option('app_description');

	}

}


/**
 * Core controller for restfull service
 */
class SP_Rest_Controller extends MX_Controller {

	protected $data = array();

	public function __construct()
	{

		parent::__construct();

		// check if request is not an XHR
		if ( ! $this->input->is_ajax_request() ) {

			show_404();

		}
		
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/controllers/MY_Controller.php */