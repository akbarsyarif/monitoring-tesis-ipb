<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'third_party/mpdf/mpdf.php';

class M_pdf
{

	protected $mode = '';
	protected $format = 'A4';
	protected $default_font_size = 0;
	protected $default_font = '';
	protected $mgl = 20;
	protected $mgr = 15;
	protected $mgt = 15;
	protected $mgb = 15;
	protected $mgh = 9;
	protected $mgf = 9;
	protected $orientation = 'P';


	/**
	 * Mpdf initialize
	 */
	public function initialize($config = array())
	{
		if ( isset($config['mode']) ) $this->mode = $config['mode'];
		if ( isset($config['format']) ) $this->format = $config['format'];
		if ( isset($config['default_font_size']) ) $this->default_font_size = $config['default_font_size'];
		if ( isset($config['default_font']) ) $this->default_font = $config['default_font'];
		if ( isset($config['mgl']) ) $this->mgl = $config['mgl'];
		if ( isset($config['mgr']) ) $this->mgr = $config['mgr'];
		if ( isset($config['mgt']) ) $this->mgt = $config['mgt'];
		if ( isset($config['mgb']) ) $this->mgb = $config['mgb'];
		if ( isset($config['mgh']) ) $this->mgh = $config['mgh'];
		if ( isset($config['mgf']) ) $this->mgf = $config['mgf'];
		if ( isset($config['mgf']) ) $this->mgf = $config['mgf'];
		if ( isset($config['orientation']) ) $this->orientation = $config['orientation'];

		return new mPDF($this->mode,
			            $this->format,
			            $this->default_font_size,
			            $this->default_font,
			            $this->mgl,
			            $this->mgr,
			            $this->mgt,
			            $this->mgb,
			            $this->mgh,
			            $this->mgf,
			            $this->orientation);
	}

}

/* End of file Mpdf.php */
/* Location: ./application/libraries/Mpdf.php */
