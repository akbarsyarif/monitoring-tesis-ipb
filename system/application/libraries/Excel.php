<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'third_party/phpexcel/Classes/PHPExcel.php';

class Excel extends PHPExcel
{
	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file Excel.php */
/* Location: ./application/libraries/Excel.php */
