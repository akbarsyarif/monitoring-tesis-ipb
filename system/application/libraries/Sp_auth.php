<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SP_Auth
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}


	/**
	 * hash password
	 * @param string [password]
	 * @return string [hash result]
	 */
	public function passHash($password)
	{

		$result = md5($password);

		return $result;

	}


	/**
	 * Check user login status
	 * is logged in?
	 * @return boolean
	 */
	public function isLoggedIn()
	{

		return $this->ci->session->userdata('logged_in');

	}


	/**
	 * get user groups
	 * @param integer [user id]
	 * @return array
	 */
	public function getUserGroups($uid=false)
	{

		// if uid param is not set
		// use current logged in user id
		if ( ! $uid ) {

			if ( $this->isLoggedIn() ) {

				$uid = $this->ci->session->userdata('user_id');

			}

		}

		$this->ci->db
		         ->select('user_groups.id, user_groups.user_id, user_groups.group_id')
		         ->select('groups.group_name')
		         ->from('user_groups')
		         ->where('user_groups.user_id', $uid)
		         ->join('users', 'users.id = user_groups.user_id')
		         ->join('groups', 'groups.id = user_groups.group_id');

		$result = $this->ci->db->get();

		$groups = array();

		if ( $result->num_rows() > 0 ) {

			foreach ( $result->result() as $group ) {

				array_push($groups, $group->group_name);

			}

		}

		return $groups;

	}


	/**
	 * Check is the user is in the current group
	 * @param string [group_name]
	 * @return boolen [return true if user is in this group
	 * or return false if it's not]
	 */
	public function isInThisGroup($group_name, $uid=false)
	{

		// if uid param is not set
		// use current logged in user id
		if ( ! $uid ) {

			if ( $this->isLoggedIn() ) {

				$uid = $this->ci->session->userdata('user_id');

			}

		}

		if ( $uid !== false ) {

			$groups = $this->getUserGroups($uid);

			if ( count($groups) > 0 ) {

				if ( in_array($group_name, $groups) ) {

					return true;

				}

			}

		}

		return false;

	}


	/**
	 * Check if user is member of one of these groups
	 * @param array [groups name]
	 * @param integer [user id]
	 * @return boolean [return true if user is a member of
	 * one of this groups]
	 */
	public function isInOneOfThisGroups($group_names=array(), $uid=false)
	{

		if ( count($group_names) > 0 ) {

			// if uid param is not set
			// use current logged in user id
			if ( ! $uid ) {

				if ( $this->isLoggedIn() ) {

					$uid = $this->ci->session->userdata('user_id');

				}

			}

			if ( $uid !== false ) {

				$user_groups = $this->getUserGroups($uid);

				if ( count($user_groups) > 0 ) {

					foreach ( $group_names as $group_name ) {

						if ( in_array($group_name, $user_groups) ) {

							return true;

						}

					}

				}

			}

		}

		return false;

	}


	/**
	 * is token valid
	 * check token validation
	 * @param integer [user id]
	 * @param string [token]
	 * @return boolen [return true if token is valid
	 * or return false if token is not found or expired]
	 */
	public function isTokenValid($uid, $token)
	{

		$this->ci
		     ->db->select('tokens.id, tokens.user_id, tokens.token, tokens.created_at, tokens.valid')
		     ->where('tokens.user_id', $uid)
		     ->where('tokens.token', $token);

		$result = $this->ci->db->get();

		if ( $result->num_rows() > 0 ) {

			$token = $result->row();

			// check if token expired
			if ( $token->valid == true ) {

				return true;

			}

		}

		return false;

	}


	/**
	 * Get user current user data
	 * @param integer [user id] [default is null]
	 * @return object
	 */
	public function userData($uid=null)
	{

		if ( is_null($uid) ) {

			$uid = $this->ci->session->userdata('user_id');

		}

		$this->ci->db
		         ->select('users.id as user_id, users.first_name, users.last_name, CONCAT(users.first_name," ",users.last_name) AS full_name, users.username, users.email, users.profile_picture, users.fakultas_id, users.user_status, users.lock')
		         ->select('groups.id as group_id, groups.group_name, groups.group_alias')
		         ->select('user_groups.id')
		         ->from('users')
		         ->where('users.id', $uid)
		         ->join('user_groups', 'user_groups.user_id = users.id')
		         ->join('groups', 'groups.id = user_groups.group_id');

		$result = $this->ci->db->get();

		if ( $result->num_rows() > 0 ) {

			foreach ( $result->result() as $user ) {

				$data = (object) array(
					'user_id' => $user->user_id,
					'first_name' => $user->first_name,
					'last_name' => $user->last_name,
					'full_name' => $user->full_name,
					'email' => $user->email,
					'profile_picture' => $user->profile_picture,
					'fakultas_id' => $user->fakultas_id,
					'user_status' => $user->user_status,
					'lock' => $user->lock,
					'group_alias' => $user->group_alias
				);

			}

			return $data;

		}

		return false;

	}


	/**
	 * Logout
	 * destroy login session
	 */
	public function logout()
	{

		$this->ci->session->sess_destroy();

	}

}

/* End of file Auth.php */
/* Location: ./application/libraries/Auth.php */
