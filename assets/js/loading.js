/**
 * Show loading
 */
function show_loading($target) {
	if ( typeof $target == 'undefined' ) {
		$target = '.sp-loading';
	}

	$($target).fadeIn(10);
}

/**
 * Hide loading
 */
function hide_loading($target) {
	if ( typeof $target == 'undefined' ) {
		$target = '.sp-loading';
	}

	$($target).fadeOut(10);
}