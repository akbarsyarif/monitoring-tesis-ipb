/**
 * append new row(s) into table
 * @return void
 */
function createTableData(rows) {
	var rows = typeof rows !== 'undefined' ? rows : [];

	var html = '';

	$tr    = '<tr>';
	$endtr = '</tr>';
	$td    = '<td>';
	$endtd = '</td>';

	for (var i = 0; i <= rows.length - 1; i++) {
		if ( typeof rows[i] == 'undefined' ) {
			console.log('Value must be an array.');
		} else {
			html += $tr;

			for (var n = 0; n <= rows[i].length - 1; n++) {
				html += $td + rows[i][n] + $endtd;
			};

			html += $endtr;
		}
	};

	return html;
}


/**
 * append row
 * @param  {[type]} data    [description]
 * @param  {[type]} $target [description]
 * @return {[type]}         [description]
 */
function appendRow(data, $target) {
	if ( typeof(data) == 'undefined' ) {
		console.log('Data is undefined.');
	} else if ( typeof($target) == 'undefined' ) {
		console.log('$target is undefined.');
	} else {
		$html  = '';
		$tr    = '<tr>';
		$endtr = '</tr>';
		$td    = '<td>';
		$endtd = '</td>';

		$html += $tr;

		for (i = 0; i <= data.length - 1; i++) {
			$html += $td + data[i] + $endtd;
		}

		$html += $endtr;
	}

	$($target).find('tbody').append($html);
}


/**
 * show / hide advance search form
 */
$(function() {
	$('#advance-search-toggle-btn').on('click', function() {
		$('.advance-search-form').slideToggle();
	});
});


/**
 * select 2 initiation
 */
$(function() {
	$('.select2-single').select2();
});

// reinitialize select2
function reinitializeSelect2Single($target) {
	$($target).select2('destroy');
	$($target).select2();
}

// init tinymce
function initTinymce(selector) {
	tinymce.init({
		selector: selector,
		plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools'
		],
		toolbar1: 'insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist link image',
		setup: function (editor) {
			editor.on('change', function () {
				editor.save();
			});
		},
	});
}

// reinitialize tinymce
function reinitTinymce(selector) {
	$(selector).tinymce().remove();
	setTimeout(function() {
		tinymce.init({
			selector: selector,
			setup: function (editor) {
				editor.on('change', function () {
					editor.save();
				});
			}
		});
	}, 0);
}

/**
 * initialize input mask
 */
// numeric
$(function() {
	$('.input-mask-integer').inputmask('integer', {
		groupSeparator : '.',
		autoGroup      : true,
		digits         : 0
	});

	$('.input-mask-numeric').inputmask('numeric');

	$('.input-mask-only-number').inputmask('integer', {
		autoGroup : true,
		digits : 0
	});
});


// reset form manual
function resetFormManual($target, isThereSelect2) {
	$target.resetForm();
	
	// reset hidden input
	$target.find('[type="hidden"]').val('');
	
	// reinitialize select2 single
	if (typeof isThereSelect2 == 'boolean') {
		if (isThereSelect2) {
			reinitializeSelect2Single($target.find('.select2-single'));
		}
	}
}
